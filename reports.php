<?php
$check=$_COOKIE['login'];
if($check!="")
{
	
}else{header("location:index.php");}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>COMPLYANCE - REPORTS</title>
<link rel="icon" href="images/logo/favicon.png" type="images/logo/favicon.png" />
<link href="fonts/fontStyle.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src="js/dataJs/jquery-latest.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<link rel="stylesheet" href="js/dataJs/jquery.dataTables.min.css" />
<script src="js/dataJs/jquery.dataTables.min.js"></script>
<script src="js/dataJs/dataTables.buttons.min.js"></script>
<script src="js/dataJs/buttons.html5.min.js"></script>
<script src="js/webjs1.js"></script>
<script src="js/new1.js"></script>
<script src="js/jquery.maskMoney.js"></script>
<script src="js/toggal.js"></script>
<script src="js/reports.js"></script>


 </head>

<body onLoad="username(),getAllrestaurant()">
<?php include('aside.php'); ?>

<section id="secFixHeight" class="overflow-hidden col-lg-10 col-md-9 col-sm-12 col-12 pull-left p-0">
<header class="pt-4 pb-4 border-bottom1px bg-colorWhite">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<div class="pull-left" style="display:block;">

<button class="btn btn-darkMenu paddingMenu cursor menuIconDis" onclick="openMenu();"><i class="fa fa-bars font-size22px"></i></button>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-7 pull-left p-0">
<h3 class="colorGreen m-0 textCenterScr768">REPORTS</h3>
</div>
<div class="col-lg-4 col-md-4 col-sm-3 col-6 pull-right">
<p class="pull-right m-0 mr-2"><span id="adminName"> </span><i class="fa fa-power-off ml-3 colorGreen font-size22px pull-right padding-top2px cursor" onclick="LogoutModal()"></i> </p>
</div>

</div>
</div>
</div>
</header>

<article class="overflow-yscroll mb-5">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<div class="col-12 overflow-hidden">
<h4 class="font-weight-bold mt-4 colorDarkGray pull-left">List of Reports</h4>
</div>

<div class="col-12 border-bottom1px pb-4 mt-3">

<div class="boxes row p-2">
 <div class="col-12 overflow-hidden">
  
  <div class="pull-left col-lg-2 col-md-5 col-sm-12 col-12 p-1">
    <select class="sort" id="sortingdropdpwn" onChange="sortingexp()">
      <option value="1">Sort by:</option>
    </select>
     
  </div> 
 
  <div class="pull-left  col-lg-2 col-md-5 col-sm-12 col-12 p-1 text-center" onClick="Sorting()">
     <input type="button" value="Expiration by Employee" class="reportsbtn" id="expirationbyemp"> 
   
  </div>

   
  <div class="pull-left col-lg-2 col-md-5 col-sm-12 col-12 p-1 text-center" onClick="Sorting1()">
   <input type="button" value="Items by category" class="reportsbtn" id="itemsbycatagoery"> 
  </div>
  
   <div class="col-lg-2 col-md-5 col-sm-12 col-12 p-1 text-center float-right" onClick="resetreports()">
      <input type="button" value="Reset" class="resetbutton">
  </div>
  
</div> 
</div>
</div>



<div class="col-12  pb-4 mt-3">

<div class="boxes row  p-4">

<table  cellpadding="10" width="100%" id="reportstable">
<thead>
<tr class="border-bottom1px colorDarkGray">
<th align="center">Title</th>
<th align="center">Employee</th>
<th align="center">Location</th>
<th align="center">Description</th>
<th align="center">Due Date</th>
</tr>
</thead>
<tbody id="reports">
</tbody>
</table>


</div>
</div>
    
    
    
    

</div>
</div>
</div>

</article>

</section>


<div class="col-12 pupopBg" id="loader" style="display:none;">
<div class="popupBg2">
<div class="loader">Loading...</div>
<P class="loaderTxt">Loading, please wait.</P>
</div>
</div>
  


<!-- LogModal Popup -->
   
<div class="modal fade" id="LogModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <p class="text-center font-familyUniversLT pt-3 pb-3 font-size20px">Are you sure you want to logout?</p>
            <div class="form-group col-6 pull-left">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="YES" onClick="LogoutUser()">
            </div>
            <div class="form-group col-6 pull-right">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="NO" onClick="closeLogoutModal()">
            </div>

      
       </div>

        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- LogModal Popup -->

   <input type="hidden" id="hiddenreports">
 
</body>
</html>

