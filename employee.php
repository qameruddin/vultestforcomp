<?php
$check=$_COOKIE['login'];
if($check!="")
{
	
}else{header("location:index.php");}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0,  maximum-scale=1.0, user-scalable=0">
<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">

<title>COMPLYANCE - EMPLOYEE</title>
<link rel="icon" href="images/logo/favicon.png" type="images/logo/favicon.png" />
<link href="fonts/fontStyle.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/datepicker.css"  rel="stylesheet" type="text/css">

<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.cookie.js"></script>
<link rel="stylesheet" href="js/dataJs/jquery.dataTables.min.css" />
<script src="js/dataJs/jquery.dataTables.min.js"></script>
<script src="js/dataJs/dataTables.buttons.min.js"></script>
<script src="js/dataJs/buttons.html5.min.js"></script>
<script src="js/webjs1.js"></script>
<script src="js/new1.js"></script>
<script src="js/toggal.js"></script>

</head>

<body onLoad="username(),getAllEmp(),getAllrestaurant()">
<?php include('aside.php'); ?>

<section id="secFixHeight" class="overflow-hidden col-lg-10 col-md-9 col-sm-12 col-12 pull-left p-0">
<header class="pt-4 pb-4 border-bottom1px bg-colorWhite">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<div class="pull-left" style="display:block;">
<!--<input type="button" class="fa fa-bars btn btn-danger pull-left " onClick="openMenu();">-->
<button class="btn btn-darkMenu paddingMenu cursor menuIconDis" onclick="openMenu();"><i class="fa fa-bars font-size22px"></i></button>
</div>
<div class="row">
   <div class="col-md-8 colorGreen m-0 textCenterScr768">
   <h3>CONTACTS <span class="rightarrow">&#x25BA;</span><span class="subheading">EMPLOYEE</span></h3>
   
   </div>
   <div class="col-lg-4 col-md-4 col-sm-3 col-6 pull-right">
<p class="pull-right m-0 mr-2"><span id="adminName"> </span><i class="fa fa-power-off ml-3 colorGreen font-size22px pull-right padding-top2px cursor" onclick="LogoutModal()"></i> </p>
</div>
   
 </div>


</div>
</div>
</div>
</header>

<article class="overflow-yscroll mb-5">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<div class="col-12 overflow-hidden">
<h4 class="font-weight-bold mt-4 colorDarkGray pull-left">Set Up Employee</h4>
<div class="alert alert-success fadeIn pull-right mb-0 margin-top10px" id="AlertMsg" role="alert" style="display:none">
  <span id="AlertTxt">You successfully Add Data.</span>
</div>
</div>

<div class="col-12 border-bottom1px pb-4 mt-3">

<div class="boxes col-12 p-4">
 <div class="col-12 overflow-hidden">
  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="empName">Employee ID</label>
    <input type="text" class="form-control icon-edit borderInputgreen" id="EmployeeID" aria-describedby="" placeholder="Employee ID">
  </div> 
  
  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="empName">Employee Name<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control icon-edit borderInputgreen" id="EmployeeName" aria-describedby="" placeholder="Employee Name"  onKeyPress="return onlyAlphabets(event)">
  </div>

  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="EmployeeEmail">Employee Email<span style="color:#149265;">*</span></label>
    <input type="email" class="form-control icon-edit borderInputgreen" id="EmployeeEmail" aria-describedby="" placeholder="Employee Email">
  </div>
  
  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="employeeDepartment">Department<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control icon-edit borderInputgreen" id="employeeDepartment" aria-describedby="" placeholder="Employee Department">
  </div>

   <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="comOn">Location<span style="color:#149265;">*</span><span class="settinglink">(Please add location in <a href="settings.php">Settings</a>)</span></label>
    <select class="form-control icon-arrow borderInputgreen" id="Restaurantdropdwon">
      <option value="">-Select-</option> 	
     
    </select>
  </div>
  
  
  
  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1"> 
    <label for="titleDeseg">Title (Designation)</label>
    <input type="text" class="form-control icon-edit borderInputgreen" id="TitleDesignation" aria-describedby=" " placeholder="Title (Designation)">
  </div>

  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1"> 
    <label for="education">Education<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control icon-edit borderInputgreen" id="education" aria-describedby=" " placeholder="Education">
  </div>

 <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="dateBirth">Date of Birth<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control borderInputgreen datepicker" id="DateOfBirth" onChange="dateDopJoinigMatch()" onkeydown="return false" placeholder="mm/dd/yy">
<!--        <input type="date" class="form-control borderInputgreen" id="DateOfBirth" onChange="dateDopJoinigMatch()" onkeydown="return false">
-->
  </div>
  
  <!--<div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="comOn">Completion of Onboarding<span style="color:#149265;">*</span></label>
    <select class="form-control icon-arrow borderInputgreen" id="CompletionOnboarding">
      <option value="">-Select-</option> 	
      <option value="Yes">Yes</option>
      <option value="No">No</option>
      <option value="N/A">N/A</option>
    </select>
  </div>-->
  
   <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="JoiningDate">Date of Hire<span style="color:#149265;">*</span></label>
<!--    <input type="date" class="form-control borderInputgreen" id="JoiningDate" aria-describedby=" " placeholder="" disabled onChange="dateofprobation()"  onkeydown="return false">
-->  
      <input type="text" class="form-control borderInputgreen datepicker" id="JoiningDate" placeholder="mm/dd/yy" disabled onChange="dateofprobation()"  onkeydown="return false">

</div>

  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="titleDeseg">Probation Period<span style="color:#149265;">*</span></label>
<!--    <input type="date" class="form-control borderInputgreen" id="ProbationPeriod" aria-describedby=" " placeholder=" "  onkeydown="return false">
-->  
    <input type="text" class="form-control borderInputgreen datepicker" id="ProbationPeriod" placeholder="mm/dd/yy"  onkeydown="return false">
</div>
  
 
  
  <div class="form-group pull-left col-lg-6 col-md-6 col-sm-12 col-12 p-1 mb-0">
    <label for="comOn">Comments</label>
    <textarea class="form-control icon-edit icon-position height-40px borderInputgreen" id="EmpComments" placeholder="Type Here..." rows="3" cols="10" maxlength="200"></textarea>
  </div>
  
</div>  
<div class="col-12 overflow-hidden">




  <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="dateBirth">Upload</label>
    <button type="button" class="form-control-file btn btn-white font-familyUniversLT icon-attachment col-lg-12 col-md-12 col-sm-12 col-12 text-left height-40px atchbtn borderInputgreen" aria-describedby="fileHelp">Upload Scanned<br> Copy of the Document</button>
   <input type="file" class="form-control-file inputfileBtn col-lg-12 col-md-12 col-sm-12 col-12 opacity0 borderInputgreen" id="fileEmp" name="fileEmp" aria-describedby="fileHelp" onChange="imgUpload()">
    <span class="colorGreen d-block m-2 pull-left" id="attached"></span>
    <span id="cross" class="colorGreen crossImgAttch font-familyUniversLT font-size14px cursor fontweightBold" onClick="imgUploadRemove()"></span>
   
  </div>

 
  
  
  <div class="form-group pull-right col-lg-3 col-md-5 col-sm-12 col-12 p-1 mt-33">
  <form mehtod="post" id="export_excel">  
    <button type="button" class="form-control-file bg-green col-lg-12 col-md-12 col-sm-12 col-12 atchbtn impbtn borderInputgreen font-size16px" aria-describedby="fileHelp">IMPORT FROM EXCEL</button>
   <input type="file" class="form-control-file inputfileBtn col-lg-12 col-md-12 col-sm-12 col-12 opacity0 borderInputgreen"  type="file" name="excel_file" id="excel_file">
    </form>
 </div>
 
  <div class="form-group pull-right col-lg-2 col-md-3 col-sm-12 col-12 p-1 margin-top28px">
    <input type="button" onClick="AddEmployeess()" class="btn borderInputgreen col-12 bg-green" value="ADD">
  </div>
 
 

</div> 



 
 
</div>
</div>


<div class="col-12 margin-top83px">
<h4 class="font-weight-bold mt-4 colorDarkGray pull-left">List of Employees</h4>
<div class="boxes2 col-12 p-4  overflow-hidden mt-3 margin-top83px">
<table class=" " cellpadding="10" width="100%" border="0" id="myTable">
<thead>
<tr class="border-bottom1px colorDarkGray">
<th align="center" class="outlineNone">#</th>
<th align="center" class="outlineNone">Employee ID</th>
<th align="center" class="outlineNone">Employee Name</th>
<th align="center" class="outlineNone" style="display: none">Employee Email</th>
<th align="center" class="outlineNone" style="display: none">Department</th>
<th align="center" class="outlineNone">Location</th>
<th align="center" class="outlineNone">Title (Designation)</th>
<th align="center" class="outlineNone" style="display: none">Education</th>
<th align="center" class="outlineNone" style="display: none">Date Of Birth</th>
<th align="center" class="outlineNone" style="display: none">Probation Period</th>
<th align="center" class="outlineNone">Joining Date</th>
<th align="center" class="outlineNone" style="display: none">Comments</th>
<th align="center" class="outlineNone"> Action</th>
</tr>
</thead>

<tbody id="AllEmployeesData">
</tbody>
</table>

</div>
</div>

</div>
</div>
</div>
</article>

</section>





<!-- Update Employee -->
<div class="modal fade" id="updateEmp" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <h5 class="pull-left col-11 p-0" id="docHead">Employee Detail</h5>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div id="empeupdate" class="alert alert-danger" style="display:none;">Please fill-in all the mandatory fields.</div>

         <div class="form-group col-12 p-0"> 
    <label for="empName" class="colorGreen">Employees ID</label>
    <input type="text" class="form-control icon-edit pupopBgPosition borderpuopInput p-0" id="EmployeeIDPOP" aria-describedby="" placeholder=" ">
  </div>

<div class="form-group col-12 p-0"> 
    <label for="empName" class="colorGreen">Employees Name*</label>
    <input type="hidden" id="EmployeeidPOP" >
    <input type="text" class="form-control icon-edit pupopBgPosition borderpuopInput p-0" id="EmployeeNamePOP" aria-describedby="" placeholder=" "  onKeyPress="return onlyAlphabets(event)">
  </div>

  <div class="form-group col-12 p-0"> 
    <label for="EmployeeEmailPOP" class="colorGreen">Employees Email*</label>
    <input type="text" class="form-control icon-edit pupopBgPosition borderpuopInput p-0" id="EmployeeEmailPOP" aria-describedby="" placeholder=" ">
  </div>

  <div class="form-group col-12 p-0"> 
    <label for="EmployeeDepartPOP" class="colorGreen">Employees Department*</label>
    <input type="text" class="form-control icon-edit pupopBgPosition borderpuopInput p-0" id="EmployeeDepartPOP" aria-describedby="" placeholder=" ">
  </div>
  
   <div class="form-group col-12 p-0">
    <label for="comOn" class="colorGreen">Location*</label>
    <select class="icon-arrowPop form-control borderpuopInput p-0 marginicon" id="Restaurantdropdwonpopup">
       <option value="">-Select-</option>
    </select>
  </div>
  
  
  
 <div class="form-group col-12 p-0"> 
    <label for="titleDeseg" class="colorGreen">Title (Designation)</label>
    <input type="text" class="form-control icon-edit pupopBgPosition borderpuopInput p-0" id="TitleDesignationPOP" aria-describedby=" " placeholder=" ">
  </div>
  <div class="form-group col-12 p-0"> 
    <label for="educationPOP" class="colorGreen">Education*</label>
    <input type="text" class="form-control icon-edit pupopBgPosition borderpuopInput p-0" id="educationPOP" aria-describedby=" " placeholder=" ">
  </div>
  <div class="form-group col-12 p-0">
    <label for="JoiningDate" class="colorGreen">Date of Hire*</label>
    <input type="text" class="form-control borderpuopInput p-0 marginicon datepicker" id="JoiningDatePOP" aria-describedby=" " placeholder=" " onkeydown="return false">

  </div>
  <!--<div class="form-group col-12 p-0">
    <label for="comOn" class="colorGreen">Completion of Onboarding</label>
    <select class="icon-arrowPop form-control borderpuopInput p-0 marginicon " id="CompletionOnboardingPOP">
      <option value="Yes">Yes</option>
      <option value="No">No</option>
      <option value="N/A">N/A</option>
    </select>
  </div>-->
  <div class="form-group col-12 p-0">
    <label for="titleDeseg" class="colorGreen">Probation Period*</label>
   <input type="text" class="form-control borderpuopInput p-0 marginicon datepicker" id="ProbationPeriodPOP" aria-describedby=" " placeholder=" " onkeydown="return false">
  </div>
  <div class="form-group col-12 p-0">
    <label for="dateBirth" class="colorGreen">Date of Birth*</label>
    <input type="text" class="form-control borderpuopInput p-0 marginicon datepicker" id="DateOfBirthPOP" aria-describedby=" " placeholder=" " onkeydown="return false">
   </div>
   
   <div class="form-group col-12 p-0"> 
    <label for="titleDeseg" class="colorGreen">Comments</label>
    <textarea type="text" class="form-control icon-edit2 borderpuopInput p-0 height-70px" id="CommentsPOP" aria-describedby=" " placeholder=" " style="padding-right:50px !important;"></textarea>
  </div>
  
  <div id="allDocsByUser">
       <div class="form-group  col-12 p-1 border-bottom1px icon-edit pupopBgPosition">
           <div class="colorGreen">License</div>
           <div class="font-size18px">Food safety</div>
             <div class="font-size14px row">
               <div class="col-lg-6 col-md-5 col-sm-12 col-12 pt-0 pl-3">Issuance date <span>(02/09/2018)</span></div>
               <div class="col-lg-6 col-md-5 col-sm-12 col-12">Expiration date <span>(02/09/2018)</span></div>
            </div>
      </div>
     
       <div class="form-group  col-12 p-1 border-bottom1px icon-edit pupopBgPosition">
           <div class="colorGreen">License</div>
           <div class="font-size18px">Food safety</div>
             <div class="font-size14px row">
               <div class="col-lg-6 col-md-5 col-sm-12 col-12 pt-0 pl-3">Issuance date <span>(02/09/2018)</span></div>
               <div class="col-lg-6 col-md-5 col-sm-12 col-12">Expiration date <span>(02/09/2018)</span></div>
            </div>
      </div>
  </div>
 
  <div class="colorGreen pb-3">Attachment</div>
  
  <div class="form-group col-12 text-right p-0" id="Dlink2">
 
  
  </div>
  
   <div class="form-group col-12 p-0">
  <input type="button" class="btn bg-green col-12 text-left p-2 icon-attachmentpupop pl-3" value="Add New Attachment">
    <input type="file" class="form-control-file inputfileBtn col-lg-12 col-md-12 col-sm-12 col-12 opacity0" id="filePOPEmp" name="filePOPEmp" aria-describedby="fileHelp">
  </div>
   
   
  <div class="form-group col-6 p-0 m-auto">
    <input type="button" class="btn bg-green col-12" value="UPDATE" onClick="updateEmp()">
  </div>


          
 

  
  


       

        </div>
        
      </div>
      
    </div>
  </div>

<!-- -->

<!-- Delete Popup -->
   
   <div class="modal fade" id="DelModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            
            <p class="text-center font-familyUniversLT pt-3 pb-3">Are you sure you want to delete this record?</p>
           <div class="form-group col-6 pull-left">
    <input type="button" class="btn bg-green col-12 cursor" value="YES" onClick="deleteEmp()">
  </div>
  <div class="form-group col-6 pull-right">
    <input type="button" class="btn bg-green col-12 cursor" value="NO" onClick="closeDelPupop()">
  </div>


        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- Delete Popup -->
  
  
  
  
  
  

<div class="col-12 pupopBg" id="loader" style="display:block;">
<div class="loader">Loading...</div>
<P class="loaderTxt">Loading, please wait.</P>
</div>


<!-- LogModal Popup -->
   
<div class="modal fade" id="LogModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <p class="text-center font-familyUniversLT pt-3 pb-3 font-size20px">Are you sure you want to logout?</p>
            <div class="form-group col-6 pull-left">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="YES" onClick="LogoutUser()">
            </div>
            <div class="form-group col-6 pull-right">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="NO" onClick="closeLogoutModal()">
            </div>

      
       </div>

        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- LogModal Popup -->
<script>dateRestrict(document.getElementById('DateOfBirth'),document.getElementById('DateOfBirthPOP'));</script>
 
  
<script>
/*$(document).ready(function(){
	
	$('#myTable').DataTable({  
	"ordering": false,
	"paging": false,
	"searching": false,
	   "bInfo" : false,
 		dom: 'lBfrtip',
		buttons: [
			  'csvHtml5',
			  {
 				filename: 'Data export',
  			}] 
	});
	$(".buttons-csv").html("<span>Excel</span>");
});	*/

$("#EmpComments,#CommentsPOP").bind('keypress', function(e) {
   
       if ((e.which > 32 && e.which < 44) || (e.which==45) ||   (e.which > 57 && e.which < 65) ||  (e.which > 90 && e.which < 97) || e.which > 122)         {
               e.preventDefault();
         }
				
}); 

var d = new Date();
var year = d.getFullYear();
d.setFullYear(year);


$("#DateOfBirth, #DateOfBirthPOP").datepicker({
changeMonth: true,
changeYear: true,
maxDate: new Date(),
yearRange : '1950:' + year + '',
defaultDate: d,
dateFormat: 'mm/dd/yy' });



$("#JoiningDate, #JoiningDatePOP").datepicker({
changeMonth: true,
changeYear: true,
yearRange : '1950:' + year + '',
defaultDate: d,
dateFormat: 'mm/dd/yy', });



$("#ProbationPeriod, #ProbationPeriodPOP").datepicker({
changeMonth: true,
changeYear: true,
yearRange : '1950:' + year + '',
defaultDate: d,
dateFormat: 'mm/dd/yy', });


</script>

<script>  
 $(document).ready(function(){  
   $('#export_excel').on('submit', function(event){  
	       event.preventDefault();  
		   var Location = $("#Restaurantdropdwon").val();
		   var data_new = new FormData(this); 
		   data_new.append('admin', ADMIN);
		   data_new.append('location', Location);
           $.ajax({  
                url:"export_employees.php",  
                method:"POST",  
                data:data_new,  
                contentType:false,  
                processData:false,  
     success:function(response)
		{
			//Cloading();
			 console.log(response);
  			 if(response==1)
			 { 	
				SAlert("Records insert successfully.");
				getAllEmp();
				$('#excel_file').val("");
			 }
  			 if(response==2)
  			 {
				DSAlert("Please upload a valid excel file.","margin-top0px");
				Cloading();
				$('#excel_file').val("");
				
  			 }
	   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");Cloading(); 	
	   }
           }); 
	
      });  
 });  
 
 
 function AddEmployee1(){
	  var conceptName = $("#Restaurantdropdwon").val();
	 
	 if(conceptName==0)
	 {
		 DSAlert("Please select location first.","margin-top0px");
	      Cloading();
	 }
	 else {
		 
		$('#export_excel').submit();
		 
		 }
	 
}

 </script> 

</body>
</html>

