<?php
//786
// Required if your environment does not handle autoloading

// Varifying request method
$request_method = $_SERVER["REQUEST_METHOD"];

if($request_method != "POST")
{
	echo $request_method. " method not allowed.";
	exit();			
}


// Checking request input

	if(!isset($_POST["receiver_number"]) || !isset($_POST["message"]))
	{
		echo "Please provide details"; 
		exit();
	}
	
	$receiver_number = trim($_POST["receiver_number"]);
	$message = $_POST["message"];
	
	if($receiver_number[0] != "+")
	{
		$receiver_number = "+".$receiver_number;
	}
	// Removing first + sign from number
	$number_without_plus = substr($receiver_number, 1);
	
	if(!ctype_digit($number_without_plus))
	{
		echo "Please enter valid number";
		exit();
	}
	
	
	if(empty($message))
	{
		echo "Please provide message";
		exit();
	}
		

require  'vendor/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;


// Your Account SID and Auth Token from twilio.com/console

/************************************************************************/
/************************************************************************/
		//  ========= LIVE CREDENTIALS ========= //

	$sid = 'AC2f9963fa79fc05c41b897f651f771dc9';
	$token = '6dabbc0253d869fb9388d02e9846290f';

/************************************************************************/
/************************************************************************/
	//  ========= TEST CREDENTIALS ========= //
	/*
	$sid = 'ACaa74f76de2533179f47e4984a19b5148';
	$token = '43c37482516b16cdbc65963023e12cca';
	*/
$client = new Client($sid, $token);

// Use the client to do fun stuff like send text messages!
$client_response = $client->messages->create(
    // the number you'd like to send the message to
    $receiver_number,
    array(
        // A Twilio phone number you purchased at twilio.com/console
        'from' => '+13524366243',
        // the body of the text message you'd like to send
        'body' => $message,
		"statusCallback" => "https://complyance.net/sms-notification/test.php",
        "provideFeedback" => True
    )
);

/*
echo "<pre>";
print_r($client_response);

echo "---------------------------- sid = ";
print_r($client_response->sid);

echo "---------------------------- status = ";
print_r($client_response->status);
echo "---------------------------- date_created = ";
print_r($client_response->date_created);
echo "---------------------------- date_updated";
print_r($client_response->date_updated);

echo "---------------------------- api_version";
print_r($client_response->api_version);
*/