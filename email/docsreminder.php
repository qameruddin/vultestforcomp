<?php

header('Access-Control-Allow-Origin: *');  
include("class.phpmailer.php");
$db =getConnection();

 
function Docs($db)
{  

	$statement= $db->prepare("select * FROM schoolforms.rcm_users_v2");
 	$statement->execute();
	if($data=$statement->fetchAll(PDO::FETCH_ASSOC))
	{	
 
	  foreach($data as $DBUser)
	  {
		  
$Totallp =array('ABC Manger License','Business License','Liquor License','Drivers License','State Issued Identification Card');

$Totalcertificate =array('TIPs Certification','Certificate of Occupancy','Food Service Manager Certification','Food Safety Handler Certification','ServSafe Manager','ServSafe Food Handler','ServSafe Alcohol','Certified Food Service Manager','TAM - Techniques in Alcohol Management','TIPS - Training for Intervention Procedures','CPR','First Aid'); 

$TotalInspection =array('Health Inspection');
 				
		$userid = array("userid" => $DBUser['id']);
		$statementRes =  $db->prepare("SELECT * FROM schoolforms.rcm_restaurant_v2 where user=:userid");
		$statementRes->execute($userid); 
		
	  if($dataRest=$statementRes->fetchAll(PDO::FETCH_ASSOC))
	  {	 
		   foreach($dataRest as $r)
		   {
			    $UserData=array();
				array_push($UserData,$DBUser['id'],$DBUser['fname'],$DBUser['email']); 
			
			    $ResData=array();
				array_push($ResData,$r['id'],$r['restaurant_name'],$r['user']);
 							
				$UTotalcertificate = $Totalcertificate;
				$UTotalLP = $Totallp;
				$UTotalInspection = $TotalInspection;
				
				
				$id = array("admin" => $r['user'],"adminR" => $r['id']); 
					
					
				//======certification ************
				$statement1= $db->prepare("SELECT DISTINCT (rcm_certification_v2.certification)FROM schoolforms.rcm_certification_v2 LEFT JOIN schoolforms.rcm_employee_v2 on rcm_certification_v2.employeeID=rcm_employee_v2.id LEFT join schoolforms.rcm_restaurant_v2 on rcm_employee_v2.resturant_id=rcm_restaurant_v2.id WHERE rcm_certification_v2.admin=:admin and  rcm_restaurant_v2.id=:adminR");
 				$statement1->execute($id);
				if($certification=$statement1->fetchAll(PDO::FETCH_ASSOC))
				{
					foreach($certification as $c)
					{
						if (($key = array_search($c['certification'], $UTotalcertificate)) !== false) {
							unset($UTotalcertificate[$key]);
						}
					}
				} 
				//==================================
				
				
				//======license_permit************//
				 $statement2= $db->prepare("SELECT DISTINCT (rcm_license_permit_v2.license) FROM schoolforms.rcm_license_permit_v2 LEFT JOIN schoolforms.rcm_employee_v2 on rcm_license_permit_v2.employeeID=rcm_employee_v2.id LEFT join schoolforms.rcm_restaurant_v2 on rcm_employee_v2.resturant_id=rcm_restaurant_v2.id WHERE rcm_license_permit_v2.admin=:admin and  rcm_restaurant_v2.id=:adminR");
			$statement2->execute($id);
			if($certification2=$statement2->fetchAll(PDO::FETCH_ASSOC))
			{
				foreach($certification2 as $c2)
				{
					if (($key2 = array_search($c2['license'], $UTotalLP)) !== false) {
					    unset($UTotalLP[$key2]);
					}
				}
			} 
			//==================================
			
			
			//======inspection******************
			$statement3= $db->prepare("SELECT DISTINCT (rcm_inspection_v2.inspection) FROM schoolforms.rcm_inspection_v2 LEFT JOIN schoolforms.rcm_employee_v2 on rcm_inspection_v2.employeeID=rcm_employee_v2.id LEFT join schoolforms.rcm_restaurant_v2 on rcm_employee_v2.resturant_id=rcm_restaurant_v2.id WHERE rcm_inspection_v2.admin=:admin and  rcm_restaurant_v2.id=:adminR");
			$statement3->execute($id);
			if($certification3=$statement3->fetchAll(PDO::FETCH_ASSOC))
			{
				foreach($certification3 as $c3)
				{
					if (($key3 = array_search($c3['inspection'], $UTotalInspection)) !== false) {
					    unset($UTotalInspection[$key3]);
					}
				}
			} 
			//==================================
 
			array_push($UserData,$UTotalInspection);
			array_push($UserData,$UTotalcertificate);
			array_push($UserData,$UTotalLP);
			array_push($UserData,$r['restaurant_name']);
			
			//echo json_encode($UserData);
			
			sendEmail($UserData);
		}
			
	   }
			
		}
			
		 


  
  

		
   	}  	
} 
 
 //30 Days Pool  
function sendEmail($emailData)
{
	//print_r($emailData);
	$userID=$emailData[0];
	$fname=$emailData[1];
	$email=$emailData[2];
	$ResName=$emailData[6];
 	 unset($emailData[0]);
	 unset($emailData[1]);
	 unset($emailData[2]);
	 unset($emailData[6]);
	 $data=""; 

 	 foreach($emailData as $d =>$val)
	 {
 	 	foreach($val as $k)
	 	{
	 		$data .="<li>".$k."</li>";
	 	}
	 }	 
   
  
  	$mail = new PHPMailer();  
	$mail->From ="info@complyance.net";
	$mail->FromName ="COMPLYANCE"; 
	$mail->addReplyTo("noreply@complyance.net", "COMPLYANCE");
	$mail->isHTML(true);
	$mail->Subject = "Missing Items for ".$ResName." ";
 	$mail->AltBody = "Error"; 
 
  	//$mail->AddAddress('ansari.asim92@gmail.com', $fname);
  	$mail->AddAddress($email, $fname);
	$mail->Body = "Hi ".$fname.", <br/><br/> Please see below the un-entered items. 
	<br/><br/>
	<ol>".$data."</ol>
	<a href='http://complyance.net/tool/'>Click here</a> to access COMPLYANCE and enter the missing items.<br/><br/>Thank You.";
	
	if(!$mail->Send()) 
	{
		echo json_encode("Error");
	} 
	else 
	{  		
		//echo json_encode("Success");
	}
	 
}
 
 

 Docs($db);
?>