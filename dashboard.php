<?php
$check=$_COOKIE['login'];

if($check!="")
{
	
}else{header("location:index.php");}



?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>COMPLYANCE - DASHBOARD</title>
<link rel="icon" href="images/logo/favicon.png" type="images/logo/favicon.png" />
<link href="fonts/fontStyle.css" rel="preload" as="font">
<link href="css/style.css" rel="stylesheet" type="text/css"> 

<script src="js/jquery.js"></script>
<script src="js/webjs1.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/new1.js"></script>

  
</head>

<body onLoad="username(),todayDate(),remainingdays(),getAllrestaurant()">
<?php include('aside.php'); ?>

 
<section id="secFixHeight" class="overflow-hidden col-lg-10 col-md-9 col-sm-12 col-12 pull-left p-0">
<header class="pt-4 pb-4 border-bottom1px bg-colorWhite">
<div class="container">


<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<div class="pull-left" style="display:block;">
<!--<input type="button" class="fa fa-bars btn btn-danger pull-left " onClick="openMenu();">-->
<button class="btn btn-darkMenu paddingMenu cursor menuIconDis" onclick="openMenu();"><i class="fa fa-bars font-size22px"></i></button>
</div>
<div class="col-lg-6 col-md-6 col-sm-8 col-6 pull-left p-0">
<h3 class="colorGreen m-0 textCenterScr768">DASHBOARD</h3>
</div>


<div class="col-lg-3 col-md-3 col-sm-3 col-6 pull-right">
<p class="pull-right m-0 mr-2"><span id="adminName"></span><i class="fa fa-power-off ml-3 colorGreen font-size22px pull-right padding-top2px cursor" onclick="LogoutModal()"></i> </p>
</div>



<div class="col-lg-3 col-md-3 col-sm-3 col-6 pull-right">
 <!-- <div class="remaindays">7 Days Remaining <i class="arrow-down"></i></div> -->
 <button class="btn btn-default dropdown-toggle remaindays" type="button" id="menu1" data-toggle="dropdown">
    <span id="daysR"></span> Days Remaining 
    <span class="caret"></span></button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
   <div class="row rowdropdown">
     <div class="col-md-3">
       <h6>Days Remaining</h6>
       <span style="font-size: 55px;background: #000;color: #fff;padding: 10px;" id="daysR2">3</span>
       <!-- <span  style="font-size: 55px;background: #000;color: #fff;padding: 10px;">0</span>-->
     </div>
     <div class="col-md-9">
       <div class="dropclose"><img src="images/catalina_service.bat.png" width="23"></div>
       <div style="margin-top:13px;">
       Trial SubscriptionExpires in  <span id="daysR1"></span> Days .<br>
You can convert this trial by purchasing the product.<br>
<button class="subbtn1" id="myBtn_1">View Subscription Plans</button>
       
       </div>
     </div>
   </div>
  </ul>
  
</div>

</div>
</div>
</div>
</header>

<article class="overflow-yscroll mb-5">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<h4 class="font-weight-bold mt-4 colorDarkGray pl-3">Summary</h4>
<div class="col-12 overflow-hidden border-bottom1px pb-4 p-0">

<div class="col-lg-6 col-md-6 col-sm-12 col-12 pull-left mt-3">
<div class="boxes col-12 p-4 overflow-hidden text-center">
<h4 class="colorLightOrange border-bottom1px m-0 pb-3 text-capitalize">Past Due</h4>
<div class="col-lg-6 col-md-6 col-sm-6 col-12 pull-left mt-3" style="padding:3px">
<h1 class="colorLightOrange m-0 pb-3 font-familyUniversLT font-size70px" id="LPPast">0</h1>
<p class="text-capitalize" id="LPPasttxt">Permit/License Has Passed Its Renewal Date</p>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-12 pull-left border-left1px border575Scr mt-3" style="padding:3px">
<h1 class="colorLightOrange m-0 pb-3 font-familyUniversLT font-size70px" id="CPast30">0</h1>
<p class="text-capitalize" id="CPast30txt">certification Has Passed Its renewal date</p>
</div>
</div>
</div>


<div class="col-lg-6 col-md-6 col-sm-12 col-12 pull-left mt-3">
<div class="boxes col-12 p-4 overflow-hidden text-center">
<h4 class="colorGreen border-bottom1px m-0 pb-3 text-capitalize">30 Days until Expiration</h4>
<div class="col-lg-6 col-md-6 col-sm-6 col-12 pull-left mt-3" style="padding:3px">
<h1 class="colorGreen m-0 pb-3 font-familyUniversLT font-size70px" id="LP30">0</h1>
<p class="text-capitalize" id="LP30txt">Permit/License is due for <br>renewal</p>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-12 pull-left border-left1px border575Scr mt-3" style="padding:3px">
<h1 class="colorGreen m-0 pb-3 font-familyUniversLT font-size70px" id="C30">0</h1>
<p class="text-capitalize" id="C30txt">certification is due for<br> renewal</p>

</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-12 pull-left mt-3">
<div class="boxes col-12 p-4 overflow-hidden text-center">
<h4 class="colorNewBlue border-bottom1px m-0 pb-3 text-capitalize">60 Days until Expiration</h4>
<div class="col-lg-6 col-md-6 col-sm-6 col-12 pull-left mt-3" style="padding:3px">
<h1 class="colorNewBlue m-0 pb-3 font-familyUniversLT font-size70px" id="LP60">0</h1>
<p class="text-capitalize" id="LP60txt">Permit/License is due for<br> renewal</p>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-12 pull-left border-left1px border575Scr mt-3" style="padding:3px">
<h1 class="colorNewBlue m-0 pb-3 font-familyUniversLT font-size70px" id="C60">0</h1>
<p class="text-capitalize" id="C60txt">certification is due for<br> renewal</p>
</div>
</div>
</div>

<div class="col-lg-6 col-md-6 col-sm-12 col-12 pull-left mt-3">
<div class="boxes col-12 p-4 overflow-hidden text-center">
<h4 class="colorParpel border-bottom1px m-0 pb-3 text-capitalize">90 Days until Expiration</h4>
<div class="col-lg-6 col-md-6 col-sm-6 col-12 pull-left mt-3" style="padding:3px">
<h1 class="colorParpel m-0 pb-3 font-familyUniversLT font-size70px" id="LP90">0</h1>
<p class="text-capitalize" id="LP90txt">Permit/License is due for<br> renewal</p>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-12 pull-left border-left1px border575Scr mt-3" style="padding:3px">
<h1 class="colorParpel m-0 pb-3 font-familyUniversLT font-size70px" id="C90">0</h1>
<p class="text-capitalize" id="C90txt">certification is due for<br> renewal</p>
</div>
</div>
</div>


</div>

<div class="col-12 margin-top83px">
<h4 class="font-weight-bold mt-4 colorDarkGray">Notifications</h4>


<div class="boxes2 col-12 p-4  overflow-hidden mt-3">
<table  cellpadding="10" width="100%">
<thead>
<tr class="border-bottom1px colorDarkGray">
<th align="center">#</th>
<th align="center">Title</th>
<th align="center">Employee</th>
<th align="center">Location</th>
<th align="center">Description</th>
<th align="center">Due Date</th>
<th align="center"></th>
</tr>
</thead>
<tbody id="notification">
</tbody>
</table>

</div>
</div>
 </div>
</div>
</div>
</article>

</section>


<div class="col-12 pupopBg" id="loader" style="display:none;">
<div class="popupBg2">
<div class="loader">Loading...</div>
<P class="loaderTxt">Loading, please wait.</P>
</div>
</div>


<!--
<div class="col-12 pupopBg" id="trialexpires" style="display:none;">
<div class="loader">Loading...</div>
<div class="loaderTxt_1">
   <h1>TRIAL ENDED</h1>
Trial Subscription is Expired<br> Renew Now.<br>
You can convert this trial by purchasing the product.<br>
<button class="subbtn" id="myBtn_2">View Subscription Plans</button><br>
For more information visit our website: <a href="https://complyance.net/" target="_blank">www.complyance.net</a>


</div>
</div>-->


<div class="modal fade" id="trialexpires" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body p-0">
           <div class="loaderTxt_1">
   <h1>TRIAL ENDED</h1>
Trial Subscription is Expired<br> Renew Now.<br>
You can convert this trial by purchasing the product.<br>
<button class="subbtn" id="myBtn_2">View Subscription Plans</button><br>
For more information visit our website: <a rel="noreferrer noopener" href="https://complyance.net/" target="_blank">www.complyance.net</a>


</div>

      
       </div>

        </div>
        
      </div>
      
    </div>




























<!--<button id="myBtn">Open Modal</button>-->
<div id="myModal_1" class="modal_1">

  <!-- Modal content -->
  <div class="modal-content_1">
    <span class="close_1 text-right"><img src="images/catalina_service.bat.png" width="23"></span>
    <div class="subheadingofplans">SUBSCRIPTION PLANS</div>
    <div class="row center-block">

              <div class="col-md-offset-3 col-md-2 col-sm-12 col-lg-3 mt-4"></div>
                <div class="col-md-2 col-sm-12 col-lg-3 mt-4">
                <div class="inner-box" data-aos="fade-down">
                <div class="icon">
                <div class="inner-icon">
                <span class="dollrsign">$</span><span class="font35">20</span>
                </div>
                </div>
                <h5>Monthly Package</h5>
                <ul>
                <li>Customizable solutions</li>
                <li>Individualized support to help admin manage</li>
                <li>Easy-to-use</li>
                <li>Unlimited space for documents</li>
                </ul>
                <a href="member/payment.php"><button type="button" class="btn btn-default nav-mybtn nav-sub bg-white" >BUY SUBSCRIPTION</button></a>
                </div>
                </div>
                
                <div class="col-md-offset-3 col-md-2 col-sm-12 col-lg-3 mt-4">
                <div class="inner-box" data-aos="fade-right">
                <div class="save"></div>
                <div class="icon">
                <div class="inner-icon">
                <span class="dollrsign">$</span><span class="font35">200</span>
                </div>
                </div>
                <h5>Yearly Package</h5>
                <ul>
                <li>Customizable solutions</li>
                <li>Individualized support to help admin manage</li>
                <li>Easy-to-use</li>
                <li>Unlimited space for documents</li>
                </ul>
                <a href="member/payment1.php"><button type="button" class="btn btn-default nav-mybtn nav-sub bg-white" data-aos="flip-down">BUY SUBSCRIPTION</button></a>
                </div>
                </div>
                            
</div>


            <div class="formor">For more information visit our website: <a href="https://complyance.net/">www.complynace.net</a></div>


</div>


  </div>

</div>




<!-- LogModal Popup -->
   
<div class="modal fade" id="LogModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <p class="text-center font-familyUniversLT pt-3 pb-3 font-size20px">Are you sure you want to logout?</p>
            <div class="form-group col-6 pull-left">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="YES" onClick="LogoutUser()">
            </div>
            <div class="form-group col-6 pull-right">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="NO" onClick="closeLogoutModal()">
            </div>

      
       </div>

        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- LogModal Popup -->
  
<input type="hidden" id="hiddenres_dash">
<script>
// Get the modal
var modal = document.getElementById('myModal_1');
var modal2 = document.getElementById('trialexpires');

// Get the button that opens the modal
var btn = document.getElementById("myBtn_1");
var btn1 = document.getElementById("myBtn_2");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close_1")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
  modal2.style.display = "none";
}

btn1.onclick = function() {
  modal.style.display = "block";
  modal2.style.display = "none";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  var daysR2 = parseFloat($("#daysR2").html());
  if(daysR2 < 1 ) {
   modal2.style.display = "block";
  }
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
	modal2.style.display = "none";
  }
}
</script>

</body>
</html>
