<?php

use PayPal\Api\Payer;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PPConnectionException;

require '../src/start.php';

$payer = new  Payer();
$details = new Details();
$amount = new Amount();
$transaction = new Transaction();
$payment = new Payment();
$redirectUrls = new RedirectUrls();




// Payer

$payer->setPaymentMethod('paypal');


// Details

$details->setShipping('0.00')
     ->setTax('0.00')
     ->setSubtotal('200.00');

// Amount 

$amount->setCurrency('USD')
      ->setTotal('200.00')
      ->setDetails($details);

// Transaction

$transaction->setAmount($amount)
           ->setDescription('Yearly Membership');

// Payments

$payment->setIntent('sale')
        ->setPayer($payer)
        ->setTransactions([$transaction]);

// redirectUrls

$redirectUrls->setReturnUrl('https://complyance.net/tool/test/paypal/pay.php?approved=true')
             ->setCancelUrl('https://complyance.net/tool/test/paypal/pay.php?approved=false');

$payment->setRedirectUrls($redirectUrls);    
 

try {
       $payment->create($api);
       
       $hash = md5($payment->getId());

       $_SESSION['paypal_hash'] = $hash;
	  
	 
	   $store = $db->prepare("
        INSERT INTO schoolforms.rcm_transaction_payments (user_id, payment_id, hash, complete) VALUES 
        (:user_id, :payment_id, :hash, 0)");
		
		$store->execute([
		   'user_id' => $_SESSION['user_id'],
		   'payment_id' => $payment->getId(),
		   'hash' => $hash
		]);
	
}
catch (PPConnectionException $ex) {
         header('Location: ../paypal/error.php');
    	
}




 foreach ($payment->getLinks() as $link) {
 	if($link->getRel() == 'approval_url')
 	{
 		$redirectUrls = $link->getHref();
 	}
 }
 //  var_dump($redirectUrls);   

 header('Location:' .$redirectUrls);
                           
