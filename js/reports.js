function DatatableInitReports()
{
 $('#reportstable').DataTable({  
	"ordering": false,
	"paging": false,
	"searching": true,
	"bInfo" : false,
	"bDestroy": true,
 		dom: 'lBfrtip',
		language: {
        searchPlaceholder: "Search",
		 search: "" 
    },
	
buttons: [
        {
            extend: 'csvHtml5',
             exportOptions: {
               columns: [ 0,1,2,3,4]
            }
        }
    ]
	});

	$(".buttons-csv").addClass("btn borderInputgreen bg-green col-2 pull-right");
 	$(".buttons-csv").html("<span>EXPORT IN EXCEL</span>");

}