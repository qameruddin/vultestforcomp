 
// JavaScript Document
var  regexCapitalLetter = new RegExp("[A-Z]"); //Uppercase Alphabet.
var  regexDigit = new RegExp("[0-9]"); //Uppercase Alphabet.
var  regexSpecialLetter = new RegExp("[$@$!%*#?&^<>_]"); //Uppercase Alphabet.


  




function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


function forgotPage()
{
	$("#loginPage").css("display","none");
	$("#ForgotPage").css("display","block");
	$("#RegisterPage").css("display","none");
}

function RegisterPage()
{
	$("#loginPage").css("display","none");
	$("#RegisterPage").css("display","block");
}
function loginPage()
{
	$("#loginPage").css("display","block");
	$("#ForgotPage").css("display","none");
	$("#RegisterPage").css("display","none");
	$("#firstname").val("");
	$("#lastname").val("");
	$("#email").val("");
	$("#password").val("");
	$("#Repassword").val("");
	
	
}
function FuncResetPassword()
{
	 NewPassword = $("#NewPassword").val();
	 ConfirmPassword = $("#ConfirmPassword").val();
	 email = $("#email").val();
	 token = $("#token").val();
	 if(NewPassword == "" || ConfirmPassword == ""){
	 	DSAlertnew("Fill the Given Fields","margin-top0px");Cloading();
	 	return false;
	 }
  	if(NewPassword==ConfirmPassword && email!="" && token!="")
	{
		loading();
		$.ajax({
		type: "POST",
 		data:{NewPassword1:NewPassword,email1:email,token1:token},
		dataType: 'json',
 		url: "passwordreset.php?act=FuncResetPassword", 
		success:function(response)
		{
			Cloading();
			// console.log(response);
  			 if(response=='success')
			 { 	
				SAlert("Password reset successfully.");
				$("#NewPassword").val("");
				$("#NewPassword").val("");
				LogoutUser();
			 }
  			 if(response=='Error')
  			 {
  			 	DSAlertnew("Unable to proceed, please try again.","margin-top0px");Cloading();
				LinkExpire();
  			 }
	   		}, 
		error: function (xhr, status) 
		{  
			DSAlertnew("Unable to proceed, please try again.","margin-top0px");Cloading();	loginPage(); 
			LinkExpire();  	
	   }
 	}); 
	}
	else
	{
		DSAlertnew("Password does not matched.");
		Cloading();
	}
}
function ForgotPasswordEmail()
{
	 userEmail = $("#Forgotemaill").val();
  if(userEmail!="" && validateEmail(userEmail)!=false)
	{
		loading();
	  	$.ajax({
			type: "POST",
	 		data:{email:userEmail},
			dataType: 'json',
	 		url: "passwordreset.php?act=ForgotPasswordEmail", 
			success:function(response)
			{
				Cloading();
	  			 if(response=='success')
				 { 	
					loginPage();
					SAlert("Email sent successfully!");
					$("#Forgotemaill").val("");
 	 			 }
	  			 if(response=='No Records')
	  			 {
	  			 
					DSAlert2("Email does not exist.","margin-top3px");
					Cloading()
	  			 }
	  			 if(response=='Error')
	  			 {
	  			 	DSAlert2('Unable to proceed, please try again.","margin-top0px');
					Cloading();
	  			 }
 	   		}, 
			error: function (xhr, status) 
			{  
			DSAlert2("Unable to proceed, please try again.","margin-top0px");
			Cloading();	
			loginPage();
	   		}
	 	});
 	}else{DSAlert2("Please enter a valid email address.","margin-top3px");Cloading()} 
}


function checkEmail(){
	Cemail = $("#email").val();
	$.ajax({
    type:'post',
        url:"api.php?action=checkMail",// put your real file name 
        data:{Cemail: Cemail},
        success:function(response){
            // console.log(response); // your message will come here.     
        }
 });

	
	
	
	
}

 function checkPassword(str)
  {
   
	 var re = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
	  
	    return re.test(str);
  }





function RegisterUser()
{
		firstname = $("#firstname").val();
		lastname = $("#lastname").val();
		email = $("#email").val();
		password = $("#password").val();
		Repassword = $("#Repassword").val();
	
		
		
		
    if(email!="" && validateEmail(email)==true && lastname!="" && firstname!="" && password!="" && Repassword!="" && password == Repassword && checkPassword(password)==true)
    {
        loading();
        $.ajax({
            type: "POST",
            data:{firstname:firstname,lastname:lastname,email:email,password:password},
            url: "api.php?action=RegisterUser",
            success:function(response)
            {
				if(response=='"exist"')
				{
				  DSAlert4("User Already Exists.","margin-top2px");
				  Cloading();
				}
				
                if(response=='"success"')
                {
                    //loginPage();
					ActivationEmail(email, firstname);
                    //SAlert("Email sent successfully!");
                    //$("#Forgotemaill").val("");
                }
                if(response=='No Records')
                {
                    DSAlert4("Please contact to support");Cloading();
                }
                if(response=='Error')
                {
                    DSAlert4('Unable to proceed, please try again.","margin-top0px');Cloading();
                }
                // console.log(response);
            },
            error: function (xhr, status)
            {
                DSAlert4("Unable to proceed, please try again.","margin-top0px");Cloading();loginPage();
            }
        });
    }
	else{
		//DSAlert3("Please fill-in all the mandatory fields.","margin-top3px");
		if(email=="" || lastname=="" || firstname=="" || password=="" || Repassword==""){
			DSAlert4("Please fill-in all the mandatory fields.","margin-top0px");
			
			}
		
		if(validateEmail(email)!=true &&  email!="")
		{
			DSAlert4("Please enter a valid email address.","margin-top0px");
		}
		if(password != Repassword){
			DSAlert4("Password does not match.","margin-top3px");
			}
			
			
		if(checkPassword(password)!=true &&  password!=""){
			DSAlert4("Password must be of minimum 8 characters containing a capital letter, a digit and a special character.","margin-top0px");
			//RegAlert2("Password must contain a capital letter, a digit and special character.", "undefined my-padding");
			}
		
		/*if(!re.test(password) &&  password!="")
		{
			DSAlert3("Enter a validate password.","margin-top3px");
		}	
			
		if(!re.test(password) && password!="" &&  !re.test(Repassword)&& Repassword!="" ){
			DSAlert3("Password must contain a capital letter, a digit and special character.","margin-top3px");
			
			}
			*/
			
		
		
		 Cloading();
		 
		 
		 
		 
		 }
}

function ActivationEmail(email, fname)
{
	 userEmail =email;
	 fname = fname;
	  	$.ajax({
			type: "POST",
	 		data:{email:userEmail, fname:fname},
			
	 		url: "passwordreset.php?act=ActivationEmail", 
			success:function(response)
			{
				// console.log(response);
				
	  			 if(response=='"success"')
				 { 	
					loginPage();
					Cloading();
					SAlertEmail("Please check your email to activate your account.");
					$("#firstname").val("");
					$("#lastname").val("");
					$("#email").val("");
					$("#password").val("");
					$("#Repassword").val("");
  	 			 }
	  			 if(response=='No Records')
	  			 {
	  			 	DSAlert2("Email does not exist.");
					Cloading();
	  			 }
	  			 if(response=='Error')
	  			 {
	  			 	DSAlert2('Unable to proceed, please try again.","margin-top0px');
					Cloading();
	  			 }
 	   		}, 
			error: function (xhr, status) 
			{  
			  DSAlert("Unable to proceed, please try again.","margin-top0px");Cloading();	loginPage();
			
	   		}
	 	});
}


var today_1 = new Date();
var dd_1 = today_1.getDate();
var mm_1 = today_1.getMonth()+1; //January is 0!
var yyyy_1 = today_1.getFullYear();

if(dd_1<10) {
    dd_1 = '0'+dd_1
} 

if(mm_1<10) {
    mm_1 = '0'+mm_1
} 

today_1 = yyyy_1 + '-' + mm_1 + '-' + dd_1;

//console.log(today_1);

var remberMe;

function remeberme() {
	remberMe = $("#remember").is(":checked");
}






function login()
{
	userEmail = $("#emaillAddLogin").val();
	UserPass = $("#passwordLogin").val();
	
 if(userEmail!="" && UserPass!="")
	{
		loading();
	    $.ajax({
		type: "POST",
 		data:{email:userEmail,password:UserPass},
		dataType: 'json',
  		url: "api.php?action=loginUser",
 		success:function(response)
		{ 
		
		//console.log(response);
		  if(response=="inactive"){
			   DSAlertnew("Please check your email and activate your account.","margin-top0px");
			   Cloading();
			  }
			  
		 else {	  
		   
		
			 if(response!='Error')
			 {
 				 $.cookie("login", "Admin");
				 $.cookie("fname", response[0]['fname']);
				 $.cookie("ID", response[0]['id']);
				 $.cookie("lname", response[0]['lname']);
				 $.cookie("email", response[0]['email']);
				// $.cookie("date", response[0]['date']);
				 $.cookie("remember", remberMe);
    			 if($.cookie("login")){window.location.href = "dashboard.php";}
			 }
			 else
			 {
				DSAlertnew("Invalid login credentials, please try again.","margin-top0px");
				Cloading();
				
			 }
		 }
			 
			
   		}, 
		error: function (xhr, status) 
		{  
			DSAlertnew("Unable to proceed, please try again.","margin-top0px");Cloading();
			Cloading();
    	}
 	});
	
	}
	
	else{DSAlertnew("Invalid login credentials, please try again.","margin-top0px");Cloading();} 
}




function LogoutUser()
{

    if( $.cookie("remember")=="true"){
		
		$.removeCookie("login");
		$.removeCookie("fname");
		$.removeCookie("lname");
		$.removeCookie("ID");
		
		}
	else{
		$.removeCookie("login");
		$.removeCookie("fname");
		$.removeCookie("lname");
		$.removeCookie("email");
		$.removeCookie("ID");	
		$.removeCookie("remember");
	}
  	window.location.href = "index.php";
}

function LinkExpire()
{ 
  	window.location.href = "linkExpired.php";
}
var ADMIN;
var remainDays=0;
function username()
{
	$('.dataTables_filter input').attr("placeholder", "Search");
	var fname = $.cookie('fname');
		ADMIN = $.cookie('ID');
		email = $.cookie('email');
		//date = $.cookie('date');
		if($.cookie('ID')==undefined || $.cookie('ID')=="" || $.cookie('email')==undefined || $.cookie('email')=="")
		{
			LogoutUser();
		}
	$("#adminName").html("Hi, "+fname);
	
	
	
	
		
		
}



function remainingdays()
{
   
   $.ajax({
		type: "POST",
 		data:{admin:ADMIN},
		dataType: 'json',
  		url: "api.php?action=remaindays",
 		success:function(response)
		{ 
		
			date = response[0]["date"];
			var day_start = new Date(today_1);
			var day_end = new Date(date);
			var total_days = (day_end - day_start) / (1000 * 60 * 60 * 24);
			
			remainDays = (Math.round(total_days));
			if(remainDays < 10)
			{
			   if(remainDays < 1)  {  $("#trialexpires").fadeIn(); remainDays=0; }
			   if(remainDays < 10) {  remainDays = "0"	+ remainDays; }
			}
			
			$("#daysR").html(remainDays);
			$("#daysR1").html(remainDays);
			$("#daysR2").html(remainDays);
			
			
		     
			 
			
   		}, 
		error: function (xhr, status) 
		{  
			DSAlertnew("Unable to proceed, please try again.","margin-top0px");Cloading();
			Cloading();
    	}
 	});	
	
	
	
	
}







	
// function AddEmployee()
// { 
// 	loading();
	
// 	EmployeeID	=   $("#EmployeeID").val();
// 	EmployeeName		= $("#EmployeeName").val();
// 	TitleDesignation 	= $("#TitleDesignation").val();
// 	JoiningDate 		= $("#JoiningDate").val();
// 	CompletionOnboarding= $("#CompletionOnboarding").val();
// 	ProbationPeriod 	= $("#ProbationPeriod").val();
// 	DateOfBirth 		= $("#DateOfBirth").val();
// 	Resturantval = $("#Restaurantdropdwon").val();
// 	EmployeeComments = $("#EmpComments").val();
	
	
	
// 	var formData = new FormData();
//  	formData.append("file", document.getElementById('fileEmp').files[0]);
//  		var objArr = [];
// 		objArr.push({"EmployeeID":EmployeeID, "EmployeeName": EmployeeName, "TitleDesignation": TitleDesignation, "JoiningDate": JoiningDate, "CompletionOnboarding": CompletionOnboarding,"ProbationPeriod":ProbationPeriod,"DateOfBirth":DateOfBirth,"Resturantval":Resturantval,"EmployeeComments":EmployeeComments,"admin":ADMIN}); 
//  		formData.append('objArr', JSON.stringify( objArr ));
		 
			
// 	if(EmployeeID!="" && EmployeeName!="" && TitleDesignation!="" && JoiningDate!="" && CompletionOnboarding!="" && ProbationPeriod!="" && DateOfBirth!="" && Resturantval!="")
// 	{
		
// 	$.ajax({		
// 		type: "POST",
//  		data:formData,
// 		contentType: false,
// 		cache: false,
// 		processData: false,
		
//  		url: "api.php?action=addEmp",

// 		success:function(response)
// 		{ 
// 			console.log(response);
//   			 if(response=='"success"')
// 			 {
// 				getAllEmp();
// 				 SAlert("Record has been added.");
				 	
// 					$("#EmployeeName").val("");
// 					$("#TitleDesignation").val("");
// 					$("#JoiningDate").val("");
// 					$("#JoiningDate").attr("disabled","disabled");
// 					$("#CompletionOnboarding").val("");
// 					$("#ProbationPeriod").val("");
// 					$("#DateOfBirth").val("");
// 					$("#EmployeeID").val("");
// 					$("#EmpComments").val("");
// 					$("#Restaurantdropdwon").val(0);
// 					imgUploadRemove();
// 			 }
// 			 else{
// 				 	Cloading();
// 				 }
				
// 			}, 
// 			error: function (xhr, status) 
// 			{  
// 				Cloading();
// 				DSAlert("Unable to proceed, please try again.","margin-top0px");
// 			}
//  		});
// 	}
// 	else{DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
// }


function AddEmployeess(){
	loading(); 	
	if(document.getElementById("excel_file").value != "") {AddEmployee1();}
	else{AddEmployee();}
}


function AddEmployee()
{ 
	loading();
	
	EmployeeID	=   $("#EmployeeID").val();
	EmployeeName		= $("#EmployeeName").val();
	EmployeeEmail		= $("#EmployeeEmail").val();
	Department 			= $('#employeeDepartment').val();
	TitleDesignation 	= $("#TitleDesignation").val();
	education			= $('#education').val();
	JoiningDate 		= $("#JoiningDate").val();
	//CompletionOnboarding= $("#CompletionOnboarding").val();
	ProbationPeriod 	= $("#ProbationPeriod").val();
	DateOfBirth 		= $("#DateOfBirth").val();
	Resturantval = $("#Restaurantdropdwon").val();
	EmployeeComments = $("#EmpComments").val();
	
	
	
	var formData = new FormData();
 	formData.append("file", document.getElementById('fileEmp').files[0]);
	console.log(document.getElementById('fileEmp').files[0]);
   	
 		var objArr = [];
		objArr.push({"EmployeeID":EmployeeID, "EmployeeName": EmployeeName, "EmployeeEmail": EmployeeEmail, "Department": Department, "TitleDesignation": TitleDesignation, "education": education, "JoiningDate": JoiningDate, "ProbationPeriod":ProbationPeriod,"DateOfBirth":DateOfBirth,"Resturantval":Resturantval,"EmployeeComments":EmployeeComments,"admin":ADMIN}); 
 		formData.append('objArr', JSON.stringify( objArr ));
		
		console.log(objArr);
		 
			
	if(EmployeeName!="" && EmployeeEmail!="" && Department!="" && education!="" && JoiningDate!=""  && ProbationPeriod!="" && DateOfBirth!="" && Resturantval!="" && Resturantval!=0)
	{
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(EmployeeEmail)){
			$.ajax({		
				type: "POST",
		 		data:formData,
				contentType: false,
				cache: false,
				processData: false,
				
		 		url: "api.php?action=addEmp",

				success:function(response)
				{ 
					console.log(response);
		  			 if(response=='"success"')
					 {
						getAllEmp();
						 SAlert("Record has been added.");
						 	
							$("#EmployeeName").val("");
							$("#EmployeeEmail").val("");
							$("#employeeDepartment").val("");
							$("#TitleDesignation").val("");
							$('#education').val("");
							$("#JoiningDate").val("");
							$("#JoiningDate").attr("disabled","disabled");
							//$("#CompletionOnboarding").val("");
							$("#ProbationPeriod").val("");
							$("#DateOfBirth").val("");
							$("#EmployeeID").val("");
							$("#EmpComments").val("");
							$("#Restaurantdropdwon").val(0);
							imgUploadRemove();
					 }
					 else{
						 	Cloading();
						 }
						
					}, 
					error: function (xhr, status) 
					{  
						Cloading();
						DSAlert("Unable to proceed, please try again.","margin-top0px");
					}
		 		});
			
		}
		else{
			DSAlert("Invalid Email Format.","margin-top0px");Cloading();
		}
	}
	else{DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
}




function DatatableInit()
{
 	$('#myTable').DataTable({  
	"ordering": false,
	"paging": false,
	"searching": true,
	   "bInfo" : false,
 		dom: 'lBfrtip',
		language: {
        searchPlaceholder: "Search",
		 search: "" 
    },
				buttons: [
        {
            extend: 'csvHtml5',
             exportOptions: {
               columns: [0,1,2,3,4,5,6,7,8,9,10,11]
            }
        }
    ]
	});
  	$(".buttons-csv").addClass("btn borderInputgreen bg-green col-2 pull-right");
 	$(".buttons-csv").html("<span>EXPORT IN EXCEL</span>");

}
function getAllEmp()
{   
loading();
 	$.ajax({
		type: "POST", 
		data:{admin:ADMIN},
 		url: "api.php?action=getAllEmp", 
		success:function(response)
		{
			Cloading();
  			 if(response!='"No Records"')
			 {
				 $("#myTable").dataTable().fnDestroy();
				 document.getElementById("AllEmployeesData").innerHTML ="";
				 response=JSON.parse(response);
				 // console.log(response);
				 for(var i=0;i<response.length;i++)
				 {
					var emp_id = "--";
					var emp_title = "--";
					var emp_JoiningDate=response[i]['joining_date'];
					dateFormate(emp_JoiningDate);
					if(response[i]['employee_id'] != ""){
						emp_id = response[i]['employee_id'];
					}
					if(response[i]['title'] != ""){
						emp_title = response[i]['title'];
					}
 					 document.getElementById("AllEmployeesData").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
					<td align="center">'+ emp_id +'</td>\
					<td align="center">'+response[i]['emp_name']+'</td>\
					<td align="center" style="display: none">'+response[i]['emp_email']+'</td>\
					<td align="center" style="display: none">'+response[i]['department']+'</td>\
					<td align="center">'+response[i]['restaurant_name']+'</td>\
					<td align="center">'+emp_title+'</td>\
					<td align="center" style="display: none">'+response[i]['education']+'</td>\
					<td align="center" style="display: none">'+response[i]['dob']+'</td>\
					<td align="center" style="display: none">'+response[i]['probation_period']+'</td>\
					<td align="center">'+finalJoiningDate+'</td>\
					<td align="center" style="display: none">'+response[i]['comments']+'</td>\
				<td align="center" class="p-0 colorGreen"><span class="cursor" onClick="getEmp('+response[i]['id']+')">View</span> | <span class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\
					</tr>';
				 }
				 DatatableInit();
			 }
			 else{ 
				 $("#myTable").dataTable().fnDestroy();
				 InfoAlert("No records found.");
				 $("#AllEmployeesData").html("");
			 }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			 
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}


function getEmp(id)
{  
	getEmpDocs(id);
	loading();
	$("#EmployeeNamePOP").val("");
	$("#EmployeeEmailPOP").val("");
	$("#EmployeeDepartPOP").val("");
	$("#TitleDesignationPOP").val("");
	$("#educationPOP").val("");
	$("#JoiningDatePOP").val("");
	//$("#CompletionOnboardingPOP").val( ""); 
	$("#DateOfBirthPOP").val(""); 
	$("#ProbationPeriodPOP").val(""); 
	$("#EmployeeidPOP").val( ""); 
	$("#EmployeeIDPOP").val("");
	 $("#Dlink2").html("");  

   	$.ajax({
		type: "POST",  
 		url: "api.php?action=getEmp", 
		data:{userID:id},
		success:function(response)
		{
			Cloading();
  			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
				// console.log(response);
				 for(var i=0;i<response.length;i++)
				 {
					 
					 $("#EmployeeIDPOP").val( response[i]['employee_id']);
					 $("#EmployeeNamePOP").val( response[i]['emp_name']);
					 $("#EmployeeEmailPOP").val( response[i]['emp_email']);
					 $("#EmployeeDepartPOP").val( response[i]['department']);
					 $("#TitleDesignationPOP").val( response[i]['title']);
					 $("#educationPOP").val( response[i]['education']);
					 
					 
					 
					 
					joining_date = new Date(response[i]['joining_date']); // yyyy-MM-dd
					joining_date = ("0" + (joining_date.getMonth() + 1)).slice(-2) + '/' + ("0" + joining_date.getDate()).slice(-2) + '/' + joining_date.getFullYear();
					$("#JoiningDatePOP").val(joining_date);
					
					dob = new Date(response[i]['dob']); // yyyy-MM-dd
					dob = ("0" + (dob.getMonth() + 1)).slice(-2) + '/' + ("0" + dob.getDate()).slice(-2) + '/' + dob.getFullYear();
					$("#DateOfBirthPOP").val(dob);
					
					probation_period = new Date(response[i]['probation_period']); // yyyy-MM-dd
					probation_period = ("0" + (probation_period.getMonth() + 1)).slice(-2) + '/' + ("0" + probation_period.getDate()).slice(-2) + '/' + probation_period.getFullYear();
					$("#ProbationPeriodPOP").val(probation_period);
					 
				
				
					
					 $("#EmployeeidPOP").val( response[i]['id']); 
					 $("#Restaurantdropdwonpopup").val( response[i]['resturant_id']); 
					 $("#CommentsPOP").val( response[i]['comments']); 

					if(response[i]['file']=="" || response[i]['file']==null)
					{
					   	
					}
					else{
					$("#Dlink2").html('<a class="btn borderInputgreen bg-green text-left d-block" href="'+response[i]['file']+'" download>View Attachment<i class="fa fa-eye pull-right font-size22px" aria-hidden="true"></i></a>');  
					}
					// openPupop();
					$("#updateEmp").modal();
 				 }
  			 }else{  InfoAlert("No records found."); }
   		}, 
		error: function (xhr, status) 
		{ 
		    Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");

   		}
 	});
}

function getEmpDocs(id)
{
 	$.ajax({
		type: "POST",  
 		url: "api.php?action=getEmpDocs", 
		data:{userID:id},
		success:function(response)
		{
			$("#allDocsByUser").html("");
			Cloading();
  			 if(response!='"No Records"')
			 {
			 	// console.log(response);
 				 response=JSON.parse(response);
		// 		 if(response['License']!="")
		// 		 {
 	// 				 for(var i=0;i<response['License'].length;i++)
		// 			 {
		// 				  var file="";
		// 				 if(response['License'][i]['lp_file_path']!="")
		// 				 {
		// file = '<a href="'+response['License'][i]['lp_file_path']+'" download=""><div class="pull-right icon-download mr-3"></div></a>';
		// 				 }
		// 				 $("#allDocsByUser").append('<div class="form-group  col-12 p-1 border-bottom1px">\
		// 					<div class="colorGreen">License</div>\
		// 					<div class="font-size18px">'+file+'</div>\
		// 					<div class="font-size18px">'+response['License'][i]['license']+'</div>\
		// 					<div class="font-size14px row">\
		// <div class="col-lg-6 col-md-5 col-sm-12 col-12 pt-0 pl-3">Issuance date <span>'+response['License'][i]['IssuanceDate']+'</span></div>\
		// <div class="col-lg-6 col-md-5 col-sm-12 col-12">Expiration date <span>'+response['License'][i]['ExpirationDate']+'</span></div>\
		// 				  </div></div>');
		// 			 }
		// 		 }
		// 		 if(response['Permit']!="")
		// 		 {
		// 			 for(var j=0;j<response['Permit'].length;j++)
		// 			 {
		// 				var file="";
		// 				 if(response['Permit'][j]['lp_file_path']!="")
		// 				 {
		// file = '<a href="'+response['Permit'][j]['lp_file_path']+'" download=""><div class="pull-right icon-download mr-3"></div></a>';
		// 				 }
		// 				 $("#allDocsByUser").append('<div class="form-group  col-12 p-1 border-bottom1px">\
		// 					<div class="colorGreen">Permit</div>\
		// 					<div class="font-size18px">'+file+'</div>\
		// 					<div class="font-size18px">'+response['Permit'][j]['license']+'</div>\
		// 					<div class="font-size14px row">\
		// <div class="col-lg-6 col-md-5 col-sm-12 col-12 pt-0 pl-3">Issuance date <span>'+response['Permit'][j]['IssuanceDate']+'</span></div>\
		// <div class="col-lg-6 col-md-5 col-sm-12 col-12">Expiration date <span>'+response['Permit'][j]['ExpirationDate']+'</span></div>\
		// 				  </div></div>');
		// 			 }
		// 		 }
				 if(response['certificate']!="")
				 {
					 for(var k=0;k<response['certificate'].length;k++)
					 {
						 var file="";
						 if(response['certificate'][k]['c_file_path']!="")
						 {
		file = '<a href="'+response['certificate'][k]['c_file_path']+'" download=""><div class="pull-right icon-download mr-3"></div></a>';
						 }
						 $("#allDocsByUser").append('<div class="form-group  col-12 p-1 border-bottom1px">\
							<div class="colorGreen">Certificate</div>\
							<div class="font-size18px">'+file+'</div>\
							<div>'+response['certificate'][k]['certification']+'</div>\
							<div class="font-size14px row">\
		<div class="col-lg-6 col-md-5 col-sm-12 col-12 pt-0 pl-3">Issuance date <span>'+response['certificate'][k]['issue_date']+'</span></div>\
		<div class="col-lg-6 col-md-5 col-sm-12 col-12">Expiration date <span>'+response['certificate'][k]['exp_date']+'</span></div>\
						  </div></div>');	
					 }
				 }
   			 }else{  InfoAlert("No records found."); }
   		}, 
		error: function (xhr, status) 
		{ 
		    Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");

   		}
 	});
}
function updateEmp()
{
//loading();
  	EmployeeName = $("#EmployeeNamePOP").val();
  	EmployeeEmail = $("#EmployeeEmailPOP").val();
  	EmployeeDepart = $("#EmployeeDepartPOP").val();
	TitleDesignation = $("#TitleDesignationPOP").val();
	education = $('#educationPOP').val();
	JoiningDate	= $("#JoiningDatePOP").val();
	//CompletionOnboarding = $("#CompletionOnboardingPOP").val();
	ProbationPeriod	= $("#ProbationPeriodPOP").val();
	DateOfBirth	= $("#DateOfBirthPOP").val();
	uID	= $("#EmployeeidPOP").val();

	CommentsPOP = $("#CommentsPOP").val();
	EmployeeIDPOP  = $("#EmployeeIDPOP").val();
	Restaurantdropdwonpopup = $("#Restaurantdropdwonpopup").val();
	 
	var form_data = new FormData();
	form_data.append("Ufile", document.getElementById('filePOPEmp').files[0]);
	
 var data = [];
data.push({"EmployeeName":EmployeeName,"EmployeeEmail": EmployeeEmail, "EmployeeDepart": EmployeeDepart,"TitleDesignation": TitleDesignation, "education": education, "JoiningDate": JoiningDate,  "ProbationPeriod": ProbationPeriod,"DateOfBirth":DateOfBirth,"admin":ADMIN,"Restaurantdropdwonpopup":Restaurantdropdwonpopup,"CommentsPOP":CommentsPOP,"EmployeeIDPOP":EmployeeIDPOP,"uID":uID});
   form_data.append('data', JSON.stringify(data));
   
   		
	if(EmployeeEmail!="" && EmployeeName!="" && EmployeeDepart!="" && education!="" && JoiningDate!="" &&  ProbationPeriod!="" && DateOfBirth!="" && Restaurantdropdwonpopup!=0)
	{
		
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
if(regex.test(EmployeeEmail)){
   
   	$.ajax({
		type: "POST",
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
		
		
 		url: "api.php?action=updateEmp",
		 
		success:function(response)
		{
			// console.log(response);
			 if(response=='"success"')
			 {
 				 getAllEmp();
				// closePupop();
				$("#updateEmp").modal('hide');
				 SAlert("Record has been updated.");
			 }
			 else{ Cloading();}
   		}, 
		error: function (xhr, status) 
		{  
			Cloading();	
			
			//DSAlert("Unable to proceed, please try again.","margin-top0px");
		$("#updateEmp").animate({ scrollTop: 0 }, "slow");
		$("#empeupdate").fadeIn("slow");
		$("#empeupdate").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Unable to proceed, please try again.");
		setTimeout(function(){ $("#empeupdate").fadeOut("slow"); }, 4000);
		Cloading();
		//console.log(status);
   		}
		
		
 	});
		}
		else 
		{
		$("#updateEmp").animate({ scrollTop: 0 }, "slow");
		$("#empeupdate").fadeIn("slow");
		$("#empeupdate").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Enter a valid email address.");
		setTimeout(function(){ $("#empeupdate").fadeOut("slow"); }, 4000);
		Cloading();
			
		}
	
	
	}
	else{
		//DSAlert("Please fill-in all the mandatory fields.","margin-top0px");
		$("#updateEmp").animate({ scrollTop: 0 }, "slow");
		$("#empeupdate").fadeIn("slow");
		$("#empeupdate").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Please fill-in all the mandatory fields.");
		setTimeout(function(){ $("#empeupdate").fadeOut("slow"); }, 4000);
		Cloading();
		
		}
	
}

function deleteEmp()
{ 
loading();
	$.ajax({
		type: "POST",
 		data:{userID:DeleteID},
		dataType: 'json',
 		url: "api.php?action=deleteEmp", 
		success:function(response)
		{
 			
 			 if(response=='success')
			 {
 				 getAllEmp();
				 closeDelPupop();
				 SAlert("Record has been deleted!");
 			 }
			 else{ DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	}
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	
   		}
 	});
}
 
function getEmpNames()
{  
	$.ajax({
		type: "POST", 
		dataType: 'json',
		data:{admin:ADMIN},
 		url: "api.php?action=getEmpNames", 
		success:function(response)
		{
			document.getElementById("EmployeeSelect").innerHTML="";
			document.getElementById("EmployeeSelect").innerHTML +='<option value="0">-Select-</option>';
  			 if(response!="No Records")
 			 {
 				 for(var i=0;i<response.length;i++)
				 { 
			          document.getElementById("EmployeeSelect").innerHTML +='<option value="'+response[i]['id']+'">'+response[i]['emp_name']+'</option>';
  				 }
  			 }
			 else{  $('.employeelink').css("visibility","visible");  }
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
	
	
}

function getResturantName(){
	
	$.ajax({
		type: "POST", 
		dataType: 'json',
		data:{admin:ADMIN},
 		url: "api.php?action=getRestNames", 
		success:function(response)
		{
			document.getElementById("RestNamebyEmp").value="";
			// console.log(response);
		
  			 if(response!="No Records")
 			 {
 				 for(var i=0;i<response.length;i++)
				 { 
		 	       document.getElementById("RestNamebyEmp").value =response[i]['resturant_id'];
  				 }
  			 }
			 else
			 { //document.getElementById("EmployeeSelect").innerHTML +='<option value="No Employee">No Employee</option>';
			  }
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
	
	
	
	
	}




function Addcertification()
{ 
 	Certification1		= $("#License").val();
	Employee1			= $("#EmployeeSelect").val();
	LocationID 			= $("#Restaurantdropdwon").val();
	IssuanceDate1 		= $("#IssuanceDate").val();
	ExpirationDate1		= $("#ExpirationDate").val();
	Comments1 			= $("#Comments").val(); 
    file                = $("#file").val();
	certificationtype=activeTab;
	

 	var form_data = new FormData();
   		form_data.append("file", document.getElementById('file').files[0]); 
var objArr = [];
objArr.push({"certification":Certification1, "employeeID": Employee1, "LocationID": LocationID, "issue_date": IssuanceDate1, "exp_date": ExpirationDate1, "comments": Comments1,"admin":ADMIN,"certificationtype":certificationtype}); 
form_data.append('objArr', JSON.stringify( objArr ));

//console.log(objArr);
if(certificationtype == "Location") {

 if(Certification1!="-Select-" && LocationID!=0 && LocationID!="" && IssuanceDate1!="" && ExpirationDate1!="")
	{
		loading();
    	$.ajax({
		type: "POST",
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
  		url: "api.php?action=Addcertification", 
		success:function(response)
		{
			
   			 if(response=='"success"')
			 {
 				  getAllCertification();
				  SAlert("Record has been added.");
				  
					
					$("#EmployeeSelect").val("");
					$("#IssuanceDate").val("");
					$("#ExpirationDate").val("");
					$("#Comments").val("");
					$("#file").val("");
					document.getElementById("ExpirationDate").disabled = true;
					imgUploadRemove();
					$("#EmployeeSelect").val(0);
					$("#Restaurantdropdwon").val(0);
					$("#License").val("-Select-");
				  
			 } else{Cloading();DSAlert("Error uploading file");}
   		}, 
		error: function (xhr, status) 
		{  
			Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
	}
	else{ DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
	
}

 else 
 {
	 
	 if(Certification1!="-Select-" && Employee1!="-Select-" && Employee1!=0  &&  IssuanceDate1!="" && ExpirationDate1!="")
	{
		loading();
    	$.ajax({
		type: "POST",
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
  		url: "api.php?action=Addcertification", 
		success:function(response)
		{
			
   			 if(response=='"success"')
			 {
 				  getAllCertification();
				  SAlert("Record has been added.");
				  
					
					$("#EmployeeSelect").val("");
					$("#IssuanceDate").val("");
					$("#ExpirationDate").val("");
					$("#Comments").val("");
					$("#Restaurantdropdwon").val("0"); 
					$("#file").val("");
					document.getElementById("ExpirationDate").disabled = true;
					imgUploadRemove();
					$("#EmployeeSelect").val(0);
					$("#Restaurantdropdwon").val(0);
					$("#License").val("-Select-");
				  
			 } else{Cloading();DSAlert("Error uploading file");}
   		}, 
		error: function (xhr, status) 
		{  
			Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
	}
	else{ DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();} 
	 
	 
	 
  }




}


var reportcertificates;
function getAllCertification()
{   
loading();
   	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
 		url: "api.php?action=getAllCertification", 
		success:function(response)
		{
			Cloading();
  			 if(response!='"No Records"')
			 {
			 	
 				 response=JSON.parse(response);
				 console.log(response);
				 reportcertificates = response
				 document.getElementById("getAllLicense_permit").innerHTML ="";
				 for(var i=0;i<response.length;i++)
				 {	
				 	 getexp_date=response[i]['exp_date'];	
					 getissue_date=response[i]['issue_date'];
					 dateFormate(getexp_date,getissue_date);
					 if(response[i]['restaurant_name']==null || response[i]['restaurant_name']==""){response[i]['restaurant_name']=$("#hiddenres").val();}
				     var empname = (response[i]['empname']==null ? "-" : response[i]['empname']);
					 document.getElementById("getAllLicense_permit").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
					<td align="center">'+response[i]['certification']+'</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+response[i]['restaurant_name']+'</td>\
					<td align="center">'+finalJoiningDate2+'</td>\
					<td align="center">'+finalJoiningDate+'</td>\
				<td align="center" class="p-0 colorGreen"><span class="cursor" onClick="emailPupop('+response[i]['id']+',\'certification\')">Email</span> | <span class="cursor" onClick="getCertification('+response[i]['id']+')">View</span> | <span  class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\
					</tr>';
				 }
   			 }else{ InfoAlert("No records found.");document.getElementById("getAllLicense_permit").innerHTML="";}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}

function getCertification(id)
{ 
loading(); 
 
	$("#Dlink").html("");
	$("#EmployeeSelectPOP").val("");
	$("#LPIDPOP").val("");
	$("#LicensePOP").val("");
	$("#CommentsPOP").val("");
	$("#IssuanceDatePOP").val("");
	$("#ExpirationDatePOP").val("");
	$("#HfilePOP").val("");
	$("#EmployeeidPOP").val("");
	$('#Restaurantdropdwonpopup').val();

   	$.ajax({
		type: "POST",  
 		url: "api.php?action=getCertification", 

		data:{certID:id},
		success:function(response)
		{  
			
			Cloading();
  			 if(response!='"No Records"')
			 {
		 	
 				 response=JSON.parse(response);
				 
				 
				 for(var i=0;i<response.length;i++)
				 {
			
					 $('#Restaurantdropdwonpopup').val(response[i]['LocationID']);
   					 $("#LicensePOP").val(response[i]['certification']);
					 $("#CommentsPOP").val(response[i]['comments']); 
 					 $("#LPIDPOP").val(response[i]['id']); 
 					 $("#EmployeeidPOP").val(response[i]['employeeID']);
					 
					/* $("#IssuanceDatePOP").val( response[i]['issue_date']);
					 $("#ExpirationDatePOP").val(response[i]['exp_date']); */
					 
					 
					dateIss = new Date(response[i]['issue_date']); // yyyy-MM-dd
					dateIss = ("0" + (dateIss.getMonth() + 1)).slice(-2) + '/' + ("0" + dateIss.getDate()).slice(-2) + '/' + dateIss.getFullYear();
					
					$("#IssuanceDatePOP").val(dateIss);
					
					dateExp = new Date(response[i]['exp_date']); // yyyy-MM-dd
					dateExp =  ("0" + (dateExp.getMonth() + 1)).slice(-2) + '/' + ("0" + dateExp.getDate()).slice(-2) + '/' + dateExp.getFullYear();
					
					$("#ExpirationDatePOP").val(dateExp);
					 
					 
					 
					 
		if(response[i]['c_file_path']!="")
		{		
  					$("#Dlink").html('<a class="btn borderInputgreen bg-green text-left d-block" href="'+response[i]['c_file_path']+'" download>View Attachment<i class="fa fa-eye pull-right font-size22px" aria-hidden="true"></i></a>');  
		}
 		else
		{
			$("#Dlink").html("");
		}
 
					  $("#updateliecense").modal();
					  
 				 }
  			 }else{  }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}

function UpdateCertification()
{
	loading();
	//CertificationPOP	=	$("#LPIDPOP").val();
	CertificationPOP	=	$("#LicensePOP").val();
	certID 			= 	$("#LPIDPOP").val();
	//EmployeeSelectPOP	=	$("#EmployeeSelectPOP").val();
	IssuanceDatePOP		=	$("#IssuanceDatePOP").val();
	ExpirationDatePOP	=	$("#ExpirationDatePOP").val();
	CommentsPOP			=	$("#CommentsPOP").val();
	LocationID 			=	$('#Restaurantdropdwonpopup').val();
	
	var form_data = new FormData();
	form_data.append("Ufile", document.getElementById('filePOP').files[0]);
		
 var data = [];
data.push({"certID":certID, "certification":CertificationPOP,"issue_date": IssuanceDatePOP, "exp_date": ExpirationDatePOP, "comments": CommentsPOP,"LocationID":LocationID});
 form_data.append('data', JSON.stringify(data));
 
 if(IssuanceDatePOP!="" && ExpirationDatePOP!="" && LocationID !="0"){
 
 	$.ajax({
		type: "POST",
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
 		url: "api.php?action=UpdateCertification", 
		success:function(response)
		{
 			// console.log(response);
			 if(response=='"success"')
			 {
				 $("#updateliecense").modal('hide');
 				 getAllCertification();
			 	 SAlert("Record has been updated.");
			 }
			 else{ Cloading();}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			//DSAlert("Unable to proceed, please try again.","margin-top0px");
			$("#Lisupdate").fadeIn("slow");
		    $("#Lisupdate").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Unable to proceed, please try again.");
		    setTimeout(function(){ $("#Lisupdate").fadeOut("slow"); }, 4000);
   		}
 	});
 }
 else{
	 Cloading();
            $("#Lisupdate").fadeIn("slow");
		    $("#Lisupdate").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Please fill-in all the mandatory fields.");
		    setTimeout(function(){ $("#Lisupdate").fadeOut("slow"); }, 4000);
	 
	 
	 
	 }
}


function deleteCertification()
{ 
loading();
	$.ajax({
		type: "POST",
 		data:{certID:DeleteID},
		dataType: 'json',
 		url: "api.php?action=deleteCertification", 
		success:function(response)
		{	
			// console.log(response);
			 if(response=='success')
			 {
 				 getAllCertification();
				 closeDelPupop();
				 SAlert("Record has been deleted!");
			 }
			 else{ DDSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	}
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	
   		}
 	});
}



  

function AddLicense_permit()
{ 

License			= $("#License").val();
Comments		= $("#Comments").val();
LicPerUploadFile = $("#file").val();
IssuanceDate 	= $("#IssuanceDate").val();
ExpirationDate 	= $("#ExpirationDate").val();
LocationID		= $("#Restaurantdropdwon").val();
EmployeeSelect	= $("#EmployeeSelect").val();

/*alert(EmployeeSelect+"-"+LocationID);
return;*/


doctype = activeTabName;
LPType = activeTab;
//alert(LPType);
 
 	var form_data = new FormData();
	form_data.append("file", document.getElementById('file').files[0]);
	var objArr = [];
	objArr.push({"license":License, "comments": Comments, "IssuanceDate": IssuanceDate, "ExpirationDate": ExpirationDate,"admin":ADMIN,"LocationID":LocationID,"EmployeeSelect":EmployeeSelect,"doctype":doctype,"LPType":LPType}); 
	form_data.append('objArr', JSON.stringify( objArr ));
	
	if(LPType == "Employee") {
	
  	if(License!="-Select-"  && IssuanceDate!="" &&  ExpirationDate!="" && EmployeeSelect!="-Select-" && EmployeeSelect!=0)
	{
		loading();
    	$.ajax({
		type: "POST",  
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
  		url: "api.php?action=AddLicense_permit", 
		success:function(response)
		{
   			 if(response=='"success"')
			 {
				  SAlert("Record has been added.");
				  GetAllDocs();
				 
					$("#License").val("-Select-");
					$("#Comments").val("");
					$("#IssuanceDate").val("");
					$("#ExpirationDate").val("");
					document.getElementById("ExpirationDate").disabled = true;
					imgUploadRemove();
					$("#EmployeeSelect").val(0);
					$("#Restaurantdropdwon").val(0);
					
					

 			 } else{Cloading();DSAlert("Unable to proceed, please try again.","margin-top0px");}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
	}
 	else{DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
	
	}
	
	else {
		
	if(License!="-Select-"  && IssuanceDate!="" &&  ExpirationDate!="" && LocationID!="0" &&  LocationID!="")
	{
		loading();
    	$.ajax({
		type: "POST",  
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
  		url: "api.php?action=AddLicense_permit", 
		success:function(response)
		{
   			 if(response=='"success"')
			 {
				  SAlert("Record has been added.");
				  GetAllDocs();
				 
					$("#License").val("-Select-");
					$("#Comments").val("");
					$("#IssuanceDate").val("");
					$("#ExpirationDate").val("");
					document.getElementById("ExpirationDate").disabled = true;
					imgUploadRemove();
					$("#EmployeeSelect").val(0);
					$("#Restaurantdropdwon").val(0);
					
					

 			 } else{Cloading();DSAlert("Unable to proceed, please try again.","margin-top0px");}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
	}
 	else{DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
		
		
		
		
		}
	
	
	
 }


function UpdateLicense_permit()
{
 	LPIDPOP			= $("#LPIDPOP").val();
	License			= $("#LicensePOP").val();
	Comments		= $("#CommentsPOP").val();
	IssuanceDatePOP	= $("#IssuanceDatePOP").val();
	ExpirationDatePOP	= $("#ExpirationDatePOP").val();
	LocationID 		= $('#Restaurantdropdwonpopup').val();
	loading();
 	var form_data = new FormData();
	form_data.append("Ufile", document.getElementById('filePOP').files[0]);
	var data = [];
	data.push({"LPID":LPIDPOP, "license": License, "comments": Comments, "IssuanceDate": IssuanceDatePOP, "ExpirationDate": ExpirationDatePOP, "LocationID":LocationID});
	form_data.append('data', JSON.stringify(data));
	
	if(IssuanceDatePOP!="" && ExpirationDatePOP!="" && LocationID!="0"){

 $.ajax({
		type: "POST",
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
 		url: "api.php?action=UpdateLicense_permit", 
		success:function(response)
		{
 			// console.log(response);
			 if(response=='"success"')
			 {
				// closePupop();
				$("#updateliecense").modal('hide');
 				 GetAllDocs();
				 SAlert("Record has been updated.");
			 	//window.location.href = "dashboard.php";
			 }
			 else{ Cloading();closePupop();}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			
			//DSAlert("Unable to proceed, please try again.","margin-top0px");
		$("#Lisupdate").fadeIn("slow");
		$("#Lisupdate").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Unable to proceed, please try again.");
		setTimeout(function(){ $("#Lisupdate").fadeOut("slow"); }, 4000);
   		}
 	});
	}
	else{
		Cloading();
		
		//DSAlert("Please fill-in all the mandatory fields.","margin-top0px");}
		$("#Lisupdate").fadeIn("slow");
		$("#Lisupdate").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Please fill-in all the mandatory fields.");
		setTimeout(function(){ $("#Lisupdate").fadeOut("slow"); }, 4000);
      }
}



function getAllLicense()
{   
loading();
  	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
  		url: "api.php?action=getAllLicense", 
		success:function(response)
		{
			
			Cloading();
  			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
				  console.log(response);
				 document.getElementById("getAllLicense_permit").innerHTML ="";
				 for(var i=0;i<response.length;i++)
				 {
					    dateFormate(response[i]['IssuanceDate'],response[i]['ExpirationDate']);
						if(response[i]['restaurant_name']==null || response[i]['restaurant_name']==""){response[i]['restaurant_name']="-";}
						 var empname = (response[i]['empname']==null ? "-" : response[i]['empname']);
						
						
						
					 document.getElementById("getAllLicense_permit").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
					<td align="center">'+response[i]['license']+'</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+response[i]['restaurant_name']+'</td>\
					<td align="center">'+finalJoiningDate+'</td>\
					<td align="center">'+finalJoiningDate2+'</td>\
				<td align="center" class="p-0 colorGreen"><span class="cursor" onClick="emailPupop('+response[i]['id']+',\'License_permit\')">Email</span> | <span class="cursor" onClick="getLicense_permit('+response[i]['id']+')">View</span> | <span class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\
					</tr>';
				 }
   			 }else{ InfoAlert("No records found.");document.getElementById("getAllLicense_permit").innerHTML="";}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}


function getAllPermit()
{   
loading();
  	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
  		url: "api.php?action=getAllPermit", 
		success:function(response)
		{Cloading();
  			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
				  console.log(response);
				 document.getElementById("getAllLicense_permit").innerHTML ="";
				 for(var i=0;i<response.length;i++)
				 {
					    dateFormate(response[i]['IssuanceDate'],response[i]['ExpirationDate']);
					
					if(response[i]['restaurant_name']==null || response[i]['restaurant_name']==""){response[i]['restaurant_name']="-";}
					 var empname = (response[i]['empname']==null || response[i]['empname']=="" ? "-" : response[i]['empname']);
					 
					 
					 document.getElementById("getAllLicense_permit").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
					<td align="center">'+response[i]['license']+'</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+response[i]['restaurant_name']+'</td>\
					<td align="center">'+finalJoiningDate+'</td>\
					<td align="center">'+finalJoiningDate2+'</td>\
				<td align="center" class="p-0 colorGreen"><span class="cursor" onClick="emailPupop('+response[i]['id']+',\'Permit\')">Email</span> | <span class="cursor" onClick="getLicense_permit('+response[i]['id']+')">View</span> | <span class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\
					</tr>';
				 }
   			 }else{ InfoAlert("No records found.");document.getElementById("getAllLicense_permit").innerHTML="";}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}

 

function getLicense_permit(id)
{  

$("#LicensePOP").val("");
$("#CommentsPOP").val("");
$("#LPIDPOP").val("");
$("#EmployeeidPOP").val("");
$("#EmployeeSelectPOP").val("");
$("#HfilePOP").val("");
$("#Dlink").html("");

loading();
   	$.ajax({
		type: "POST",  
 		url: "api.php?action=getLicense_permit", 
		data:{LPID:id},
		success:function(response)
		{	
		
		Cloading();
  			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
				
 				 for(var i=0;i<response.length;i++)
				 {
				 	 console.log(response);
					$('#Restaurantdropdwonpopup').val(response[i]['LocationID']);
 					$("#LPIDPOP").val(response[i]['id']);
					$("#LicensePOP").val(response[i]['license']);
					$("#CommentsPOP").val(response[i]['comments']);
					
					dateIss = new Date(response[i]['IssuanceDate']); // yyyy-MM-dd
					dateIss = ("0" + (dateIss.getMonth() + 1)).slice(-2) + '/' + ("0" + dateIss.getDate()).slice(-2) + '/' + dateIss.getFullYear();
					
					$("#IssuanceDatePOP").val(dateIss);
					
					dateExp = new Date(response[i]['ExpirationDate']); // yyyy-MM-dd
					dateExp =  ("0" + (dateExp.getMonth() + 1)).slice(-2) + '/' + ("0" + dateExp.getDate()).slice(-2) + '/' + dateExp.getFullYear();
					
					$("#ExpirationDatePOP").val(dateExp);
				
					
					
					$("#HfilePOP").val(response[i]['lp_file_path']);
					$("#EmployeeidPOP").val(response[i]['employeeID']);
					
		if(response[i]['lp_file_path']!="")
		{		
  				$("#Dlink").html('<a class="btn borderInputgreen bg-green text-left d-block" href="'+response[i]['lp_file_path']+'" download>View Attachment<i class="fa fa-eye pull-right font-size22px" aria-hidden="true"></i></a>');  
		}
   					  //openPupop(); 
					 $("#updateliecense").modal();
					 
 				 }
  			 }else{  }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}


function deleteLicense_permit()
{   
	loading();
	$.ajax({
		type: "POST",
 		data:{LPID:DeleteID},
		dataType: 'json',
 		url: "api.php?action=deleteLicense_permit", 
		success:function(response)
		{  	
			// console.log(response);	 
			 if(response=='success')
			 {
 				  GetAllDocs();
				 closeDelPupop();
				 SAlert("Record has been deleted!");
			 }
			 else{ DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	}
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	
   		}
 	});
}








function Add_inspections(){
	loading(); 	
	if(document.getElementById("excel_file").value != "") {Add_inspection1();}
	else{Add_inspection();}
}


function Add_inspection()
{ 
	
	InspectorName       = $("#InspectorName").val();
	Inspection			= $("#Inspection").val();
	LocationID			= $("#Restaurantdropdwon").val();
	InspectionDate		= $("#InspectionDate").val();
	is_followUp			= $("#noYesSwitch").attr('aria-pressed');
	Comments			= $("#Comments").val();
	insploadFile 		= $("#file").val();	

	followUp_date		= "";
	if(is_followUp=="true")
	{
		followUp_date = $("#switchDate").val();
 	}
 	
	var form_data = new FormData();
	form_data.append("file", document.getElementById('file').files[0]);
	var objArr = [];
 	objArr.push({"inspection":Inspection, "date_of_inspection": InspectionDate,"comments":Comments,"admin":ADMIN,"is_followUp":is_followUp,"followUp_date":followUp_date,"LocationID":LocationID,"InspectorName":InspectorName}); 
	form_data.append('objArr', JSON.stringify( objArr ));
	
 	if(InspectorName!="" && Inspection!="-Select-" && InspectionDate!="" && LocationID!=0 && LocationID!="-Select-" && (is_followUp=="false" && followUp_date=="" || is_followUp=="true" && followUp_date!=""  && LocationID!="-Select-" ))
	{ 
 		loading();
  	$.ajax({
		type: "POST",  
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
  		url: "api.php?action=Add_inspection", 
		success:function(response)
		{
			// console.log(response);	
  			 if(response=='"success"')
			 {
				  getAll_inspection();
 				 // console.log("success");
				  SAlert("Record has been added.");
					$("#InspectorName").val("");
					$("#Inspection").val("-Select-");
					$("#Comments").val("");
					$("#InspectionDate").val("");
					$("#switchDate").val("");
					$("#Restaurantdropdwon").val("-Select-");
					$("#Restaurantdropdwon").val(0);
					

					imgUploadRemove();
				  
			 } else{Cloading();}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
	}
	else{ DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
}


function getAll_inspection()
{   
loading();
 
 	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
 		url: "api.php?action=getAll_inspection", 
		success:function(response)
		{Cloading();
			// console.log(response);
  			 if(response!='"No Records"')
			 {
			 	$("#inspectionTable").dataTable().fnDestroy();
				 response=JSON.parse(response);
				 console.log(response);
				 document.getElementById("ListInspections").innerHTML ="";
				 for(var i=0;i<response.length;i++)
				 {

					 gatDateInspection=response[i]['date_of_inspection']
					 dateFormate(gatDateInspection);
					 
					 document.getElementById("ListInspections").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
				    <td align="center">'+response[i]['InspectorName']+'</td>\
					<td align="center">'+response[i]['inspection']+'</td>\
					<td align="center">'+response[i]['restaurant_name']+'</td>\
					<td align="center">'+finalJoiningDate+'</td>\
				<td align="center" class="p-0 colorGreen"><span class="cursor" onClick="emailPupop('+response[i]['id']+',\'inspection\')">Email</span> | <span class="cursor" onClick="get_inspection('+response[i]['id']+')">View</span> | <span   class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\
					</tr>';
				 }
				 DatatableInit3();
   			 }else{ InfoAlert("No records found.");Cloading();document.getElementById("ListInspections").innerHTML="";}
   		}, 
		error: function (xhr, status)
		{  
			// console.log(xhr + "--" + status);
			Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}	 
 

function delete_inspection()
{   
	loading();
	$.ajax({
		type: "POST",
 		data:{inspID:DeleteID},
		dataType: 'text',
 		url: "api.php?action=delete_inspection", 
		success:function(response)
		{  	
			// console.log(response);
			 if(response=='"success"')
			 {
 				 getAll_inspection();
				 closeDelPupop();
				 SAlert("Record has been deleted!");
			 }
			 else{ DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	}
   		}, 
		error: function (xhr, status) 
		{  
			// console.log("error");
			DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	
   		}
 	});
}




function get_inspection(id)
{ 

$("#InspectionamePOP").val("");
$("#InspectionIDPOP").val("");
$("#InspectionPOP").val("");
$("#CommentsPOP").val("");
$("#InspectionDatePOP").val("");
// $("#EmployeeSelectPOP").val("");
$("#switchAfterDatepop2").val("");
$("#Dlink").html("");
$("#noYesSwitchpop").attr('aria-pressed','false');
 loading();
   	$.ajax({
		type: "POST",  
 		url: "api.php?action=get_inspection", 
		data:{inspID:id},
		success:function(response)
		{	
		// console.log(response);
		Cloading();
  			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
 				 for(var i=0;i<response.length;i++) 
				 {
				 	var empname = (response[i]['empname']==null ? "(Deleted Employee)" : response[i]['empname']);
					$("#InspectionIDPOP").val(response[i]['id']);
					$("#InspectionPOP").val(response[i]['inspection']);
					$("#CommentsPOP").val(response[i]['comments']);
					
					date_of_inspection = new Date(response[i]['date_of_inspection']); // yyyy-MM-dd
					date_of_inspection = ("0" + (date_of_inspection.getMonth() + 1)).slice(-2) + '/' + ("0" + date_of_inspection.getDate()).slice(-2) + '/' + date_of_inspection.getFullYear();
					
					$("#InspectionDatePOP").val(date_of_inspection);
					
					
					
					$("#InspectionamePOP").val(response[i]['InspectorName']);
					$('#Restaurantdropdwonpopup').val(response[i]['LocationID']);
					
					if(response[i]['is_followUp']=='true')
					{
 						$("#noYesSwitchpop").attr('aria-pressed',''+response[i]['is_followUp']+'');
  						$("#noYesSwitchpop").addClass("active");
  						//$("#switchAfterDatepop").val(''+response[i]['followUp_date']+'');
						followUp_date = new Date(response[i]['followUp_date']); // yyyy-MM-dd
						followUp_date = ("0" + (followUp_date.getMonth() + 1)).slice(-2) + '/' + ("0" + followUp_date.getDate()).slice(-2) + '/' + followUp_date.getFullYear();
						$("#switchAfterDatepop").val(followUp_date);
					
					
						$("#switchAfterDatepop").removeAttr('disabled');
					}
					else
					{
 						$("#noYesSwitchpop").attr('aria-pressed',''+response[i]['is_followUp']+'');
						$("#switchAfterDatepop").val("");
						$("#noYesSwitchpop").removeClass("active");
						$("#switchAfterDatepop").attr('disabled','disabled');
					} 					
					 
 if(response[i]['insp_file_path']!="")
 {
	$("#Dlink").html('<a class="btn borderInputgreen bg-green text-left d-block" href="'+response[i]['insp_file_path']+'" download>View Attachment<i class="fa fa-eye pull-right font-size22px" aria-hidden="true"></i></a>');  
}
   					  //openPupop(); 
					  $("#EditInspecModal").modal();
					  
 				 }
  			 }else{  }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}



function Update_inspection()
{ 

 	InspectionIDPOP			= $("#InspectionIDPOP").val();
	InspectionPOP			= $("#InspectionPOP").val();
	CommentsPOP				= $("#CommentsPOP").val();
	InspectionDatePOP		= $("#InspectionDatePOP").val();
	LocationID 				= $('#Restaurantdropdwonpopup').val();
	is_followUp				= $("#noYesSwitchpop").attr('aria-pressed');
	followUp_date			= "";

	if(is_followUp=="true" && $("#switchAfterDatepop").val()!="")
	{
		followUp_date = $("#switchAfterDatepop").val();
 	} 
	else
	{
		followUp_date = "";
		is_followUp="false"
 	} 
	
 	loading();
 	var form_data = new FormData();
	form_data.append("Ufile", document.getElementById('filePOP').files[0]);
	var data = [];
 	
	data.push({"inspID":InspectionIDPOP, "inspection":InspectionPOP, "date_of_inspection":InspectionDatePOP,"comments":CommentsPOP,"is_followUp":is_followUp,"followUp_date":followUp_date, "LocationID":LocationID});
	form_data.append('data', JSON.stringify(data));
 
 	
	
	if(InspectionDatePOP!="" && LocationID !="0")
	{
	$.ajax({
		type: "POST",
 		data:form_data,
		contentType: false,
		cache: false,
		processData: false,
 		url: "api.php?action=Update_inspection", 
 		success:function(response)
		{
 			 // console.log(response);
			 if(response=='"success"')
			 {
				 //closePupop();
				  $("#EditInspecModal").modal('hide');
 				 getAll_inspection();
				 SAlert("Record has been updated.");
			 	//window.location.href = "dashboard.php";
			 }
			 else{ Cloading();}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			//DSAlert("Unable to proceed, please try again.","margin-top0px");
		$("#updinspect").fadeIn("slow");
		$("#updinspect").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Unable to proceed, please try again.");
		setTimeout(function(){ $("#updinspect").fadeOut("slow"); }, 4000);
   		}
 	});
	
	}
	else{
		
		Cloading();
		//DSAlert("Please fill-in all the mandatory fields.","margin-top0px");
		$("#updinspect").fadeIn("slow");
		$("#updinspect").html("<i class='fa fa-exclamation-circle font-size22px pull-left'></i>Please fill-in all the mandatory fields.");
		setTimeout(function(){ $("#updinspect").fadeOut("slow"); }, 4000);
		
		}
	
}


 
var NotifyTbl="";
var LP30Arr=[],LP60Arr=[],LP90Arr=[],LPPastArr=[];
var C30Arr=[],C60Arr=[],C90Arr=[],CPastArr=[];
var IN30Arr=[],IN60Arr=[],IN90Arr=[],INPastArr=[];

function notifications()
{
	
	var tbl="";
	var no=1;
	$("#notification").html("");
	$("#reports").html("");
	
	 for(var i=0;i<LPPastArr.length;i++)
	 {
	
	     var empname = (LPPastArr[i]['empname']==null ? "-" : LPPastArr[i]['empname']);
	     dateFormate(LPPastArr[i]['ExpirationDate']);
		 tbl+='<tr><td align="center">'+no+'</td><td align="center">'+LPPastArr[i]['doctype']+'</td><td align="center">'+empname+'</td><td align="center">'+LPPastArr[i]['LocationName']+'</td><td align="center">'+LPPastArr[i]['license']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color1"></td></tr>';
		 no++;
	 } 
	 	 
	 for(var i=0;i<CPastArr.length;i++)
	 {
		 
		 var empname = (CPastArr[i]['empname']==null || CPastArr[i]['empname']=="null"  ? "-" : CPastArr[i]['empname']);
		 var LOC = (CPastArr[i]['LocationName']==null || CPastArr[i]['LocationName']=="null"  ? CPastArr[i]['LocationName']=$("#hiddenres_dash").val() : CPastArr[i]['LocationName']);
		 
		 dateFormate(CPastArr[i]['exp_date']);
		 tbl+='<tr><td align="center">'+no+'</td><td align="center">'+"Certificate"+'</td><td align="center">'+empname+'</td><td align="center">'+LOC+'</td><td align="center">'+CPastArr[i]['certification']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color1"></td></tr>'; no++;
	 }	
	 	 
	 for(var i=0;i<INPastArr.length;i++)
	 {
	// var empname = (INPastArr[i]['empname']==null ? "(Deleted Employee)" : INPastArr[i]['empname']);

 		   dateFormate(INPastArr[i]['followUp_date']);
		 tbl+='<tr><td align="center">'+no+'</td><td align="center">'+"Inspection"+'</td><td align="center">'+INPastArr[i]['inspection']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color1"></td></tr>';
		 no++;
	 }	
 	 
	 for(var i=0;i<LP30Arr.length;i++)
	 {
	// var empname = (LP30Arr[i]['empname']==null ? "(Deleted Employee)" : LP30Arr[i]['empname']);
		  dateFormate(LP30Arr[i]['ExpirationDate']);
		  EMP=LP30Arr[i]['empname'];
		  if(LP30Arr[i]['empname'] == null || LP30Arr[i]['empname'] ==""){ EMP="-"; }
		  tbl+='<tr><td align="center">'+no+'</td><td align="center">'+ LP30Arr[i]['doctype'] +'</td><td align="center">'+ EMP +'</td><td align="center">'+ LP30Arr[i]['LocationName'] +'</td><td align="center">'+LP30Arr[i]['license']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color2"></td></tr>';no++;
	 }	
	 
	 for(var i=0;i<C30Arr.length;i++)
	 {
		 var empname = (C30Arr[i]['empname']==null || C30Arr[i]['empname']=="null"  ? "-" : C30Arr[i]['empname']);
		 var LOC = (C30Arr[i]['LocationName']==null || C30Arr[i]['LocationName']=="null"  ? C30Arr[i]['LocationName']=$("#hiddenres_dash").val() : C30Arr[i]['LocationName']);
		 
 		 dateFormate(C30Arr[i]['exp_date']);
		 tbl+='<tr><td align="center">'+no+'</td><td align="center">'+"Certificate"+'</td><td align="center">'+empname+'</td><td align="center">'+LOC+'</td><td align="center">'+C30Arr[i]['certification']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color2"></td></tr>';no++;
	 }	
 
 	 for(var i=0;i<IN30Arr.length;i++)
	 {
		  // var empname = (IN30Arr[i]['empname']==null ? "(Deleted Employee)" : IN30Arr[i]['empname']);
		  dateFormate(IN30Arr[i]['followUp_date']);
		 tbl+='<tr><td align="center">'+no+'</td><td align="center">'+"Inspection"+'</td><td align="center">'+IN30Arr[i]['inspection']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color2"></td></tr>';no++;
	 }	
	 
	 
	 for(var i=0;i<LP60Arr.length;i++)
	 {
		  // var empname = (LP60Arr[i]['empname']==null ? "(Deleted Employee)" : LP60Arr[i]['empname']);
		EMP=LP60Arr[i]['empname'];
		if(LP60Arr[i]['empname'] == null || LP60Arr[i]['empname'] ==""){ EMP="-"; }
		dateFormate(LP60Arr[i]['ExpirationDate']);
		tbl+='<tr><td align="center">'+no+'</td><td align="center">'+LP60Arr[i]['doctype']+'</td><td align="center">'+ EMP +'</td><td align="center">'+ LP60Arr[i]['LocationName'] +'</td></td><td align="center">'+LP60Arr[i]['license']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color3"></td></tr>';no++;
	 }
	 
	 for(var i=0;i<C60Arr.length;i++)
	 {
		 var empname = (C60Arr[i]['empname']==null || C60Arr[i]['empname']=="null"  ? "-" : C60Arr[i]['empname']);
		 var LOC = (C60Arr[i]['LocationName']==null || C60Arr[i]['LocationName']=="null"  ? C60Arr[i]['LocationName']=$("#hiddenres_dash").val() : C60Arr[i]['LocationName']);
		 
		
 		  dateFormate(C60Arr[i]['exp_date']);
 		  tbl+='<tr><td align="center">'+no+'</td><td align="center">'+"Certificate"+'</td><td align="center">'+empname+'</td><td align="center">'+LOC+'</td><td align="center">'+C60Arr[i]['certification']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color3"></td></tr>';no++;
	 }
	 
	 for(var i=0;i<IN60Arr.length;i++)
	 {
		  // var empname = (IN60Arr[i]['empname']==null ? "(Deleted Employee)" : IN60Arr[i]['empname']);
		  dateFormate(IN60Arr[i]['followUp_date']);
		 tbl+='<tr><td align="center">'+no+'</td><td align="center">'+"Inspection"+'</td><td align="center">'+IN60Arr[i]['inspection']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color3"></td></tr>';no++;
	 }
	 
	 
	 for(var i=0;i<LP90Arr.length;i++)
	 {
		  // var empname = (LP90Arr[i]['empname']==null ? "(Deleted Employee)" : LP90Arr[i]['empname']);
		  EMP=LP90Arr[i]['empname'];
		 if(LP90Arr[i]['empname'] == null || LP90Arr[i]['empname'] ==""){ EMP="-"; }
		  dateFormate(LP90Arr[i]['ExpirationDate']);
		 tbl+='<tr><td align="center">'+no+'</td><td align="center">'+LP90Arr[i]['doctype']+'</td><td align="center">'+ EMP +'</td><td align="center">'+LP90Arr[i]['LocationName']+'</td><td align="center">'+LP90Arr[i]['license']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color4"></td</tr>';no++;
	 }
 	 
	 for(var i=0;i<C90Arr.length;i++)
	 {
		  
		 var empname = (C90Arr[i]['empname']==null || C90Arr[i]['empname']=="null"  ? "-" : C90Arr[i]['empname']);
		 var LOC = (C90Arr[i]['LocationName']==null || C90Arr[i]['LocationName']=="null"  ? C90Arr[i]['LocationName']=$("#hiddenres_dash").val() : C90Arr[i]['LocationName']);
		 
 		  dateFormate(C90Arr[i]['exp_date']);
		 tbl+='<tr><td align="center">'+no+'</td><td align="center">'+"Certificate"+'</td><td align="center">'+empname+'</td><td align="center">'+LOC+'</td><td align="center">'+C90Arr[i]['certification']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color4"></td></tr>';no++;
	 }
	 
	 
	 for(var i=0;i<IN90Arr.length;i++)
	 {
		 // var empname = (IN90Arr[i]['empname']==null ? "(Deleted Employee)" : IN90Arr[i]['empname']);
		  dateFormate(IN90Arr[i]['followUp_date']);
		 tbl+='<tr><td align="center">'+no+'</td>><td align="center">'+IN90Arr[i]['LocationName']+'</td><td align="center">'+IN90Arr[i]['inspection']+'</td><td align="center">'+finalJoiningDate+'</td><td class="color4"></td</tr>';no++;
	 }
	 
	$("#notification").html(tbl);
	$("#reports").html(tbl);
 } 
 

function DashboardTxt()
{
	var LP30 	= Number($("#LP30").html());	
	var LP60 	= Number($("#LP60").html());	
	var LP90 	= Number($("#LP90").html());	
	var LPPast  = Number($("#LPPast").html());	
	var C30 	= Number($("#C30").html());	
	var C60 	= Number($("#C60").html());	
	var C90 	= Number($("#C90").html());	
	var CPast30 = Number($("#CPast30").html());	
	
	if(LP30>1){$("#LP30txt").html("Permits/Licenses Are Due For Renewal")}
		else{$("#LP30txt").html("Permit/License is Due For Renewal")}
	if(LP60>1){$("#LP60txt").html("Permits/Licenses Are Due For Renewal")}
		else{$("#LP60txt").html("Permit/License is Due For Renewal")}
 	if(LP90>1){$("#LP90txt").html("Permits/Licenses Are Due For Renewal")}
		else{$("#LP90txt").html("Permit/License is Due For Renewal")}
		
	if(LPPast>1){$("#LPPasttxt").html("Permits/Licenses Have Passed their Renewal Dates")}
		else{$("#LPPasttxt").html("Permit/License Has Passed Its Renewal Date")}

	if(C30>1){$("#C30txt").html("Certificates Are Due For <br>Renewal")}
		else{$("#C30txt").html("Certificate is Due For <br>Renewal")}
	if(C60>1){$("#C60txt").html("Certificates Are Due For <br>Renewal")}
		else{$("#C60txt").html("Certificate is Due For <br>Renewal")}
 	if(C90>1){$("#C90txt").html("Certificates Are Due For <br>Renewal")}
		else{$("#C90txt").html("Certificate is Due For <br>Renewal")}
		
	if(CPast30>1){$("#CPast30txt").html("Certificates Have Passed their Renewal Dates")}
		else{$("#CPast30txt").html("Certificate Has Passed Its Renewal Date")}
		
		

}


var GetDate=""; 
function todayDate()
{
	$("#reports").html("");  
	$.ajax({
		type: "POST",  
 		url: "api.php?action=todayDate", 
		success:function(response)
		{ 
			GetDate = response;
			//console.log(GetDate)
			 D_certification();
			 D_license_permit();
			// D_inpection();
		},
		error: function (xhr, status) 
		{  
   		}
 	});
}

function D_license_permit()
{   
var LP30=0,LP60=0,LP90=0,LPPast=0;
  var today = new Date(GetDate);
  
 loading();
  	$.ajax({
		type: "POST", 
		data:{admin:ADMIN}, 
 		url: "api.php?action=D_license_permit", 
		success:function(response)
		{
			 Cloading();
   			 if(response!='"No Records"')
			 {
				
				 response=JSON.parse(response);
				  //console.log(response);
				 var notify="";
				 for(var i=0;i<response.length;i++)
				 { 
 						var expiryDate= response[i]['ExpirationDate'];
						expiryDate = new Date(expiryDate.substr(0, 4),month=(expiryDate.substr(5, 2))-1,expiryDate.substr(8, 3));
						if(today<expiryDate)
						{
							var DateDifference = Math.floor((expiryDate - today) / (1000*60*60*24));
							//console.log("remaining"+DateDifference+" /"+response[i]['ExpirationDate']);
							if(DateDifference>=0 && DateDifference<31)
							{
								$("#LP30").html(++LP30);
								LP30Arr.push(response[i]);
							}
							if(DateDifference>30 && DateDifference<61)
							{
								$("#LP60").html(++LP60);
								LP60Arr.push(response[i]);
							}
							if(DateDifference>60 && DateDifference<91)
							{
								$("#LP90").html(++LP90);
								LP90Arr.push(response[i]);
							}
						}
						else
						{
							var DateDifference = Math.floor((today - expiryDate) / (1000*60*60*24));
 							$("#LPPast").html(++LPPast);
							LPPastArr.push(response[i]);
							LPPastArr1.push(response[i]);
							
						}
				 }
				 DashboardTxt();
				 notifications();
				 Reports();
   			 } 
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}	 



function D_certification()
{   
var C30=0,C60=0,C90=0,CPast30=0;
   var today = new Date(GetDate);
 loading();
  	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
 		url: "api.php?action=D_certification", 
		success:function(response)
		{
			//console.log(response);
		Cloading();
   			 if(response!='"No Records"')
			 {
				 response=JSON.parse(response);
				 console.log(response);
				 Cloading();
				 for(var i=0;i<response.length;i++)
				 {
 						var expiryDate= response[i]['exp_date'];
						expiryDate = new Date(expiryDate.substr(0, 4),month=(expiryDate.substr(5, 2))-1,expiryDate.substr(8, 3));
						if(today<expiryDate)
						{
							var DateDifference = Math.floor((expiryDate - today) / (1000*60*60*24));
 							if(DateDifference>=0 && DateDifference<31)
							{
								$("#C30").html(++C30);
								C30Arr.push(response[i]); 
							}
							if(DateDifference>30 && DateDifference<61)
							{
								$("#C60").html(++C60);
								C60Arr.push(response[i]);
							}
							if(DateDifference>60 && DateDifference<91)
							{
								$("#C90").html(++C90);
								C90Arr.push(response[i]);
							}
						}
						else
						{
							var DateDifference = Math.floor((today - expiryDate) / (1000*60*60*24));
 							$("#CPast30").html(++CPast30);
							CPastArr.push(response[i]);
							CPastArr1.push(response[i]);
						}
						
				 }
				 DashboardTxt();
				 notifications();
				 Reports();
   			 } 
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}	 

//var IN30Arr=[],IN60Arr=[],IN90Arr=[],INPastArr=[];
function D_inpection()
{
var IN30=0,IN60=0,IN90=0,INPast30=0;
   var today = new Date(GetDate);
 //loading();
  	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
 		url: "api.php?action=D_inpection",
		success:function(response)
		{ 
 		//Cloading();
			// console.log(response);
   			 if(response!='"No Records"')
			 {
				response=JSON.parse(response);
				Cloading();
				for(var i=0;i<response.length;i++)
				 { 
 						var expiryDate= response[i]['followUp_date'];
						expiryDate = new Date(expiryDate.substr(0, 4),month=(expiryDate.substr(5, 2))-1,expiryDate.substr(8, 3));
						if(today<expiryDate)
						{
							var DateDifference = Math.floor((expiryDate - today) / (1000*60*60*24));
 							if(DateDifference>=0 && DateDifference<31)
							{
 								IN30Arr.push(response[i]); 
							}
							if(DateDifference>30 && DateDifference<61)
							{
 								IN60Arr.push(response[i]);
							}
							if(DateDifference>60 && DateDifference<91)
							{
 								IN90Arr.push(response[i]);
							}
						}
						else
						{
							var DateDifference = Math.floor((today - expiryDate) / (1000*60*60*24));
 							INPastArr.push(response[i]);
						}
 				 }
				 DashboardTxt();
				 notifications();
				 Reports();
   			 } 
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}	 


function getProfile()
{   
loading();
$("#firstName").val($.cookie("fname"));
$("#lastName").val($.cookie("lname"));
$("#email").val($.cookie("email"));
		
 	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
 		url: "api.php?action=getProfile", 
		success:function(response)
		{
			Cloading();
   			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
 				 for(var i=0;i<response.length;i++)
				 { 
  					$.cookie("fname", response[0]['fname']);
 					$.cookie("lname", response[0]['lname']);
					$.cookie("email", response[0]['email']);
 				 }
   			 }
			 else{ InfoAlert("No records found.");Cloading();}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}	 
 

function UpdateProfile()
{   
 	firstName	=	$("#firstName").val();
	lastName	=	$("#lastName").val();
	email		=	$("#email").val();
	if(firstName!="" && lastName!="" && email!="" && validateEmail(email)!=false)
	{
		loading(); 	
		$.ajax({
			type: "POST",  
			data:{admin:ADMIN,firstName:firstName,lastName:lastName,email:email},
			url: "api.php?action=UpdateProfile", 
			success:function(response)
			{
				Cloading();
				 if(response=='"success"')
				 {
					 SAlert("Profile has been updated.");
					 
						$.cookie("fname", $("#firstName").val());
						$.cookie("lname", $("#lastName").val());
						$.cookie("email", $("#email").val());
				 username();
				 }
			}, 
			error: function (xhr, status) 
			{  Cloading();
				DSAlert("Unable to proceed, please try again.","margin-top0px");
			}
		});
	}
	else{DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
}	 
 


function UpdatePassword()
{ 


   
   	oldPass		=	$("#oldPass").val();
	newPass		=	$("#newPass").val();
	confirmPass =	$("#confirmPass").val();
	
	if(oldPass!="" && newPass!="" && confirmPass!="" && newPass==confirmPass && checkPassword(newPass)==true)
	{
		loading(); 	
		$.ajax({
			type: "POST",  
			data:{admin:ADMIN,newPass:newPass,oldPass:oldPass},

			url: "api.php?action=UpdatePassword", 
			success:function(response)
			{
				Cloading();
				 if(response=='"success"')
				 {
					SAlert("Password has been updated.");
					$("#oldPass").val("");
					$("#newPass").val("");
					$("#confirmPass").val("");
					}
				 else
				 {
					 DSAlert("Password has not been updated.","margin-top0px");
				 }
			}, 
			error: function (xhr, status) 
			{  Cloading();
				DSAlert("Unable to proceed, please try again.","margin-top0px");
			}
		});
	}
	else{
		
		if(oldPass=="" || newPass=="" || confirmPass==""){
		  DSAlert("Please fill-in all the mandatory fields.","margin-top0px");
		  Cloading();
		}
		
		if(newPass!=confirmPass){
		  DSAlert("Password does not match.","margin-top0px");
		  Cloading();
		}
		
		if(checkPassword(newPass)!=true && newPass!=""){
			DSAlert("Password must be of minimum 8 characters containing a capital letter, a digit and a special character.","margin-top0px");
		    Cloading();
			}
		
		
		
		
		}
}	

function GetNumber(){
	var user_id = $.cookie("ID");
     $.ajax({
         type: "POST",
         data: {user_id:user_id},
         url: "api.php?action=getNumber",
         success: function(response) {
            if(response != '"No Records"'){
                var response = JSON.parse(response);
				//console.log(response);
                var activeChecker = response[0]['active'];
                $('#sms-number').text(response[0]['countrycode'] + response[0]['phone']);
                $('#editsmsbutton').attr('data-number', response[0]['phone']);
                $('#editsmsbutton').attr('data-country', response[0]['countrycode']);
                $('#editsmsbutton').attr('data-checker', '1');
                if(activeChecker == 'active'){
                    $('#smsReminderToggel').addClass('active');
                    $('#smsReminderToggel').attr('data-info', '1');
                }else{
                    $('#smsReminderToggel').removeClass('active');
                    $('#smsReminderToggel').attr('data-info', '0');
                }
				
				
            }else{
                $('#smsReminderToggel').prop('disabled', function(i, v) { return !v; });
                $('#sms-number').text("No Number Found");
            }
         },
         error: function(xhr, status, errorThrown) {
             DSAlert("Unable to proceed, please try again.", "margin-top0px");
         }
     });
    // var user_id = $.cookie("ID");
    //  $.ajax({
    //      type: "POST",
    //      data: {user_id:user_id},
    //      url: "api.php?action=getNumber",
    //      success: function(response) {
    //         if(response != '"No Records"'){
    //             var response = JSON.parse(response);
    //             console.log(response);
    //             var activeChecker = response[0]['active'];
    //             $('#sms-number').text(response[0]['countrycode'] + response[0]['phone']);
    //             $('#editsmsbutton').attr('data-number', response[0]['phone']);
    //             $('#editsmsbutton').attr('data-country', response[0]['countrycode']);
    //             $('#editsmsbutton').attr('data-checker', '1');
    //             if(activeChecker == 'active'){
    //                 $('#smsReminderToggel').addClass('active');
    //             }else{
    //                 $('#smsReminderToggel').removeClass('active');
    //             }
    //         }else{
    //         	$('#smsReminderToggel').prop('disabled', function(i, v) { return !v; });
    //             $('#sms-number').text("No Number Found");
    //         }
    //         console.log(response);
    //      },
    //      error: function(xhr, status, errorThrown) {
    //          DSAlert("Unable to proceed, please try again.", "margin-top0px");
    //      }
    //  });
}


function AddNotifyEmail()
{   
 	NotifyEmail1	=	$("#NotifyEmail").val(); 
 	fname2	=	$("#fname").val(); 
 	lname2	=	$("#lname").val(); 
	if(NotifyEmail1!="" && validateEmail(NotifyEmail1)!=false && fname2!="" && lname2!="")
	{
		loading(); 	
		$.ajax({
			type: "POST", 
			data:{admin:ADMIN,email:NotifyEmail1,fname:fname2,lname:lname2},
			url: "api.php?action=AddNotifyEmail", 
			success:function(response)
			{
					
				$("#NotifyEmail").val(""); 
				$("#fname").val(""); 
				$("#lname").val("");
  				if(response=='"success"')
				 {
				   // closePupop();
				    $("#addEmlModal").modal('hide');
					SAlert("Record has been added.");
					GetNotifyEmail(); 
				 }
				 else if(response=='"Exist"')
				 {
					//closePupop();
					$("#addEmlModal").modal('hide');
      				DSAlert("Email already exist.");
					Cloading();
 				 }
				 else
				 {
					 Cloading();
				 }
			}, 
			error: function (xhr, status) 
			{  Cloading();
				DSAlert("Unable to proceed, please try again.","margin-top0px");
			}
		});
	}
	else{
		if(NotifyEmail1=="" || fname2=="" || lname2==""){
			$("#notifyEmailPopUp").fadeIn("");
			$("#notifyEmailPopUp").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Please fill-in all the mandatory fields.");
			$("#notifyEmailPopUp").removeClass("alert-success").addClass("alert alert-danger");
			setTimeout(function(){ $("#notifyEmailPopUp").fadeOut("slow"); }, 4000);
			return;
			
			}
		if(validateEmail(NotifyEmail1)==false){
			$("#notifyEmailPopUp").fadeIn("");
			$("#notifyEmailPopUp").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Please enter a valid email address.");
			$("#notifyEmailPopUp").removeClass("alert-success").addClass("alert alert-danger");
			setTimeout(function(){ $("#notifyEmailPopUp").fadeOut("slow"); }, 4000);
			return;
			
			}	
			
		
		 /*DSAlert("Please fill-in all the mandatory fields.","margin-top0px");*/
		 Cloading();
		
		
		
		}
}	 
 


function GetNotifyEmail()
{  
loading();
 	$.ajax({
		type: "POST", 
		data:{admin:ADMIN},
 		url: "api.php?action=GetNotifyEmail", 
		success:function(response)
		{
			//console.log(response);
			Cloading();
  			 if(response!='"No Records"')
			 {
				 document.getElementById("AllNotifyEmail").innerHTML ="";
				 response=JSON.parse(response);
				 for(var i=0;i<response.length;i++)
				 { 
  					 document.getElementById("AllNotifyEmail").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
					<td align="center">'+response[i]['fname']+'</td>\
					<td align="center">'+response[i]['lname']+'</td>\
					<td align="center">'+response[i]['email']+'</td>\
					<td align="center" class="p-0 colorGreen"><span class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\
					</tr>';
				 } 				 
  			 }else{ //InfoAlert("No records found.");
			 document.getElementById("AllNotifyEmail").innerHTML="";}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			 
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}


function DeleteNotifyEmail()
{  
loading();
	$.ajax({
		type: "POST",
 		data:{DeleteID:DeleteID},
		dataType: 'json',
 		url: "api.php?action=DeleteNotifyEmail", 
		success:function(response)
		{
  			 if(response=='success')
			 {
 				 GetNotifyEmail();
 				 SAlert("Record has been deleted!");
				  closeDelPupop();
 			 }
			 else{ DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	}
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	
   		}
 	});
}


function SendEmail()
{
	var jsonString = "";
	name = $.cookie('fname');
	Fromemail = $.cookie('email');
	var chkArray = [];
	$(".chk:checked").each(function() {
		chkArray.push($(this).val());
	});
	if($("#emaiTo").val() != ""){
		chkArray.push($("#emaiTo").val());
	}else if(($("#emaiTo").val() == "") && (chkArray.length == 0)){
		$("#emailError").fadeIn("slow");
		$("#emailError").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Please Select Email Address");
		setTimeout(function(){ $("#emailError").fadeOut("slow"); }, 4000);
		Cloading(); 
		return;
	}
	jsonString = JSON.stringify(chkArray);
		loading();
		$.ajax({
			type: "POST",
	 		data:{emailID:emailID,doc:docType,admin:name,arrayEmail:jsonString,Fromemail2:Fromemail},
			dataType: 'json',
			cache: false,
	 		url: "email/sendEmail.php?action=emailID", 
			success:function(response)
			{
				
	  			 if(response=='success')
				 {
	 				  SAlert("Email sent successfully!");
					  closePupop();
					  Cloading();
					  $("#emaiTo").val("");	
					  $("#emaiCC").val("");	
					  $("#emaiBC").val("");	
					  
	 			 }
				 else{ 
				   
						$("#emailError").fadeIn("slow");
						$("#emailError").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Please enter a valid email address.");
						setTimeout(function(){ $("#emailError").fadeOut("slow"); }, 4000);
						Cloading();
					}
	   		}, 
			error: function (xhr, status) 
			{ 
						$("#emailError").fadeIn("slow");
						$("#emailError").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Please enter a valid email address.");
						setTimeout(function(){ $("#emailError").fadeOut("slow"); }, 4000);
						Cloading();	
			}
	 	});
	
	
}

function Vendors(){
	loading(); 	
	if(document.getElementById("excel_file").value != "") {AddVendors1();}
	else{AddVendor();}
}








function AddVendor()
{   
 	FirstName	=	$("#FirstName").val();  
 	/*LastName	=	$("#LastName").val(); */ 
 	EmailAddress=	$("#EmailAddress").val();  
 	PhoneNumber	=	$("#PhoneNumber").val();  
 	CompanyName	=	$("#CompanyName").val();  
 	Expertise	=	$("#Expertise").val();  
 
 	if(FirstName!=""  && EmailAddress!="" && PhoneNumber!="" &&  Expertise!="" && CompanyName!="" && validateEmail(EmailAddress)!=false)
	{
		loading(); 	
		$.ajax({
			type: "POST", 
			data:{admin:ADMIN,FirstName:FirstName,EmailAddress:EmailAddress,PhoneNumber:PhoneNumber,CompanyName:CompanyName,Expertise:Expertise},
			url: "api.php?action=AddVendor", 
			success:function(response)
			{
    			if(response=='"success"')
				{
 				 	GetAllVendor();
 					SAlert("Record has been added.");
					$("#FirstName").val("");  
					$("#EmailAddress").val("");  
					$("#PhoneNumber").val("");  
					$("#CompanyName").val("");  
					$("#Expertise").val(""); 

				 } 
				 else
				 {
					 Cloading();
				 }
			}, 
			error: function (xhr, status) 
			{  Cloading();
				DSAlert("Unable to proceed, please try again.","margin-top0px");
			}
		});
	}
	else{ 
	   
	   if(!validateEmail(EmailAddress) && EmailAddress!="" ){DSAlert("Please enter a valid email address.","margin-top0px");Cloading();}
	   else{DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
	
	}
}	 
 




function GetAllVendor()
{  
loading();
 	$.ajax({
		type: "POST", 
		data:{admin:ADMIN},
 		url: "api.php?action=GetAllVendor", 
		success:function(response)
		{
			//console.log(response);
			Cloading();

  			 if(response!='"No Records"')
			 {
			 	$("#vendorTable").dataTable().fnDestroy();
				 document.getElementById("AllVendorsData").innerHTML ="";
				 response=JSON.parse(response);
				 for(var i=0;i<response.length;i++)
				 { 
  					document.getElementById("AllVendorsData").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
					<td align="center">'+response[i]['FirstName']+'</td>\
 					<td align="center">'+response[i]['EmailAddress']+'</td>\
					<td align="center">'+response[i]['PhoneNumber']+'</td>\
					<td align="center">'+response[i]['CompanyName']+'</td>\
					<td align="center">'+response[i]['Expertise']+'</td>\
					<td align="center" class="p-0 colorGreen"><span class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\</tr>';
				 }
				  DatatableInit4();			 
  			 }
  			 else{ 
  			 	// $("#vendorTable").dataTable().fnDestroy();
  			 	InfoAlert("No records found.");
			 document.getElementById("AllVendorsData").innerHTML="";
			}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			 
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}






function DeleteVendor()
{  
    loading();
	$.ajax({
		type: "POST",
 		data:{DeleteID:DeleteID},
		dataType: 'json',
 		url: "api.php?action=DeleteVendor", 
		success:function(response)
		{
  			 if(response=='success')
			 {
 				 GetAllVendor();
 				 SAlert("Record has been deleted!");
				 closeDelPupop();
 			 }
			 else{ DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	}
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	
   		}
 	});
}


function AddTasks(){
	loading(); 	
	if(document.getElementById("excel_file").value != "") {AddTask1();}
	else{AddTask();}
}



var today;
function AddTask()
{  

today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today =  mm+ '/' + dd + '/' + yyyy;



 	DateOfTask = today;  
 	TaskType = $("#TaskType").val();  
 	TaskPriority = $("#TaskPriority").val();  
 	//DateOfTaskCompletion = $("#DateOfTaskCompletion").val();  
	
 	 
 
 	if(TaskType!="" && TaskPriority!="") {
		loading(); 	
		$.ajax({
			type: "POST", 
			data:{admin:ADMIN,DateOfTask:DateOfTask,TaskType:TaskType,TaskPriority:TaskPriority},
			url: "api.php?action=AddTask", 
			success:function(response)
			{
				//console.log(response);
    			if(response=='"success"')
				{
 				 	GetAlltask();
 					SAlert("Record has been added.");
					/*$("#DateOfTask").val("");  */
					$("#TaskType").val("");  
					$("#TaskPriority").val("");  
					/*$("#DateOfTaskCompletion").val("");  */
					 Cloading();
					 

				 } 
				 else
				 {
					 Cloading();
				 }
			}, 
			error: function (xhr, status) 
			{  Cloading();
				DSAlert("Unable to proceed, please try again.","margin-top0px");
			}
		});
	}
	else{DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
}	 
function DatatableInit2()
{
 	$('#myTable').DataTable({  
	"ordering": false,
	"paging": false,
	"searching": false,
	   "bInfo" : false,
 		dom: 'lBfrtip',
 		buttons: [
        {
            extend: 'csvHtml5',
             exportOptions: {
               columns: [0,1,2,3,4,5,6,7,8,9,10]
            }
        }
    ]
	});
	
  	$(".buttons-csv").addClass("btn borderInputgreen bg-green col-2 pull-right");
 	$(".buttons-csv").html("<span>EXPORT IN EXCEL</span>");

} 

function DatatableInit2_todo()
{
 	$('#myTabletodo').DataTable({  
	"ordering": false,
	"paging": false,
	"searching": false,
	   "bInfo" : false,
 		dom: 'lBfrtip',
 		buttons: [
        {
            extend: 'csvHtml5',
             exportOptions: {
               columns: [0,1,2,3,4]
            }
        }
    ]
	});
	
  	$(".buttons-csv").addClass("btn borderInputgreen bg-green col-2 pull-right");
 	$(".buttons-csv").html("<span>EXPORT IN EXCEL</span>");

}
function DatatableInit3()
{
 	$('#inspectionTable').DataTable({  
	"ordering": false,
	"paging": false,
	"searching": true,
	   "bInfo" : false,
 		dom: 'lBfrtip',
 		buttons: [
    ],
    language: {
    	searchPlaceholder: "Search",
		 search: "" 
    	},
	});

}
function DatatableInit4()
{
 	$('#vendorTable').DataTable({  
	"ordering": false,
	"paging": false,
	"searching": true,
	   "bInfo" : false,
 		dom: 'lBfrtip',
 		buttons: [
    ],
	 aaSorting: [[0, "asc"]],
    language: {
    	searchPlaceholder: "Search",
		 search: "" 
    	},
	});

} 


function DatatableInit5()
{
 	$('#CustomersTable').DataTable({  
	"ordering": false,
	"paging": false,
	"searching": true,
	   "bInfo" : false,
 		dom: 'lBfrtip',
 		buttons: [
    ],
	 aaSorting: [[0, "asc"]],
    language: {
    	searchPlaceholder: "Search",
		 search: "" 
    	},
	});

} 




function GetAlltask()
{ 
loading();
 	$.ajax({
		type: "POST", 
		data:{admin:ADMIN},

 		url: "api.php?action=GetAlltask", 
		success:function(response)
		{
			//console.log(response);
			Cloading();

  			 if(response!='"No Records"')
			 {
				 $("#myTabletodo").dataTable().fnDestroy();
				 document.getElementById("AllTaskData").innerHTML ="";
				 response=JSON.parse(response);
				 for(var i=0;i<response.length;i++)
				 { 
				 
				 
				   if(response[i]['Status']=="Pending"){
				 	document.getElementById("AllTaskData").innerHTML +='<tr>\
					<td align="center">'+response[i]['taskdate']+'</td>\
 					<td align="center">'+response[i]['tasktype'].substr(0,50)+'</td>\
					<td align="center">'+response[i]['taskpriority']+'</td>\
					<td align="center">'+response[i]['taskcompdate']+'</td>\
					<td align="center">'+response[i]['Status']+'</td>\
                    <td align="center" class="p-0 colorGreen">'+'<span class="cursor" onClick="gettask('+response[i]['id']+')">View</span> | <span class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\</tr>';
				   }
				   else{
					   
					document.getElementById("AllTaskData").innerHTML +='<tr>\
					<td align="center">'+response[i]['taskdate']+'</td>\
 					<td align="center">'+response[i]['tasktype'].substr(0,50)+'</td>\
					<td align="center">'+response[i]['taskpriority']+'</td>\
					<td align="center">'+response[i]['taskcompdate']+'</td>\
					<td align="center">'+response[i]['Status']+'</td>\
                    <td align="center" class="p-0 colorGreen">'+'<span class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\</tr>';
                       }

				 } 	
				 DatatableInit2_todo();			 
  			 }
  			 else{ 
			 $("#myTabletodo").dataTable().fnDestroy();
			 InfoAlert("No records found.");
			 document.getElementById("AllTaskData").innerHTML="";
			}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			 
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}

var TaskID;
function gettask(id)
{
   TaskID=id;
   loading();
	$("#TaskNamePOP").val("");
	
   	$.ajax({
		type: "POST",  
 		url: "api.php?action=GetTask", 
		data:{TaskID:TaskID},
		success:function(response)
		{
			//console.log(response)
			Cloading();
			
  			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
				// console.log(response);
				 for(var i=0;i<response.length;i++)
				 {
						$("#TaskNamePOP").val(response[i]['tasktype']);
						$("#TaskPriorityPop").val(response[i]['taskpriority']); 
						$("#TaskStatusPop").val(response[i]['Status']); 
						
					     //finishBox();
						 $("#EditTaskModal").modal();
 				 }
  			 }else{  InfoAlert("No records found."); }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");

   		}
 	});
	
}




function deletTask()
{  
loading();
	$.ajax({
		type: "POST",
 		data:{DeleteID:DeleteID},
		dataType: 'json',
 		url: "api.php?action=deletTask", 
		success:function(response)
		{
  			 if(response=='success')
			 {
 				 GetAlltask();
 				 SAlert("Record has been deleted!");
				  closeDelPupop();
 			 }
			 else{ DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	}
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	
   		}
 	});
}

/*********************************/
 var count = 60;
 var interval;
 var smsCode;

 function smsCodeTimer() {
     count = 60;
     var timer;
     $("#resendBtn").addClass("none").removeClass("block");
     $("#timecode").addClass("block").removeClass("none");
     interval = setInterval(Countdown, 1000);
 }

 function Countdown() {
     $("#timecode").val("0:" + count--);
     if (count == 0) {
         clearInterval(interval);
         count = 60;
         $("#timecode").val("0:60");
         $("#resendBtn").addClass("block").removeClass("none");
         $("#timecode").addClass("none").removeClass("block");
     };
 }


 function generateSmsCode() {
    var phoneNumber = $('#PhoneNumber').val();
    if(phoneNumber != ""){
     $("#userPhoneNumber").addClass("none").removeClass("block");
     $("#enterCodeScreen").addClass("block").removeClass("none");
     $("#resendBtn").addClass("none").removeClass("block");
     $("#timecode").addClass("block").removeClass("none");
     smsCode = Math.floor(100000 + Math.random() * 900000);
     sendSmsCode();
    }else{
        $("#notifyPhone").fadeIn("slow");
        $("#notifyPhone").addClass("alert alert-danger");
        $("#notifyPhone").html("<i class='fa fa-exclamation-circle font-size18px pull-left'></i>Please fill-in all the mandatory fields.");
        setTimeout(function(){ $("#notifyPhone").fadeOut("slow"); }, 4000);
    }
 }


 function sendSmsCode() {
     smsCodeTimer();
     $("#resendBtn").addClass("none").removeClass("block");
     $("#timecode").addClass("block").removeClass("none");

     CountryCode = $("#CountryCode").val();
     PhoneNumber = $("#PhoneNumber").val();
     userNumber = CountryCode + PhoneNumber;
     message = "Your complyance verification code is " + smsCode;
     $.ajax({
         type: "POST",
         crossDomain: true,
         data: {message: message, receiver_number	: userNumber},
         // url: "http://ec2-34-228-197-182.compute-1.amazonaws.com/MessageAPIs/api/Message/sendMessage?message=" + message + "&contact=" + userNumber + "",
        // url: "https://celeritas-solutions.com/cs/aws_sns/sms.php",
		url: "https://complyance.net/sms-notification/sendnotifications.php",
         success: function(response) {
           // console.log(response);
            $.cookie("number", PhoneNumber);
            $.cookie("smsReminders", "inactive");
            $.cookie("CountryCode", CountryCode);
            sendDataToDatabase();
        }
     });
 }

function sendDataToDatabase(){
   // var checker = $('#send-code-btn').attr('data-checker');
   var checker = $('#editsmsbutton').attr('data-checker');
	
	    if(checker == 1){
        var user_id = $.cookie("ID");
        var CountryCode = $("#CountryCode").val();
        var phone = $("#PhoneNumber").val();
        var userNumber = CountryCode + PhoneNumber;
        var code = smsCode;
        var active = $.cookie("smsReminders");
         $.ajax({
             type: "POST",
             data: {user_id:user_id, phone:phone, code:code, active:active, CountryCode:CountryCode},
             url: "api.php?action=updateSmsNotification",
             success: function(response) {
               // console.log(response);
             },
             error: function(xhr, status, errorThrown) {
                 DSAlert("Unable to proceed, please try again.", "margin-top0px");
             }
         }); 
    }else{
        var user_id = $.cookie("ID");
        var phone = $.cookie("number");
        var code = smsCode;
        var active = $.cookie("smsReminders");
        var CountryCode = $.cookie("CountryCode");
         $.ajax({
             type: "POST",
             data: {user_id:user_id, phone:phone, code:code, active:active, CountryCode:CountryCode},
             url: "api.php?action=saveSmsNotificationDetailsUnactive",
             success: function(response) {
                //console.log(response);
             },
             error: function(xhr, status, errorThrown) {
                 DSAlert("Unable to proceed, please try again.", "margin-top0px");
             }
         });
    }
}

 function matchCode() {
     var number = $("#verificationCode").val();
     if(number == smsCode){
        VerifyUserNumber();
        // $('#verificationCode').hide();
        // $('#timecodebox').hide();
        // $('#showactivation').show();
        // $('.disablePhoneNumber').hide();
     }
     // if (number == smsCode) {
     //      VerifyUserNumber();
     //     //Keyboard.hide();
     //     $("#number1").blur();
     // }
 }
 
 
function VerifyUserNumber() {
    $("#enterCodeScreen").addClass("none").removeClass("block");
     $("#smsNotificationSuccessBox").addClass("block").removeClass("none");
    var user_id = $.cookie("ID");
    var phone = $.cookie("number");
     $.cookie("smsReminders", "active");
    var code = smsCode;
    var active = 'active';
 // 	var storage = window.localStorage;
	// var OwnerID = storage.getItem('OwnerID');
	// var userNumber = storage.getItem('userNumber'); 
	// var smscode = storage.getItem('smscode'); 
	// active = "active";

    $.ajax({
        type: "POST",
        data: {user_id: user_id, phone:phone, code:code, active: active},
        url: "api.php?action=saveSmsNotificationDetailsActive",
        success: function(response) {
          //  console.log(response);
    //         if (response == "success") {
    //             storage.setItem('userNumber', userNumber);
    //             storage.setItem('smscode', smsCode);
    //             storage.setItem('numberactive', active);
				//  GetAllPets();
				// selectDogScreen();
 			// 	$("#number1").val(""); 
				
    //          } else {

    //         }
        },
        error: function(xhr, status, errorThrown) {
			CloseloadingScreen();
			PopupScreen("Error", "Unable to proceed, please try again.");
			$("#btxt").css("display", "block");
			$("#btntxt").html("Ok");
        }
    });
	
}
/*********************************/

function updatetask()
{
	
	var todayComplete = new Date();
	var dd = todayComplete.getDate();
	var mm = todayComplete.getMonth()+1; 
	var yyyy = todayComplete.getFullYear();
	
	if(dd<10) {
	dd = '0'+dd
	} 
	
	if(mm<10) {
	mm = '0'+mm
	} 
	
	todayComplete = mm + '/' + dd + '/' + yyyy;
	

   TaskNamePOP = $("#TaskNamePOP").val();
   TaskPriorityPop = $("#TaskPriorityPop").val();
   TaskStatusPop = $("#TaskStatusPop").val();
   TaskCompletion = todayComplete;
   
    if(TaskStatusPop!="Complete"){TaskCompletion="";}
	
	if(TaskNamePOP!=""){
	
	
	  loading(); 
		$.ajax({
		type: "POST",
 		data:{TaskID:TaskID,TaskNamePOP:TaskNamePOP,TaskPriorityPop:TaskPriorityPop,TaskStatusPop:TaskStatusPop,TaskCompletion:TaskCompletion},
		dataType: 'json',
 		url: "api.php?action=updatetask", 
		success:function(response)
		{
			 if(response=='success')
			 {
 				 GetAlltask();
				// closeFinishPupop();
				$("#EditTaskModal").modal('hide');
				 SAlert("Record has been updated.");
			 }
			 else{ Cloading();}
   		}, 
		error: function (xhr, status) 
		{  
			Cloading();	
			//DSAlert("Unable to proceed, please try again.","margin-top0px");
			$("#Tupdate").fadeIn("slow");
		$("#Tupdate").html("<i class='fa fa-exclamation-circle font-size18px pull-left'></i>Please fill-in all the mandatory fields.");
		setTimeout(function(){ $("#Tupdate").fadeOut("slow"); }, 4000);
   		}
 	});
	}
	else{
		// DSAlert("Please fill-in all the mandatory fields.","margin-top0px");
		$("#Tupdate").fadeIn("slow");
		$("#Tupdate").html("<i class='fa fa-exclamation-circle font-size18px pull-left'></i>Please fill-in all the mandatory fields.");
		setTimeout(function(){ $("#Tupdate").fadeOut("slow"); }, 4000);
		 Cloading();
		 }
	
}



function AddRestaurant()
{
	restaurant = $("#restaurant_name").val();
 	if(restaurant!="") {
		loading(); 	
		$.ajax({
			type: "POST", 
			data:{hotelname:restaurant,admin:ADMIN},
			url: "api.php?action=AddRestaurant", 
			success:function(response)
			{
				
				//response = JSON.parse(response);
				//console.log(response);
    			if(response=='"success"')
				{
						GetRestaurant();
						SAlert("Record has been added.");
						$("#restaurant_name").val("");  
						Cloading();
						openPupopRestclose();
				 } 
				if(response=='"Noaddlocation"')
				{
					Cloading();
				    DSAlert("You can add one location.","margin-top0px");
					openPupopRestclose();
				}
			}, 
			error: function (xhr, status) 
			{  Cloading();
				DSAlert("Unable to proceed, please try again.","margin-top0px");
			}
		});
	}
	else{
		  $("#restMsg").fadeIn("slow");
		  $("#restMsg").addClass("alert-danger").removeClass("alert-successP");;
		  $("#restMsg").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Enter a valid location.");
		  Cloading();
		}
}

function GetRestaurant()
{  
loading();
 	$.ajax({
		type: "POST", 
		data:{admin:ADMIN},
 		url: "api.php?action=GetRestaurant", 
		success:function(response)
		{
			//console.log(response);
			
			Cloading();

  			 if(response!='"No Records"')
			 {
				
				 document.getElementById("AllRestaurant").innerHTML ="";
				 response=JSON.parse(response);
				 
				 for(var i=0;i<response.length;i++)
				 { 
				 	document.getElementById("AllRestaurant").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
 					<td class="center">'+response[i]['restaurant_name']+'</td>\
					<td align="center" class="p-0 colorGreen">'+'<span class="cursor" onClick="getrestaurent('+response[i]['id']+')">Edit</span></td></tr>';
					
					//<td align="center" class="p-0 colorGreen">'+'<span class="cursor" onClick="getrestaurent('+response[i]['id']+')">Edit</span> |<span class="cursor" onClick="getAlldocsdata(\'' +response[i]['restaurant_name']+'\')"> Export Data</span></td>';
					//document.getElementById("Restaurantdropdwon2").innerHTML+='<option>'+response[i]['restaurant_name']+'</option>';
				 } 	
				
				}
  			 else{ 
			 
			 InfoAlert("No records found.");
			 document.getElementById("AllRestaurant").innerHTML="";
			}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			 
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}
var restID;
function getrestaurent(id)
{
   restID=id;
   loading();
	$("#restaurant_namePOP").val("");
	
   	$.ajax({
		type: "POST",  
 		url: "api.php?action=GetRestaurant2", 
		data:{restID:restID},
		success:function(response)
		{
			//console.log(response)
			Cloading();
  			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
				 // console.log(response);
				 for(var i=0;i<response.length;i++)
				 {
					 $("#restaurant_namePOP").val(response[i]['restaurant_name']);
					 $("#myModal").modal();
					// opnerestpopup();
 				 }
  			 }else{  InfoAlert("No records found."); }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");

   		}
 	});
	
}

function updateRest(){
	
	updaterest = $("#restaurant_namePOP").val();
	if(updaterest!="")
	{
		loading(); 	
		$.ajax({
			type: "POST",  
			data:{updaterest1:updaterest,restID:restID},
			url: "api.php?action=UpdateRest", 
			success:function(response)
			{
				// console.log(response);
				Cloading();
				 if(response=='"success"')
				 {
					SAlert("Location has been updated.");
					//$("#restMsg1").fadeIn("slow");
					//$("#restMsg1").addClass("alert-successP").removeClass("alert-dangerP");
					//$("#restMsg1").html("Restaurant Added Successfully");
					// setTimeout(function(){ $("#restMsg1").fadeOut("slow"); }, 2000);
					//opnerestpopupclose();
					$('#myModal').modal('hide');

				 }
				 else
				 {
					// DSAlert("Restaurant has not been updated.","margin-top0px");
					$("#restMsg1").fadeIn("slow");
					$("#restMsg1").addClass("alert-danger").removeClass("alert-successP");;
					$("#restMsg1").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Enter a valid location.");
				 }
				 GetRestaurant();
			}, 
			error: function (xhr, status) 
			{  Cloading();
				//DSAlert("Unable to proceed, please try again.","margin-top0px");
				$("#restMsg1").fadeIn("slow");
				$("#restMsg1").addClass("alert-danger").removeClass("alert-successP");;
				$("#restMsg1").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Enter a valid location.");
			}
		});
	}
	else{
		//DSAlert("Please fill-in all the mandatory fields.","margin-top0px");
		  $("#restMsg1").fadeIn("slow");
		  $("#restMsg1").addClass("alert-danger").removeClass("alert-successP");;
		  $("#restMsg1").html("<i class='fa fa-exclamation-circle font-size22px  pull-left'></i>Enter a valid location.");
		Cloading();}
}

var resturantdrop="";

function getAllrestaurant(){
	
	 	$.ajax({
		type: "POST",  
 		url: "api.php?action=GetRestaurantAll", 
		data:{admin:ADMIN},
		success:function(response)
		{
			// console.log(response);
			resturantdrop="";
			Cloading();
  			 if(response!='"No Records"')
			 {
 				 response=JSON.parse(response);
				// console.log(response);
				  if(response.length ==0){ }
				  
				resturantdrop+= "<option value='0'>-Select-</option>"; 
 				 for(var i=0;i<response.length;i++)
				 {
 					resturantdrop+= "<option value='" + response[i].id + "'>"+response[i].restaurant_name+"</option>"; 
					$("#hiddenres").val(response[i].restaurant_name);
					$("#hiddenres_dash").val(response[i].restaurant_name);
					$("#hiddenreports").val(response[i].restaurant_name);
					$("#resturant_Name").html(response[i].restaurant_name);
					
					
					
  				 }
				 
				 $("#Restaurantdropdwon").html(resturantdrop);
				 $("#Restaurantdropdwonpopup").html(resturantdrop);
				 $("#Restaurantdropdwon2").html(resturantdrop);
				 
				 
				 
				 
  			 }
			 else
			 { 
			      InfoAlert("No records found.");
			      $('.settinglink').css("visibility","visible"); 
			}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");

   		}
 	});
	
	
	
	
}

var RestaurantByEmployee = [];
function GetRestaurantByEmployee(){ 	
	 	$.ajax({
		type: "POST",  
 		url: "api.php?action=GetRestaurantByEmployee", 
		data:{admin:ADMIN},
		success:function(response)
		{
			resturantdrop="";
			Cloading();
  			 if(response!='"No Records"')
			 {
 				 RestaurantByEmployee=JSON.parse(response);
   			 }else{  InfoAlert("No records found."); }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");

   		}
});}


function EmployeeRestaurant()
{
	var emp = $("#EmployeeSelect").val(); 
	if(emp=="-Select-"){$("#Restaurantdropdwon").val(0);}
	if(RestaurantByEmployee!="")
	{
		for(r in RestaurantByEmployee){
			if(RestaurantByEmployee[r].id==emp)
			{
				$("#Restaurantdropdwon").val(RestaurantByEmployee[r].resturant_id);				 
			}
		}
	}
}


function SuppEmail()
{
	 first=$.cookie("fname");
     last=$.cookie("lname");
     email=$.cookie("email");
	 
	
	 userDetails = first+" "+last+"<"+email+">";
	 Subject = $("#Subject").val();
	 Message = $("#Message").val();
	 
  if(Subject!="" && Message!="")
	{
		loading();
	  	$.ajax({
			type: "POST",
	 		data:{Subject:Subject,Message:Message,admin:userDetails},
			dataType: 'json',
	 		url: "passwordreset.php?act=suppurt", 
			success:function(response)
			{
				Cloading();
				// console.log(response);
				
	  			 if(response=='success')
				 { 	
					
					//SAlert("Email sent successfully!");
				$("#emailsupport").fadeIn("slow");
				$("#emailsupport").removeClass("alert-danger").addClass("alert-success");
		        $("#emailsupport").html("<i class='fa fa-exclamation-circle font-size18px  pull-left'></i>Your request has been sent successfully.");
		        setTimeout(function(){ $("#emailsupport").fadeOut("slow"); }, 4000);
				
				$("#Subject").val("");
	            $("#Message").val("");
					//suppClose();
					
 	 			 }
	  			 if(response=='Error')
	  			 {
	  			 	//DSAlert2('Unable to proceed, please try again.","margin-top0px');
				Cloading();
				$("#emailsupport").fadeIn("slow");
				$("#emailsupport").removeClass("alert-success").addClass("alert-danger");
		        $("#emailsupport").html("<i class='fa fa-exclamation-circle font-size18px  pull-left'></i>Unable to proceed, please try again.");
		        setTimeout(function(){ $("#emailsupport").fadeOut("slow"); }, 4000);
					
	  			 }
 	   		}, 
			error: function (xhr, status) 
			{  
			//  DSAlert("Unable to proceed, please try again.","margin-top0px");
			$("#emailsupport").fadeIn("slow");
			$("#emailsupport").removeClass("alert-success").addClass("alert-danger");
		    $("#emailsupport").html("<i class='fa fa-exclamation-circle font-size18px  pull-left'></i>Unable to proceed, please try again.");
		    setTimeout(function(){ $("#emailsupport").fadeOut("slow"); }, 4000);
		    Cloading();	
			//loginPage();
	   		}
	 	});
 	}
	else
	{
		 //DSAlert2("Please enter a valid email address.","margin-top3px");
		 $("#emailsupport").fadeIn("slow");
		 $("#emailsupport").removeClass("alert-success").addClass("alert-danger");
		 $("#emailsupport").html("<i class='fa fa-exclamation-circle font-size18px pull-left'></i>Please fill-in all the mandatory fields.");
		 setTimeout(function(){ $("#emailsupport").fadeOut("slow"); }, 4000);
		// emailsupport
		 Cloading();
	 } 
}

function getAlldocsdata(name){
	
	restName=name;
	userID=ADMIN;
	
	
    loading();
 	$.ajax({
			type: "POST",
	 		data:{restName:restName,userID:userID},
			url: "api.php?action=getAlldocsdata", 
			success:function(response)
			{
								
				response=JSON.parse(response);
				rows = response;
				csvContent = "data:text/csv;charset=utf-8,";
				rows.forEach(function(rowArray){
				row = rowArray.join(",");
				csvContent += row + "\r\n";
				}); 
				var encodedUri = encodeURI(csvContent);
				var link = document.createElement("a");
				link.setAttribute("href", encodedUri);
				link.setAttribute("download", "Complyance.csv");
				document.body.appendChild(link); 

                link.click();
				Cloading();
				
 	   		}, 
			error: function (xhr, status) 
			{
				Cloading();
				DSAlert("Unable to proceed, please try again.","margin-top0px");  
			  
			
			}
	 	});
}


/*var rows=[];
function getAlldocsdata(){
	
	restName=$("#Restaurantdropdwon2 option:selected").text()
	userID=ADMIN;
	$("#btndown").attr('disabled',true);
	$("#btndown").css("opacity",0.5);

 	$.ajax({
			type: "POST",
	 		data:{restName:restName,userID:userID},
			url: "http://localhost/RCM-Toolv2/api.php?action=getAlldocsdata", 
			success:function(response)
			{
				
				
				response=JSON.parse(response);
				rows = response;
				console.log(response);
				$("#btndown").attr('disabled',false);
				$("#btndown").css("opacity",1);
				if(rows.length <1){
				$("#btndown").attr('disabled',true);
				$("#btndown").css("opacity",0.5);
				}
				
 	   		}, 
			error: function (xhr, status) 
			{  
			  
			
			}
	 	});
}


function downloadexcel(){
	           
				csvContent = "data:text/csv;charset=utf-8,";
				rows.forEach(function(rowArray){
				row = rowArray.join(",");
				csvContent += row + "\r\n";
				}); 
				var encodedUri = encodeURI(csvContent);
				var link = document.createElement("a");
				link.setAttribute("href", encodedUri);
				link.setAttribute("download", "RCM Tool.csv");
				document.body.appendChild(link); 

                link.click();
	
	
}


*/
$('#remember').click(function() {
    if($(this).is(':checked'))
    	remember = true;
    else
        remember = false;
});


function unactiveSmsNot(){
    var user_id = $.cookie("ID");
    $.ajax({
         type: "POST",
         data: {user_id:user_id},
         url: "api.php?action=unactiveSmsNot",
         success: function(response) {
            if(response == '"success"'){
                DSAlert("SMS Notification Unactive", "margin-top0px");
            }
          //  console.log(response);
         },
         error: function(xhr, status, errorThrown) {
             DSAlert("Unable to proceed, please try again.", "margin-top0px");
         }
     });
}

function activeSmsNot(){
     var user_id = $.cookie("ID");
    $.ajax({
         type: "POST",
         data: {user_id:user_id},
         url: "api.php?action=activeSmsNot",
         success: function(response) {
            if(response == '"success"'){
                SAlert("SMS Notification Active");
            }
           // console.log(response);
         },
         error: function(xhr, status, errorThrown) {
             DSAlert("Unable to proceed, please try again.", "margin-top0px");
         }
     });
}




/*************************************************************************************************************************/

function Customers(){
	loading(); 	
	if(document.getElementById("excel_file").value != "") {AddCustomers1();}
	else{AddCustomers();}
}




function AddCustomers()
{   
 
	Customer_Name	=	$("#CustomerName").val();  
 	Customer_EmailAddress =	$("#CustomerEmailAddress").val();  
 	Customer_PhoneNumber	=	$("#CustomerPhoneNumber").val();  
 	Customer_CompanyName	=	$("#CustomerCompanyName").val();  
 	
 	if(Customer_Name!=""  && Customer_EmailAddress!="" && Customer_PhoneNumber!="" &&  Customer_CompanyName!="" && validateEmail(Customer_EmailAddress)!=false)
	{
		loading(); 	
		$.ajax({
			type: "POST", 
			data:{admin:ADMIN,Customer_Name:Customer_Name,Customer_EmailAddress:Customer_EmailAddress,Customer_PhoneNumber:Customer_PhoneNumber,Customer_CompanyName:Customer_CompanyName},
			url: "api.php?action=AddCustomer", 
			success:function(response)
			{
				
    			if(response=='"success"')
				{
 				 	GetAllCustomers();
					SAlert("Record has been added.");
					$("#CustomerName").val("");  
					$("#CustomerEmailAddress").val("");  
					$("#CustomerPhoneNumber").val("");  
					$("#CustomerCompanyName").val("");
					Cloading();  

				 } 
				 else
				 {
					 Cloading();
				 }
			}, 
			error: function (xhr, status) 
			{   Cloading();
				DSAlert("Unable to proceed, please try again.","margin-top0px");
			}
		});
	}
	else{ 
	   
	   if(!validateEmail(Customer_EmailAddress) && Customer_EmailAddress!="" ){DSAlert("Please enter a valid email address.","margin-top0px");Cloading();}
	   else{DSAlert("Please fill-in all the mandatory fields.","margin-top0px");Cloading();}
	
	}
}	





function GetAllCustomers()
{  
   loading();
 	$.ajax({
		type: "POST", 
		data:{admin:ADMIN},
 		url: "api.php?action=GetAllCustomers", 
		success:function(response)
		{
			//console.log(response);
			Cloading();

  			 if(response!='"No Records"')
			 {
			 	$("#vendorTable").dataTable().fnDestroy();
				 document.getElementById("AllVendorsData").innerHTML ="";
				 response=JSON.parse(response);
				 for(var i=0;i<response.length;i++)
				 { 
  					document.getElementById("AllVendorsData").innerHTML +='<tr>\
					<td align="center">'+(i+1)+'</td>\
					<td align="center">'+response[i]['Customer_name']+'</td>\
 					<td align="center">'+response[i]['Customer_email']+'</td>\
					<td align="center">'+response[i]['Customer_phone']+'</td>\
					<td align="center">'+response[i]['Customer_business']+'</td>\
					<td align="center" class="p-0 colorGreen"><span class="cursor" onClick="deleteBox('+response[i]['id']+')">Delete</span></td>\</tr>';
				 }
				  DatatableInit4();			 
  			 }
  			 else{ 
  			 	// $("#vendorTable").dataTable().fnDestroy();
  			 	InfoAlert("No records found.");
			 document.getElementById("AllVendorsData").innerHTML="";
			}
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			 
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
}




function DeleteCustomer()
{  
    
    loading();
	$.ajax({
		type: "POST",
 		data:{DeleteID:DeleteID},
		dataType: 'json',
 		url: "api.php?action=DeleteCustomer", 
		success:function(response)
		{
  			 if(response=='success')
			 {
 				 GetAllCustomers();
 				 SAlert("Record has been deleted!");
				 closeDelPupop();
 			 }
			 else{ DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	}
   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");closeDelPupop();Cloading();	
   		}
 	});
}


function sortingexp(){
	sortingdropdpwn=$("#sortingdropdpwn option:selected").text();
	if(sortingdropdpwn=="Permit"){ R_Permit();}
	if(sortingdropdpwn=="License"){ R_License();}
	if(sortingdropdpwn=="Certificate"){ R_certification();}
	if(sortingdropdpwn=="Employee"){ empbusiness();}
	if(sortingdropdpwn=="Business"){ empbusiness1();}
	
}
function Sorting(){
	$("#loader").show();
	$("#reports").html("");
	$("#reports").show();
	tbl2="";
	LPPastArr1=[];
	CPastArr1=[];
	 $("#reportstable").dataTable().fnDestroy();
	$('#expirationbyemp').addClass("activesorting");
	$('#itemsbycatagoery').removeClass("activesorting");
	$('#sortingdropdpwn').html(
	  '<option>Sort by:</option> \
	  <option>License</option>\
	  <option>Permit</option>\
	  <option>Certificate</option>'
	  );
	 
	  todayDate();
}
function resetreports(){
	$('#itemsbycatagoery').removeClass("activesorting");
	$('#expirationbyemp').removeClass("activesorting");
	$('#sortingdropdpwn').html('<option>Sort by:</option>');
	$("#reports").hide();
	/*$("#reportstable").html("");*/
	tbl2="";
	cer="";
	LPPastArr1=[];
	CPastArr1=[];
	$("#reportstable").dataTable().fnDestroy();
	
}	


var LPPastArr1=[],CPastArr1=[];
var tbl2="";

var LArray=[];


function Reports()
{
	tbl2="";
	var no1=1;
	$("#reports").html("");
	$("#reportstable").dataTable().fnDestroy();
	
	 for(var i=0;i<LPPastArr1.length;i++)
	 {
	  
		  dateFormate(LPPastArr1[i]['ExpirationDate']);
		 
		 var loc = (LPPastArr1[i]['LocationName']==null || LPPastArr1[i]['LocationName']== "undefined" ? LPPastArr1[i]['LocationName']=$("#hiddenreports").val() : LPPastArr1[i]['LocationName']);
         var empname = (LPPastArr1[i]['empname']==null ? "-" : LPPastArr1[i]['empname']);
		 tbl2+='<tr><td align="center">'+LPPastArr1[i]['doctype']+'</td><td align="center">'+empname+'</td><td align="center">'+loc+'</td><td align="center">'+LPPastArr1[i]['license']+'</td><td align="center">'+finalJoiningDate+'</td></tr>';
		 no1++;
	 } 
	 	 
	 for(var i=0;i<CPastArr1.length;i++)
	 {
 		
 		 dateFormate(CPastArr1[i]['exp_date']);
		 
		 var loc = (CPastArr1[i]['LocationName']==null || CPastArr1[i]['LocationName']=="undefined" ? LPPastArr1[i]['LocationName']=$("#hiddenreports").val() : CPastArr1[i]['LocationName']);
		 
		 var empname = (CPastArr1[i]['empname']==null ? "-" : CPastArr1[i]['empname']);
		 
		 tbl2+='<tr><td align="center">'+"Certificate"+'</td><td align="center">'+empname+'</td><td align="center">'+loc+'</td><td align="center">'+CPastArr1[i]['certification']+'</td><td align="center">'+finalJoiningDate+'</td></tr>';
		  no1++;
	 }	
    $("#reports").html(tbl2);
	DatatableInitReports();
} 
 
var cer;

function R_certification()
{ 
   $("#reports").html("");
   $("#reportstable").dataTable().fnDestroy();
    cer=""; 
	
 for(var i=0;i<CPastArr1.length;i++)
	 {
 		
 		 dateFormate(CPastArr1[i]['exp_date']);
		 var empname = (CPastArr1[i]['empname']==null ? "-" : CPastArr1[i]['empname']);
		 var loc = (CPastArr1[i]['LocationName']==null || CPastArr1[i]['LocationName']== "undefined" ? LPPastArr1[i]['LocationName']=$("#hiddenreports").val() : CPastArr1[i]['LocationName']);
		   
		 cer+='<tr><td align="center">'+"Certificate"+'</td><td align="center">'+empname+'</td><td align="center">'+loc+'</td><td align="center">'+CPastArr1[i]['certification']+'</td><td align="center">'+finalJoiningDate+'</td></tr>';
		
	 }	
    $("#reports").html(cer);
	DatatableInitReports();

}	

var lic;
function R_License()
{ 
   $("#reports").html("");
   $("#reportstable").dataTable().fnDestroy();
   lic=""; 
   var counting=1;
  for(var i=0;i<LPPastArr1.length;i++)
	 {
	
	    if(LPPastArr1[i]['doctype'] == "License"){
			
	     var empname = (LPPastArr1[i]['empname']==null ? "-" : LPPastArr1[i]['empname']);
		 var loc = (LPPastArr1[i]['LocationName']==null || LPPastArr1[i]['LocationName']== "undefined" ? LPPastArr1[i]['LocationName']=$("#hiddenreports").val() : LPPastArr1[i]['LocationName']);
		 
		 dateFormate(LPPastArr1[i]['ExpirationDate']);
		 lic+='<tr><td align="center">'+LPPastArr1[i]['doctype']+'</td><td align="center">'+empname+'</td><td align="center">'+loc+'</td><td align="center">'+LPPastArr1[i]['license']+'</td><td align="center">'+finalJoiningDate+'</td></tr>';
		 counting++;
		}
 } 
	  $("#reports").html(lic);
	  DatatableInitReports();

}

var Per;
function R_Permit()
{ 
var number=1;
$("#reports").html("");
$("#reportstable").dataTable().fnDestroy();
Per=""; 

  for(var i=0;i<LPPastArr1.length;i++)
	 {
	
	    if(LPPastArr1[i]['doctype'] == "Permit"){
		 dateFormate(LPPastArr1[i]['ExpirationDate']);
		   var empname = (LPPastArr1[i]['empname']==null ? "-" : LPPastArr1[i]['empname']);
		   var loc = (LPPastArr1[i]['LocationName']==null || LPPastArr1[i]['LocationName']== "undefined" ? LPPastArr1[i]['LocationName']=$("#hiddenreports").val() : LPPastArr1[i]['LocationName']);
			
		 Per+='<tr><td align="center">'+LPPastArr1[i]['doctype']+'</td><td align="center">'+empname+'</td><td align="center">'+loc+'</td><td align="center">'+LPPastArr1[i]['license']+'</td><td align="center">'+finalJoiningDate+'</td></tr>';
		 number++;
		}
		
 } 
	  $("#reports").html(Per);
DatatableInitReports();
}

function Sorting1(){
	$("#loader").show();
	$("#reports").html("");
	$("#reports").show();
	tablesorting="";
	tablesorting2="";
	mainArry=[];
	mainArry2=[];
	
	$("#reportstable").dataTable().fnDestroy();
	
	$('#itemsbycatagoery').addClass("activesorting");
	$('#expirationbyemp').removeClass("activesorting");
	$('#sortingdropdpwn').html(
	  '<option>Sort by:</option> \
	  <option>Employee</option>\
	  <option>Business</option>\
	');
	 // todayDate();
	 itemsbycatagory();
	
}






var tablesorting="";
var counted=1;
var mainArry=[];
var mainArry2=[];


function itemsbycatagory(){
	
	counted=1;
    document.getElementById("reports").innerHTML ="";
	$("#reportstable").dataTable().fnDestroy();
    tablesorting="";

	loading();
   	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
 		url: "api.php?action=getAllCertification", 
		success:function(response)
		{
		    	Cloading();
  			 if(response!='"No Records"')
			 {
 				 response1=JSON.parse(response);
				 mainArry=response1;
				 
				 console.log(mainArry);
				 
				
			 for(var i=0;i<mainArry.length;i++)
				 {	
				  var empname = (mainArry[i]['empname']==null ? "-" : mainArry[i]['empname']);
		          var loc = (mainArry[i]['restaurant_name']==null || mainArry[i]['restaurant_name']== "undefined" ? mainArry[i]['restaurant_name']=$("#hiddenreports").val() : mainArry[i]['restaurant_name']);
				  mainArry[i]['exp_date'] = getFormattedDate(mainArry[i]['exp_date']);
				 
				 	 tablesorting +='<tr>\
					<td align="center">Certificate</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+loc+'</td>\
					<td align="center">'+mainArry[i]['certification']+'</td>\
					<td align="center">'+mainArry[i]['exp_date']+'</td>\
					</tr>';
					counted++;
				 }
				 
				 document.getElementById("reports").innerHTML=tablesorting;
   			 } 
			 else{ 
			  InfoAlert("No records found.");
			 // document.getElementById("reports").innerHTML="";
			 }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});

	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
  		url: "api.php?action=getlicensepermitsforreports", 
		success:function(response)
		{
			Cloading();
  			 if(response!='"No Records"')
			 {
 				 response_1=JSON.parse(response);
				 mainArry2=response_1;
				 console.log(mainArry2);
				 
				 
			 for(var j=0;j<mainArry2.length;j++)
				 {	
				   var empname = (mainArry2[j]['empname']==null ? "-" : mainArry2[j]['empname']);
		           var loc = (mainArry2[j]['LocationName']==null || mainArry2[j]['LocationName']== "undefined" ? mainArry2[j]['LocationName']=$("#hiddenreports").val() : mainArry2[j]['LocationName']);
				   mainArry2[j]['ExpirationDate'] = getFormattedDate(mainArry2[j]['ExpirationDate']);
				
				
				   
					 tablesorting +='<tr>\
					<td align="center">'+response_1[j]['doctype']+'</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+loc+'</td>\
					<td align="center">'+response_1[j]['license']+'</td>\
					<td align="center">'+mainArry2[j]['ExpirationDate']+'</td>\
					</tr>';
					
				 }
				
				document.getElementById("reports").innerHTML=tablesorting;
				DatatableInitReports();	
				
				 }
			 else{ 
			     InfoAlert("No records found.");
			     //document.getElementById("reports").innerHTML="";
			  }
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});
	

	
   			

}

var tablesorting2="";
function empbusiness(){
	
	tablesorting2="";
	document.getElementById("reports").innerHTML="";
    $("#reportstable").dataTable().fnDestroy();
	
	
	
	  for(var k=0;k<mainArry2.length;k++)
				 {
				  var empname = (mainArry2[k]['empname']==null ? "-" : mainArry2[k]['empname']);
		          var loc = (mainArry2[k]['LocationName']==null || mainArry2[k]['LocationName']== "undefined" ? mainArry2[k]['LocationName']=$("#hiddenreports").val() : mainArry2[k]['LocationName']);
				  mainArry2[k]['ExpirationDate'] = getFormattedDate(mainArry2[k]['ExpirationDate']);
			
			if(mainArry2[k]['LPtype']=="Employee"){	
			
			    
					 tablesorting2 +='<tr>\
					<td align="center">'+mainArry2[k]['doctype']+'</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+loc+'</td>\
					<td align="center">'+mainArry2[k]['license']+'</td>\
					<td align="center">'+mainArry2[k]['ExpirationDate']+'</td>\
					</tr>';
					
					document.getElementById("reports").innerHTML=tablesorting2;
			}
	 }
	

	  for(var l=0;l<mainArry.length;l++)
				 {	
				   var empname = (mainArry[l]['empname']==null ? "-" :mainArry[l]['empname']);
		          var loc = (mainArry[l]['restaurant_name']==null || mainArry[l]['restaurant_name']== "undefined" ? mainArry[l]['restaurant_name']=$("#hiddenreports").val() : mainArry[l]['restaurant_name']);
				  mainArry[l]['exp_date'] = getFormattedDate(mainArry[l]['exp_date']);
			
			if(mainArry[l]['certificatetype']=="Employee"){	 
					 tablesorting2 +='<tr>\
					<td align="center">certificate</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+loc+'</td>\
					<td align="center">'+mainArry[l]['certification']+'</td>\
					<td align="center">'+mainArry[l]['exp_date']+'</td>\
					</tr>';
					
					document.getElementById("reports").innerHTML=tablesorting2;
			}
	 }
	 
	 DatatableInitReports();
	

	}
	
	
function empbusiness1(){
	
tablesorting2="";	
$("#reports").html("");
document.getElementById("reports").innerHTML="";
$("#reportstable").dataTable().fnDestroy();

	
	
	  for(var k=0;k<mainArry2.length;k++)
				 {	
				 
		var empname = (mainArry2[k]['empname']==null ? "-" : mainArry2[k]['empname']);
		var loc = (mainArry2[k]['LocationName']==null || mainArry2[k]['LocationName']== "undefined" ? mainArry2[k]['LocationName']=$("#hiddenreports").val() : mainArry2[k]['LocationName']);
		mainArry2[k]['ExpirationDate'] = getFormattedDate(mainArry2[k]['ExpirationDate']);
		
		
			
			if(mainArry2[k]['LPtype']=="Location"){	 
					tablesorting2 +='<tr>\
					<td align="center">'+mainArry2[k]['doctype']+'</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+loc+'</td>\
					<td align="center">'+mainArry2[k]['license']+'</td>\
					<td align="center">'+mainArry2[k]['ExpirationDate']+'</td>\
					</tr>';
					
					document.getElementById("reports").innerHTML=tablesorting2;
			}
	 }
	
	
	  for(var l=0;l<mainArry.length;l++)
				 {	
				 
			var empname = (mainArry[l]['empname']==null ? "-" :mainArry[l]['empname']);
			var loc = (mainArry[l]['restaurant_name']==null || mainArry[l]['restaurant_name']== "undefined" ? mainArry[l]['restaurant_name']=$("#hiddenreports").val() : mainArry[l]['restaurant_name']);
			mainArry[l]['exp_date'] = getFormattedDate(mainArry[l]['exp_date']);
			
			
			if(mainArry[l]['certificatetype']=="Location"){	 
					 tablesorting2 +='<tr>\
					<td align="center">certificate</td>\
					<td align="center">'+empname+'</td>\
					<td align="center">'+loc+'</td>\
					<td align="center">'+mainArry[l]['certification']+'</td>\
					<td align="center">'+mainArry[l]['exp_date']+'</td>\
					</tr>';
					
					document.getElementById("reports").innerHTML=tablesorting2;
			}
	 }
	
 DatatableInitReports();
	}	


function paymentdetails()
{
	$("#payment_details").html("");
	//$("#userdetails").html("");
	$.ajax({
		type: "POST",  
		data:{admin:ADMIN},
  		url: "api.php?action=paymentdetailsofuser", 
		success:function(response)
		{
			response=JSON.parse(response);
			//console.log(response);
			
			Cloading();
			if(response!="No Records")
			{
				for(i=0;i<response.length;i++)
			{
				$("#methode").html(response[i]["payment_methode"]);
				$("#buyer_name").html(response[i]["buyer_name"]);
				$("#buyer_name1").html(response[i]["buyer_name"]);
				$("#buyer_email").html(response[i]["buyer_email"]);
				$("#addressofbuyer").html(response[i]["shipping_add"]);
			}
				
			}
			else
			{
			    $("#userdetails").html("You didn't buy any plan");
			}
  			
   		}, 
		error: function (xhr, status) 
		{  Cloading();
			DSAlert("Unable to proceed, please try again.","margin-top0px");
   		}
 	});

}

