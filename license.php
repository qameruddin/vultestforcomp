<?php
$check=$_COOKIE['login'];
if($check!="")
{
	
}else{header("location:index.php");}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>COMPLYANCE - LICENSE/PERMIT</title>
<link rel="icon" href="images/logo/favicon.png" type="images/logo/favicon.png" />
<link href="fonts/fontStyle.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/datepicker.css" rel="stylesheet" type="text/css">
  
<!--<script src="js/jquery.js"></script>-->
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/webjs1.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/new1.js"></script>
<script src="js/toggal.js"></script>

 </head>

<body onLoad="username(),getEmpNames(),getAllrestaurant(),getAllLicense(),GetRestaurantByEmployee()">
<?php include('aside.php'); ?>

<section id="secFixHeight" class="overflow-hidden col-lg-10 col-md-9 col-sm-12 col-12 pull-left p-0">
<header class="pt-4 pb-4 border-bottom1px bg-colorWhite">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<div class="pull-left" style="display:block;">
<!--<input type="button" class="fa fa-bars btn btn-danger pull-left " onClick="openMenu();">-->
<button class="btn btn-darkMenu paddingMenu cursor menuIconDis" onclick="openMenu();"><i class="fa fa-bars font-size22px"></i></button>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-7 pull-left p-0">
<h3 class="colorGreen m-0 textCenterScr768">LICENSE/PERMIT/CERTIFICATE</h3>
</div>
<div class="col-lg-4 col-md-4 col-sm-3 col-6 pull-right">
<p class="pull-right m-0 mr-2"><span id="adminName"></span><i class="fa fa-power-off ml-3 colorGreen font-size22px pull-right padding-top2px cursor" onclick="LogoutModal()"></i> </p>
</div>

</div>
</div>
</div>
</header>

<article class="overflow-yscroll mb-5">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto border-bottom1px pb-4">
<div class="col-12 overflow-hidden p-0">
<h4 class="font-weight-bold mt-4 colorDarkGray pull-left">Add New License/Permit/Certificate</h4>
<div class="alert alert-success fadeIn pull-right mb-0 margin-top10px" id="AlertMsg" role="alert" style="display:none">
  <span id="AlertTxt">You successfully Add Data.</span>
</div>
</div>

<div class="boxes col-12 p-4 overflow-hidden mt-3">
<div class="col-12 overflow-hidden border-bottom1px">

<ul class="tab">
<li id="tab1" class="activeTab text-uppercase cursor" onClick="licPerTab(1)">License</li>
<li id="tab2" class="text-uppercase cursor" onClick="licPerTab(2)">Permit</li>
<li id="tab3" class="text-uppercase cursor" onClick="licPerTab(3)">certificate</li>
</ul>

<ul class="tab float-right" id="emplocationtabs">
<li id="emp1" class="activeTab text-uppercase cursor" onclick="emplocation(1)">Employee</li>
<li id="emp2" class="text-uppercase cursor"  onclick="emplocation(2)">Business</li>
</ul>



</div>
<div class="col-12 overflow-hidden mt-2">
  <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="empName"><span id="capName">License</span><span style="color:#149265;">*</span></label>
     <select class="form-control icon-arrow pr-5 borderInputgreen" id="License">
      <option>-Select-</option>
      <option>ABC Manger License (ABRA)</option>
      <option>Drivers License</option>
	  <option>State Issued Identification Card</option>
    </select>
  </div>
 
  <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1"  id="certificateEmployee"> 
    <label for="titleDeseg">Employee<span style="color:#149265;">*</span><span class="employeelink">(Please add <a href="employee.php" style="color:#149265;">Employee </a>)</span></label>
     <select class="form-control icon-arrow borderInputgreen" id="EmployeeSelect" onchange="EmployeeRestaurant()">
     <option value="-Select-">-Select-</option>
     </select>
  </div>

   <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1" id="Location" style="display:none;">
    <label for="comOn">Location<span style="color:#149265;">*</span><span class="settinglink">(Please add location in <a href="settings.php" style="color:#149265;">Settings</a>)</span></label>
    <select class="form-control icon-arrow borderInputgreen" id="Restaurantdropdwon">
       <option value="0">-Select-</option>
     </select>
  </div> 
  

   <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="JoiningDate">Issuance Date<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control borderInputgreen datepicker" id="IssuanceDate"  onchange="issueExpDateMatch()" onkeydown="return false"  placeholder='mm/dd/yy'>
  </div>
  
    <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="JoiningDate">Expiration Date<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control borderInputgreen datepicker" id="ExpirationDate"  disabled title="Expiration Date Can Not Be Before Issuance Date." onkeydown="return false"  placeholder='mm/dd/yy'> 
  </div>

  <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1 mb-0">
    <label for="comOn">Comments</label>
    <textarea id="Comments" class="form-control icon-edit icon-position  icon-editComint borderInputgreen" placeholder="Type Here..." id="exampleTextarea" rows="3" cols="10"></textarea>
  </div>

  <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="dateBirth">Upload</label>
    <button type="button" class="atchbtn form-control-file btn btn-white font-familyUniversLT icon-attachment col-lg-12 col-md-12 col-sm-12 col-12 text-left borderInputgreen" aria-describedby="fileHelp">Upload Scanned Copy <br>of the License/Permit</button>
    <input type="file" class="form-control-file inputfileBtn col-lg-12 col-md-12 col-sm-12 col-12 opacity0" name="file" id="file" aria-describedby="fileHelp" onChange="imgUpload()">
    <span class="colorGreen d-block m-2 pull-left" id="attached"></span>
    <span id="cross" class="bg-green crossImgAttch font-familyUniversLT font-size14px cursor fontweightBold" onClick="imgUploadRemove()"></span>
  </div>
  
   <div class="form-group pull-right col-lg-3 col-md-5 col-sm-12 col-12 p-1 mt-33" id="LPDOC">
  <form mehtod="post" id="export_excel">  
    <button type="button" class="form-control-file bg-green col-lg-12 col-md-12 col-sm-12 col-12 atchbtn impbtn borderInputgreen font-size16px" aria-describedby="fileHelp">IMPORT FROM EXCEL</button>
   <input type="file" class="form-control-file inputfileBtn col-lg-12 col-md-12 col-sm-12 col-12 opacity0 borderInputgreen"  type="file" name="excel_file" id="excel_file">
    </form>
 </div>
 
 
  <div class="form-group pull-right col-lg-3 col-md-5 col-sm-12 col-12 p-1 mt-33" id="CER"  style="display:none;">
  <form mehtod="post" id="export_excel2">  
    <button type="button" class="form-control-file bg-green col-lg-12 col-md-12 col-sm-12 col-12 atchbtn impbtn borderInputgreen font-size16px" aria-describedby="fileHelp">IMPORT FROM EXCEL</button>
   <input type="file" class="form-control-file inputfileBtn col-lg-12 col-md-12 col-sm-12 col-12 opacity0 borderInputgreen"  type="file" name="excel_file" id="excel_file2">
    </form>
 </div>
 
 
   <input type="hidden" id="hiddenres">
 

  <div class="form-group pull-right col-lg-2 col-md-3 col-sm-12 col-12 p-1 mb-0 margin-top28px">
    <input type="button" class="btn borderInputgreen col-12 bg-green" value="ADD" onClick="AddDocs()">
  </div>
</div>  
 

</div>
</div>



<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto">
<h4 class="font-weight-bold mt-4 colorDarkGray pull-left">List of Licenses/Permits/Certificates</h4>


<div class="boxes2 col-12 p-4  overflow-hidden mt-3 margin-top83px">
<table class=" " cellpadding="10" width="100%">
<thead>
<tr class="border-bottom1px colorDarkGray">
<th align="center">#</th>
<th align="center" id="DocTabHead">License</th>
<th align="center" id="chengeWithCert">Employee</th>
<th align="center">Location</th>
<th align="center">Issuance Date</th>
<th align="center">Expiration Date</th>
<th align="center"> Action</th>
</tr>
</thead>
<tbody id="getAllLicense_permit">
</tbody>
</table>

</div>
</div>


</div>
</div>
</article>

</section>



<!-- Update Popup -->

<div class="modal fade" id="updateliecense" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <h5 class="pull-left col-11 p-0" id="docHead">License Detail</h5>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div id="Lisupdate" class="alert alert-danger" style="display:none;">Please fill-in all the mandatory fields.</div>
            <div class="form-group col-12 p-0"> 
            <label for="titleDeseg" class="colorGreen" id="firstDocType">License</label>
            <input type="hidden"   id="LPIDPOP">
            <input type="hidden"   id="HfilePOP">
            <input type="hidden" id="EmployeeidPOP" >
            <input type="text" class="form-control icon-edit pupopBgPosition borderpuopInput p-0" id="LicensePOP" style="background:#FFF;" readonly>
            </div>
 

  <div class="form-group col-12 p-0">
    <label for="comOn" class="colorGreen">Location</label>
    <select class="icon-arrowPop form-control borderpuopInput p-0 marginicon" id="Restaurantdropdwonpopup"><option value="0">-Select-</option></select>
  </div>
  <div class="form-group col-12 p-0">
    <label for="comOn" class="colorGreen">Comments</label>
    <textarea class="form-control icon-edit icon-position height-70px borderpuopInput p-0 pr-2 icon-editComint" id="CommentsPOP" rows="3" cols="10"></textarea>
  </div>
  <div class="form-group col-12 p-0">
    <label for="dateBirth" class="colorGreen">Issuance Date</label>
    <input type="text" class="form-control borderpuopInput p-0 marginicon datepicker" id="IssuanceDatePOP"   onkeydown="return false">
  </div>
  <div class="form-group col-12 p-0">
    <label for="dateBirth" class="colorGreen">Expiration Date</label>
    <input type="text" class="form-control borderpuopInput p-0 marginicon datepicker" id="ExpirationDatePOP" onkeydown="return false">
  </div> 
  <label for="dateBirth" class="colorGreen">Attachment</label>
  <div class="form-group col-12 p-0 text-right" id="Dlink">
  <a class="btn bg-green text-left d-block" href="" download>View Attachment<i class="fa fa-eye pull-right font-size22px" aria-hidden="true"></i></a>
  </div>
  <div class="form-group col-12 p-0">
  <input type="button" class="btn bg-green col-12 text-left p-2 icon-attachmentpupop pl-3" value="Add New Attachment">
    <input type="file" class="form-control-file inputfileBtn col-lg-12 col-md-12 col-sm-12 col-12 opacity0" name="filePOP" id="filePOP" aria-describedby="fileHelp">
  </div>
  <div class="form-group col-6 p-0 m-auto">
    <input id="upd" type="button" class="btn bg-green col-12" value="UPDATE" onClick="UpdateDocs()">
  </div>
  


       

        </div>
        
      </div>
      
    </div>
  </div>
  
   <!--Update Popup -->







<!-- Email Popup -->
  <div class="modal fade" id="emailPupopModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <h5 class="pull-left col-11 p-0">Email</h5>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div id="emailError" class="alert alert-danger" style="display:none;">Please fill-in all the mandatory fields.</div>
        <div class="form-group col-12 p-0">  
        <label for="titleDeseg" class="colorGreen">To</label>
        <div class="email-checkbox" id="email-checkbox">
          
        </div>
        <input type="text" class="form-control icon-edit pupopBgPosition borderpuopInput p-0" id="emaiTo" aria-describedby="" placeholder="Enter Email">
          </div>  
        
          <div class="form-group col-6 p-0 m-auto">
            <input type="button" class="btn bg-green col-12" value="SEND" onClick="SendEmail()">
          </div>
         
       </div>

        </div>
        
      </div>
      
    </div>
  </div>
  
   <!--Email Popup -->
   
   


 <!-- Delete Popup -->
   
   <div class="modal fade" id="DelModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            
            <p class="text-center font-familyUniversLT pt-3 pb-3">Are you sure you want to delete this record?</p>
            <div class="form-group col-6 pull-left">
            <input type="button" class="btn bg-green col-12 cursor" value="YES" onClick="DeleteDocs()">
            </div>
            <div class="form-group col-6 pull-right">
            <input type="button" class="btn bg-green col-12 cursor" value="NO" onClick="closeDelPupop()">
            </div>


        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- Delete Popup -->



<div class="col-12 pupopBg" id="loader" style="display:block;">
<div class="loader">Loading...</div>
<P class="loaderTxt">Loading, please wait.</P>
</div>

<!-- LogModal Popup -->
   
<div class="modal fade" id="LogModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <p class="text-center font-familyUniversLT pt-3 pb-3 font-size20px">Are you sure you want to logout?</p>
            <div class="form-group col-6 pull-left">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="YES" onClick="LogoutUser()">
            </div>
            <div class="form-group col-6 pull-right">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="NO" onClick="closeLogoutModal()">
            </div>

      
       </div>

        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- LogModal Popup -->


</body>
<script>

$("#Comments,#CommentsPOP").bind('keypress', function(e) {
   
       if ((e.which > 32 && e.which < 44) || (e.which==45) ||   (e.which > 57 && e.which < 65) ||  (e.which > 90 && e.which < 97) || e.which > 122)         {
               e.preventDefault();
         }
				
}); 

var d = new Date();
var year = d.getFullYear();
d.setFullYear(year);



   
       
     
     $("#IssuanceDate, #IssuanceDatePOP").datepicker({
		   onSelect: function(date){
			  $("#ExpirationDate").datepicker( "option", "minDate", date );  
			  document.getElementById("ExpirationDate").disabled = false;
		   },
		changeMonth: true,
		changeYear: true,
		dateFormat: 'mm/dd/yy',
		yearRange : '1950:' + year + '',
		defaultDate: d
		 
		 }); 
     $("#ExpirationDate, #ExpirationDatePOP").datepicker(
	 {
		changeMonth: true,
		changeYear: true,
		dateFormat: 'mm/dd/yy',
		yearRange : '1950:' + (year+5) + '',
		defaultDate: d
		 });	
		
       
   



</script>

<script>  


function AddLP(){
	     $('#export_excel').submit();
}

 $(document).ready(function(){  
   $('#export_excel').on('submit', function(event){  
	       event.preventDefault();  
		   var Location = $("#Restaurantdropdwon").val();
		   var data_new = new FormData(this); 
		   data_new.append('admin', ADMIN);
		   data_new.append('doctype', activeTabName);
		   data_new.append('LPType', activeTab);
		   data_new.append('location', Location);
		   
           $.ajax({  
                url:"licensePermit.php",  
                method:"POST",  
                data:data_new,  
                contentType:false,  
                processData:false,  
     success:function(response)
		{
			
			 console.log(response);
  			 if(response==1)
			 { 	
			
				SAlert("Records insert successfully.");
				$('#excel_file').val("");
				getAllLicense();
			
			
				
			 }
  			 if(response==2)
  			 {
				DSAlert("Please upload a valid excel file.","margin-top0px");
				Cloading();
				$('#excel_file').val("");
				
  			 }
	   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");Cloading(); 	
	   }
           }); 
	
      });  
 });  
 


 </script> 
 
<script>  


function AddCER(){
	     $('#export_excel2').submit();
}

 $(document).ready(function(){  
   $('#export_excel2').on('submit', function(event){  
	       event.preventDefault();  
		   var Location = $("#Restaurantdropdwon").val();
		   
		   var EmployeeSelect = $("#EmployeeSelect").val();
		   
		   if(EmployeeSelect=="-Select-"){EmployeeSelect=0;}
		   
		   var data_new = new FormData(this); 
		   data_new.append('admin', ADMIN);
		   data_new.append('doctype', activeTabName);
		   data_new.append('LPType', activeTab);
		   data_new.append('location', Location);
		   data_new.append('Emp', EmployeeSelect);
		   
           $.ajax({  
                url:"export_certificate.php",  
                method:"POST",  
                data:data_new,  
                contentType:false,  
                processData:false,  
     success:function(response)
		{
			
			 console.log(response);
  			 if(response==1)
			 { 	
			 //alert(activeTabName);
				SAlert("Records insert successfully.");
				$('#excel_file2').val("");
				getAllCertification();
				
			 }
  			 if(response==2)
  			 {
				DSAlert("Please upload a valid excel file.","margin-top0px");
				Cloading();
				$('#excel_file2').val("");
				
  			 }
	   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");Cloading(); 	
	   }
           }); 
	
      });  
 });  
 


 </script>  
</html>
