<?php

use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

require '../src/start.php';

$hash = $_SESSION['paypal_hash'];

if(isset($_GET['approved']))
{

	$approved = $_GET['approved'] === 'true';
	
	if($approved){
		
		$payerID = $_GET['PayerID'];
		
		// Get the Payment ID 
		$arr = array("hash" => $hash);
		
		$paymentId = $db->prepare("SELECT payment_id FROM schoolforms.rcm_transaction_payments WHERE hash = :hash");
		
		$paymentId->execute(['hash' =>  $_SESSION['paypal_hash']]);
		 
		$paymentId = $paymentId->fetchObject()->payment_id;
		
		$payment = Payment::get($paymentId, $api);
		
		

        $payment_methode = $payment->payer->payment_method;
		$buyer_name = $payment->payer->payer_info->first_name." ".$payment->payer->payer_info->last_name;
		$buyer_email = $payment->payer->payer_info->email;
		
		foreach($payment->transactions as $item)
		{
		  
		  
		   $membershiptype = $item->description;
		   $amount = $item->amount->total;
		   $shipping_add = $item->item_list->shipping_address->line1.",".$item->item_list->shipping_address->city.",".$item->item_list->shipping_address->state." ".$item->item_list->shipping_address->postal_code.",".$item->item_list->shipping_address->country_code;
		  $taxamount = $item->amount->details->tax;
		  
		
		  
		   
		}
		
		$execution = new PaymentExecution();
		
		$execution->setPayerId($payerID);
		
		$payment->execute($execution, $api);
		
		
		

		$updatetrans = $db->prepare("UPDATE	schoolforms.rcm_transaction_payments SET 
		complete = 1,
		payment_methode='$payment_methode',
		buyer_name='$buyer_name',
		buyer_email='$buyer_email',
		membershiptype='$membershiptype',
		shipping_add='$shipping_add',
		amount='$amount',
		tax='$taxamount'
		 WHERE payment_id = :payment_id");
		
		$updatetrans->execute(['payment_id' => $paymentId]);
		
	
			
		$todaydatetime = date('Y-m-d');	
		//Get the preivios date date of a user
		
		$edate = $db->prepare("SELECT date FROM schoolforms.rcm_users_v2 WHERE id = :user_id");	
		$edate->execute(['user_id' => $_SESSION['user_id']]);
		
		if($data=$edate->fetchAll(PDO::FETCH_ASSOC))
	    {
	  	   $singupdate = $data[0]["date"];
	     }
		 
		
		 
		 if($todaydatetime > $singupdate)
		 
		 {
				if($amount<21)
				{
				$datetime = date('Y-m-d', strtotime($todaydatetime. ' + 30 days'));
				}
				
				if($amount >199)
				{
				$datetime = date('Y-m-d', strtotime($todaydatetime. ' + 365 days'));
				}
		 }
		 
	  	else { 
		 
	   
				if($amount<21)
				{
				$datetime = date('Y-m-d', strtotime($singupdate. ' + 30 days'));
				}
				
				if($amount >199)
				{
				$datetime = date('Y-m-d', strtotime($singupdate. ' + 365 days'));
				}
		
		}
	
		
	    $updatedate = $db->prepare("UPDATE schoolforms.rcm_users_v2 SET date ='$datetime' WHERE id = :user_id");	
		$updatedate->execute(['user_id' => $_SESSION['user_id']]);
		
		
		header('Location: ../index.php');
		 
		 
		
		
		
		}
	else {
		
		header('Location: ../paypal/cancelled.php');
		
		}	
	
}
	




?>