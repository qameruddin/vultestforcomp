<?php



header('Access-Control-Allow-Origin: *');  
include("class.phpmailer.php");

$db =getConnection();
 
 
 
function Pool30($db)
{ 
	$statement= $db->prepare("SELECT id as noteID,pool,doc_type,doc_id,is_notify,admin FROM schoolforms.rcm_notification_v2 where is_notify='no' and pool='30Pool' group by admin");
 	$statement->execute();
	
	if($data=$statement->fetchAll(PDO::FETCH_ASSOC))
	{	
	  
		
		foreach($data as $d)
		{
			
 			$userData = array("admin" => $d['admin']);
			
			$Ustatement= $db->prepare("SELECT  rcm_notification_v2.id as id,  rcm_notification_v2.pool,rcm_notification_v2.doc_type, rcm_notification_v2.doc_id, rcm_notification_v2.is_notify, rcm_users_v2.id as userID, rcm_users_v2.fname, rcm_users_v2.lname, rcm_users_v2.email from schoolforms.rcm_users_v2 inner join schoolforms.rcm_notification_v2 on rcm_users_v2.id=rcm_notification_v2.admin where rcm_notification_v2.pool='30Pool' and rcm_notification_v2.admin=:admin order by rcm_notification_v2.is_notify");
 			$Ustatement->execute($userData);
			if($user=$Ustatement->fetchAll(PDO::FETCH_ASSOC))
			{
				
				
				$UserData=array();
				array_push($UserData,$user[0]['fname'],$user[0]['email'],$user[0]['userID']);
				
				
 				foreach($user as $u)
				{
 					$Docs = array("doc_id"=>$u['doc_id']);
 					if($u['doc_type']=="rcm_license_permit_v2")
					{
						$Docsstatement1= $db->prepare("SELECT schoolforms.rcm_license_permit_v2.id,schoolforms.rcm_license_permit_v2.license,schoolforms.rcm_license_permit_v2.LocationID,schoolforms.rcm_license_permit_v2.EmpID,schoolforms.rcm_employee_v2.emp_name,schoolforms.rcm_restaurant_v2.restaurant_name as location
FROM schoolforms.rcm_license_permit_v2 
LEFT JOIN schoolforms.rcm_employee_v2 on schoolforms.rcm_license_permit_v2.EmpID = schoolforms.rcm_employee_v2.id
LEFT JOIN schoolforms.rcm_restaurant_v2 on schoolforms.rcm_license_permit_v2.LocationID = schoolforms.rcm_restaurant_v2.id
WHERE schoolforms.rcm_license_permit_v2.id=:doc_id");


						$Docsstatement1->execute($Docs);
						if($UserData1=$Docsstatement1->fetchAll(PDO::FETCH_ASSOC))
						{
							
							$UserData1=array("docs"=>$UserData1[0]['license'],"emp"=>$UserData1[0]['emp_name'],"location"=>$UserData1[0]['location'],"Title"=>'Permit/License');
							
							
  							array_push($UserData,$UserData1);
							$Updatestatement= $db->prepare("UPDATE schoolforms.rcm_notification_v2 SET is_notify='yes' where rcm_notification_v2.doc_id=:doc_id");
 							$Updatestatement->execute($Docs);
							
							
 						}
						
	                  }
					  
					if($u['doc_type']=="rcm_certification_v2")
					{
						$Docsstatement2= $db->prepare("SELECT rcm_certification_v2.id as id,rcm_certification_v2.certification as certification, rcm_certification_v2.employeeID as employeeID,rcm_certification_v2.LocationID,schoolforms.rcm_employee_v2.emp_name,schoolforms.rcm_restaurant_v2.restaurant_name as location
FROM schoolforms.rcm_certification_v2
LEFT JOIN schoolforms.rcm_employee_v2 on schoolforms.rcm_certification_v2.employeeID = schoolforms.rcm_employee_v2.id
LEFT JOIN schoolforms.rcm_restaurant_v2 on schoolforms.rcm_certification_v2.LocationID = schoolforms.rcm_restaurant_v2.id
WHERE rcm_certification_v2.id=:doc_id");
						$Docsstatement2->execute($Docs);
						if($UserData2=$Docsstatement2->fetchAll(PDO::FETCH_ASSOC))
						{ 
							$UserData2=array("docs"=>$UserData2[0]['certification'],"emp"=>$UserData2[0]['emp_name'],"location"=>$UserData2[0]['location']);
  							array_push($UserData,$UserData2);
							$Updatestatement= $db->prepare("UPDATE schoolforms.rcm_notification_v2 SET is_notify='yes' where rcm_notification_v2.doc_id=:doc_id");
 							$Updatestatement->execute($Docs);						
						}
  					}
					
 					/*if($u['doc_type']=="rcm_inspection_v2")
					{
						$Docsstatement3= $db->prepare("SELECT inspection FROM schoolforms.rcm_inspection_v2 WHERE id=:doc_id");
						$Docsstatement3->execute($Docs);
						if($UserData3=$Docsstatement3->fetchAll(PDO::FETCH_ASSOC))
						{ 
							$UserData3=array("docs"=>$UserData3[0]['inspection'],"Title"=>'Inspection');
  							array_push($UserData,$UserData3);
							$Updatestatement= $db->prepare("UPDATE schoolforms.rcm_notification_v2 SET is_notify='yes' where rcm_notification_v2.doc_id=:doc_id");
 							$Updatestatement->execute($Docs);
						}
  					}*/
					
				}
				
				//echo json_encode($UserData);
				sendEmail($UserData,$db);
				//sms($UserData,$db);
			}
		}
		 
  	}  	
} 

 //30 Days Pool  
function sendEmail($emailData,$db)
{
	$fname=$emailData[0];
	$email=$emailData[1];
	$userID=$emailData[2];
 
 	 unset($emailData[0]);
	 unset($emailData[1]);
	 unset($emailData[2]);
	 $data=""; 
 	 foreach($emailData as $d)
	 {
		 if($d['emp'] !="") { $d['emp'] = "of ".$d['emp']; }
		 $data .="<li>".$d['docs'].'  '.$d['emp'].' from '.$d['location']."</li>";
		 
	 }	
	 
	
	 
	  
  	$mail = new PHPMailer();  
	$mail->From ="info@complyance.net";
	$mail->FromName ="COMPLYANCE"; 
	$mail->addReplyTo("noreply@complyance.net", "COMPLYANCE");
	$mail->isHTML(true);
	$mail->Subject = "30 Days Until Expiration"; 
 	$mail->AltBody = "Error";
	
	$statement = $db->prepare("SELECT email,fname FROM schoolforms.rcm_notify_email_v2 where admin=:admin");
 	$userData = array("admin" => $userID);
	$statement->execute($userData);
	$user=$statement->fetchAll(PDO::FETCH_ASSOC);
 	array_push($user,array("email"=>$email,"fname"=>$fname));
 	
 	foreach($user as $u)
	{
 		$mail2 = clone $mail;
 		$mail2->AddAddress($u["email"], $u["fname"]);
		$mail2->Body = "Hi ".$u["fname"].", <br/><br/> You have the following items that are 30 days away from its expiration date. 
		<br/><br/>
		<ol>".$data."</ol>
		<br/>
		Please <a href='http://complyance.net/tool'>click here</a> to access COMPLYANCE and view the details. <br/><br/>Thank You.";
 		if(!$mail2->Send()) 
		{
			
		    echo 'Mailer Error: ' . $mail2->ErrorInfo;

		} 
		else 
		{  		
			echo json_encode($u["email"]."<br>");
		}
		
	}

	
}



function sms($emailData,$db)
{
	
 	$curl = curl_init();
	$fname=$emailData[0];
	$email=$emailData[1];
	$userID=$emailData[2];
 
 	 unset($emailData[0]);
	 unset($emailData[1]);
	 unset($emailData[2]);
	 $data=""; 
 	 foreach($emailData as $d)
	 {
		 $data .= $d['docs']."\n";
		  //$data +=1;
 	 }
	 
	
	 
 	$statement= $db->prepare("SELECT userid,phone,active,countrycode FROM schoolforms.sms_notify where userid=:admin and active='active'");
	$userData = array("admin" => $userID);
	$statement->execute($userData);
  	if($userSMS=$statement->fetchAll(PDO::FETCH_ASSOC))
	{
		
	foreach($userSMS as $SMS)
	{
		
		
		 $phoneNumber = $SMS['countrycode'].$SMS['phone'];
		 $message ="Hi ".$fname.", you have \n".$data." item(s) that are 30 days away from it's expiration. For further information, please login into your Complyance account. Thank you";
			
				curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://complyance.net/sms-notification/sendnotifications.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_number\"\r\n\r\n".$phoneNumber."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"message\"\r\n\r\n\"".$message."\"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
		  CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			"postman-token: 10530b0d-c583-86dd-f95f-4236791a0abf"
		  ),
		));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  echo $response;
			}
			
	}
				  
		
}
///close if



}


Pool30($db);
?>