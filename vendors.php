<?php
$check=$_COOKIE['login'];
if($check!="")
{
	
}else{header("location:index.php");}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>COMPLYANCE - VENDORS</title>
<link rel="icon" href="images/logo/favicon.png" type="images/logo/favicon.png" />
<link href="fonts/fontStyle.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src="js/dataJs/jquery-latest.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<link rel="stylesheet" href="js/dataJs/jquery.dataTables.min.css" />
<script src="js/dataJs/jquery.dataTables.min.js"></script>
<script src="js/dataJs/dataTables.buttons.min.js"></script>
<script src="js/dataJs/buttons.html5.min.js"></script>
<script src="js/webjs1.js"></script>
<script src="js/new1.js"></script>
<script src="js/jquery.maskMoney.js"></script>
<script src="js/toggal.js"></script>


 </head>

<body onLoad="username(),GetAllVendor()">
<?php include('aside.php'); ?>

<section id="secFixHeight" class="overflow-hidden col-lg-10 col-md-9 col-sm-12 col-12 pull-left p-0">
<header class="pt-4 pb-4 border-bottom1px bg-colorWhite">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<div class="pull-left" style="display:block;">

<button class="btn btn-darkMenu paddingMenu cursor menuIconDis" onclick="openMenu();"><i class="fa fa-bars font-size22px"></i></button>
</div>
<div class="row">
   <div class="col-md-8 colorGreen m-0 textCenterScr768">
   <h3>CONTACTS <span class="rightarrow">&#x25BA;</span><span class="subheading">VENDORS</span></h3>
   
   </div>
   <div class="col-lg-4 col-md-4 col-sm-3 col-6 pull-right">
<p class="pull-right m-0 mr-2"><span id="adminName"> </span><i class="fa fa-power-off ml-3 colorGreen font-size22px pull-right padding-top2px cursor" onclick="LogoutModal()"></i> </p>
</div>
   
 </div>


</div>
</div>
</div>
</header>

<article class="overflow-yscroll mb-5">
<div class="container">
<div class="row">
<div class="col-lg-11 col-md-11 col-sm-12 col-12 m-auto p-0">
<div class="col-12 overflow-hidden">
<h4 class="font-weight-bold mt-4 colorDarkGray pull-left">Add Vendor</h4>
<div class="alert alert-success fadeIn pull-right mb-0 margin-top10px" id="AlertMsg" role="alert" style="display:none">
  <span id="AlertTxt">You successfully Add Data.</span>
</div>
</div>

<div class="col-12 border-bottom1px pb-4 mt-3">

<div class="boxes col-12 p-4">
 <div class="col-12 overflow-hidden">
  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="empName">Vendor Name<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control icon-edit borderInputgreen" id="FirstName" placeholder="Vendor Name"  onKeyPress="return onlyAlphabets(event)">
  </div> 
 
  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="dateBirth">Email Address<span style="color:#149265;">*</span></label>
    <input type="email" class="form-control icon-edit borderInputgreen" id="EmailAddress"  placeholder="Email Address" >
  </div>
  <div class="form-group pull-left  col-lg-3 col-md-5 col-sm-12 col-12 p-1">
   <label for="dateBirth">Phone Number<span style="color:#149265;">*</span></label>
   <input type="text" class="form-control icon-edit borderInputgreen" id="PhoneNumber"  placeholder="Phone Number" maxlength="15" onkeypress="return isNumber(event)">
  </div>
  
  <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="titleDeseg">Company Name<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control icon-edit borderInputgreen" id="CompanyName" placeholder="Company Name" onKeyPress="return onlyAlphabets(event)">
  </div>
  
</div>  
<div class="col-12 overflow-hidden">
  
  
  <div class="form-group pull-left col-lg-3 col-md-5 col-sm-12 col-12 p-1">
    <label for="JoiningDate">Expertise<span style="color:#149265;">*</span></label>
    <input type="text" class="form-control icon-edit borderInputgreen" id="Expertise" placeholder="Expertise" >
  </div>
  
 
  
  <div class="form-group pull-right col-lg-3 col-md-5 col-sm-12 col-12 p-1 mt-33">
  <form mehtod="post" id="export_excel" enctype="multipart/form-data">  
    <button type="button" class="form-control-file bg-green col-lg-12 col-md-12 col-sm-12 col-12 atchbtn impbtn borderInputgreen font-size16px" aria-describedby="fileHelp">IMPORT FROM EXCEL</button>
   <input type="file" class="form-control-file inputfileBtn col-lg-12 col-md-12 col-sm-12 col-12 opacity0 borderInputgreen"  type="file" name="excel_file" id="excel_file">
    </form>
   
  </div>
   <div class="form-group pull-right col-lg-2 col-md-3 col-sm-12 col-12 p-1 margin-top28px">
    <input type="button" onClick="Vendors()" class="btn borderInputgreen col-12 bg-green" value="ADD">
  </div>
  
</div>  
 
</div>
</div>


<div class="col-12 margin-top83px">
<h4 class="font-weight-bold mt-4 colorDarkGray pull-left">List of Vendors</h4>
<div class="boxes2 col-12 p-4 overflow-hidden mt-3 margin-top83px">
<table class=" " cellpadding="10" width="100%" border="0" id="vendorTable">
<thead>
<tr class="border-bottom1px colorDarkGray">
<th align="center">#</th>
<th align="center">Vendor Name</th>
<th align="center">Email Address</th>
<th align="center">Phone Number</th>
<th align="center">Company Name</th>
 <th align="center">Expertise</th>
 <th align="center">Action</th>
</tr>
</thead>

<tbody id="AllVendorsData">
</tbody>
</table>

</div>
</div>

</div>
</div>
</div>

</article>

</section>
  

<!--/*DELETE PUPOP*/ 
<div class="col-12 pupopBg" id="deleteBox" style="display:none;">
<div class="col-lg-3 col-md-4 col-sm-6 col-11 boxes pupopInnerBox">
<div class="col-12 pt-3 overflow-hidden">
<p class="text-center font-familyUniversLT pt-3 pb-3">Are you sure you want to delete this record?</p>
<div class="form-group col-6 pull-left">
    <input type="button" class="btn borderInputgreen bg-green col-12 cursor" value="YES" onClick="DeleteVendor()">
  </div>
  <div class="form-group col-6 pull-right">
    <input type="button" class="btn borderInputgreen bg-green col-12 cursor" value="NO" onClick="closeDelPupop()">
  </div>
</div>
</div>
</div>

-->


<!-- Delete Popup -->
   
<div class="modal fade" id="DelModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <p class="text-center font-familyUniversLT pt-3 pb-3">Are you sure you want to delete this record?</p>
            <div class="form-group col-6 pull-left">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="YES" onClick="DeleteVendor()">
            </div>
            <div class="form-group col-6 pull-right">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="NO" onClick="closeDelPupop()">
            </div>

      
       </div>

        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- Delete Popup -->





<div class="col-12 pupopBg" id="loader" style="display:block;">
<div class="loader">Loading...</div>
<P class="loaderTxt">Loading, please wait.</P>
</div>


<!-- LogModal Popup -->
   
<div class="modal fade" id="LogModal" role="dialog">
    <div class="modal-dialog" style="top: 45% !important;transform: translate(0, -50%) !important;-ms-transform: translate(0, -50%) !important;
    -webkit-transform: translate(0, -50%) !important;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <p class="text-center font-familyUniversLT pt-3 pb-3 font-size20px">Are you sure you want to logout?</p>
            <div class="form-group col-6 pull-left">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="YES" onClick="LogoutUser()">
            </div>
            <div class="form-group col-6 pull-right">
            <input type="button" class="btn borderInputgreen col-12 cursor bg-green" value="NO" onClick="closeLogoutModal()">
            </div>

      
       </div>

        </div>
        
      </div>
      
    </div>
  </div>
  
  <!-- LogModal Popup -->

<script>  
 $(document).ready(function(){  
   $('#export_excel').on('submit', function(event){  
	       event.preventDefault();  
		   var data_new = new FormData(this); 
		  data_new.append('admin', ADMIN);
		   
		   
           $.ajax({  
                url:"export_vendors.php",  
                method:"POST",  
                data:data_new,  
                contentType: false,
				processData: false, 
     success:function(response)
		{
			
			
			// console.log(data_new);
  			 if(response==1)
			 { 	
				SAlert("Records insert successfully.");
				GetAllVendor();
				 $('#excel_file').val("");
			 }
  			 if(response==2)
  			 {
				DSAlert("Please upload a valid excel file.","margin-top0px");
				Cloading();
				 $('#excel_file').val("");
				
  			 }
	   		}, 
		error: function (xhr, status) 
		{  
			DSAlert("Unable to proceed, please try again.","margin-top0px");
			Cloading(); 	
	   }
           }); 
	
      });  
 });  
 
 
 function AddVendors1(){
	 
	  $('#export_excel').submit(); 
	
	 
	 
	 }

 </script>  

 
</body>
</html>

