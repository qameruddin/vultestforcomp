<?php
header('Access-Control-Allow-Origin:*');
include("email/class.phpmailer.php");
$db = getConnection();
/*function encryptIt( $q ) {
    $cryptKey  = 'B3rmuda-01102017-0102PM';
    $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
     return( $qEncoded );
}
function decryptIt( $q ) {
    $cryptKey  = 'B3rmuda-01102017-0102PM';
    $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
    return( $qDecoded );
}*/
//$string_to_encrypt="this is my first test";
$Enkey="B3rmuda-01102017-0102PM";
// $encrypted_string=openssl_encrypt($string_to_encrypt,"AES-128-ECB",$Enkey);
// $decrypted_string=openssl_decrypt($encrypted_string,"AES-128-ECB",$Enkey);
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} 
$action=$_GET['act'];
 
if($action=='ForgotPasswordEmail')
{
	ForgotPasswordEmail($db);			
}
if($action=='reset')
{
	resetP($db);			
}
if($action=='activate')
{
	activate($db);			
}
if($action=='FuncResetPassword')
{
	FuncResetPassword($db);			
}
if($action=='suppurt')
{
	suppurt($db);			
}
if($action=='ActivationEmail')
{
	ActivationEmail($db);			
}
function suppurt($db){
	
	 $Subject = $_POST['Subject'];
	 $Message = $_POST['Message'];
	 $admin = $_POST['admin'];
	 
	 $mail = new PHPMailer();  
	 $mail->From =$admin;
	 $mail->FromName =""; 
	 
 	 $mail->addAddress('info@complyance.net', 'COMPLYANCE Support <info@complyance.net>');
 	 $mail->addReplyTo("info@complyance.net", "COMPLYANCE Support");
 	 $mail->isHTML(true);
	 $mail->Subject ="COMPLYANCE Support Message"; 
	 $mail->Body = "Subject:".$Subject."<br/>
	                Message: ".$Message." <br/><br/>Thank you.";
							 	 
			$mail->AltBody = "Error";
			if(!$mail->Send()) 
			{
			 	echo json_encode("Error");
			} 
			else 
			{  		
			    echo json_encode("success");
			} 
}
 
function FuncResetPassword($db)
{
	$NewPassword1 = $_POST['NewPassword1'];
	$encrypted = my_simple_crypt($NewPassword1 , 'e');
	$email1		= $_POST['email1'];
	$token1		= $_POST['token1'];
	$arr = array("token" => $token1,"email" => $email1);
$statement= $db->prepare('SELECT * FROM schoolforms.rcm_users_v2 WHERE email=:email AND forgotKey=:token');
$statement->execute($arr);
	if($data=$statement->fetchAll(PDO::FETCH_ASSOC))
	{
		$arr2 = array("pwd" => $encrypted,"token" => null,"linkexpire"=>null,"id" => $data[0]['id']);
		$statement2= $db->prepare('UPDATE schoolforms.rcm_users_v2 SET pwd=:pwd,linkexpire=:linkexpire, forgotKey=:token WHERE id=:id');
		if($statement2->execute($arr2))
		{  
			echo json_encode("success");
		}
		else
		{ echo json_encode("Error");  }
	}
	else
	{
		header("Location:https://complyance.net/tool/linkExpired.php");
	}
	
}
 function resetP($db)
 {
 	$Enkey = "B3rmuda-01102017-0102PM";
 	$token		= $_GET['token'];
 	$email		= $_GET['pr'];
  	$token1 = str_replace(" ", "+", $token);
	$email1 = str_replace(" ", "+", $email);
 	/*$token = decryptIt( $token1);
	$email = decryptIt( $email1 );*/
	$token = openssl_decrypt($token1,"AES-128-ECB",$Enkey);
	$email = openssl_decrypt($email1,"AES-128-ECB",$Enkey);
  	$arr		= array("token" => $token,"email" => $email);
	$statement= $db->prepare('SELECT * FROM schoolforms.rcm_users_v2 WHERE email=:email AND forgotKey=:token AND linkexpire="false"');
   	$statement->execute($arr);
	if($data=$statement->fetchAll(PDO::FETCH_ASSOC))
	{
		header("Location:https://complyance.net/tool/resetpassword.php?act=reset&token=".$token1."&pr=".$email1);
	}
	else
	{
		header("Location:https://complyance.net/tool/linkExpired.php");
	}
 }
 function activate($db)
 {
 	$Enkey = "B3rmuda-01102017-0102PM";
 	$token		= $_GET['token'];
 	$email		= $_GET['pr'];
  	$token1 = str_replace(" ", "+", $token);
	$email1 = str_replace(" ", "+", $email);
 	//$token = decryptIt( $token1);
	//$email = decryptIt( $email1 );
	$token = openssl_decrypt($token1,"AES-128-ECB",$Enkey);
	$email = openssl_decrypt($email1,"AES-128-ECB",$Enkey);

  	$arr		= array("token" => $token,"email" => $email);
	$statement= $db->prepare('SELECT * FROM schoolforms.rcm_users_v2 WHERE email=:email AND forgotKey=:token AND linkexpire="false"');
   	$statement->execute($arr);
	if($data=$statement->fetchAll(PDO::FETCH_ASSOC))
	{
 		$arr1 = array("email" => $email );
 		$statemen1t= $db->prepare('UPDATE schoolforms.rcm_users_v2 SET linkexpire="true",status="active" WHERE email=:email');
		$statemen1t->execute($arr1);
		header("Location:https://complyance.net/tool/#accountactive");
 	}
	else
	{
		header("Location:https://complyance.net/tool/linkExpired.php");
	}
 }

function ForgotPasswordEmail($db)
{
	$Enkey = "B3rmuda-01102017-0102PM";
	$email		= $_POST['email'];
	$input = generateRandomString();
	$token = openssl_encrypt($input,"AES-128-ECB",$Enkey);
	$emailEnc = openssl_encrypt($email,"AES-128-ECB",$Enkey);
 	$arr		= array("email" => $email);
	$statement= $db->prepare('SELECT * FROM schoolforms.rcm_users_v2 WHERE email=:email');
 	$statement->execute($arr);
 	if($data=$statement->fetchAll(PDO::FETCH_ASSOC))
	{
   		  	$mail = new PHPMailer();  
			$mail->From ="info@complyance.net";
			$mail->FromName ="COMPLYANCE"; 
 			$mail->addAddress($email,$data[0]['fname']);
 			$mail->addReplyTo("noreply@complyance.net", "COMPLYANCE");
 			$mail->isHTML(true);
			$mail->Subject ='Password Reset'; 
		 	$mail->Body = "Hi ".$data[0]['fname'].", <br/><br/>You recently requested to reset your password for your COMPLYANCE account. Please <a href='https://complyance.net/tool/passwordreset.php?act=reset&token=".$token."&pr=".$emailEnc."'>Click Here</a> to reset it.
			<br/><br/>
			If you did not request a password reset, please ignore this email.
			<br/><br/>Thank You";
		 	 
			$mail->AltBody = "Error";
			if(!$mail->Send()) 
			{
			 	echo json_encode("Error");
			} 
			else 
			{  		
 			   	$arr = array("email" => $email,"token" => $input,"linkexpire" =>'false');
 			 	$statement= $db->prepare('UPDATE schoolforms.rcm_users_v2 SET forgotKey=:token,linkexpire=:linkexpire WHERE email=:email');
				if($statement->execute($arr))
				{  
					echo json_encode("success");
				}
				else
				{ echo json_encode("Error");  }
			} 
   	}
	else
	{  echo json_encode("No Records"); 	}
}
 
function ActivationEmail($db)
{
	$Enkey="B3rmuda-01102017-0102PM";
	$email = $_POST['email'];
	$fname = $_POST['fname'];
	$input = generateRandomString();
	//$token = encryptIt( $input );
	$token = openssl_encrypt($input,"AES-128-ECB",$Enkey);
	//$emailEnc = encryptIt($email);
	$emailEnc = openssl_encrypt($email,"AES-128-ECB",$Enkey);
 	$arr		= array("email" => $email);
 	$statement= $db->prepare('SELECT * FROM schoolforms.rcm_users_v2 WHERE email=:email');
 	$statement->execute($arr);
 	if($data=$statement->fetchAll(PDO::FETCH_ASSOC))
	{
			$mail = new PHPMailer();  
			$mail->From ="info@complyance.net";
			$mail->FromName ="COMPLYANCE"; 
 			$mail->addAddress($email, $fname);
 			$mail->addReplyTo("noreply@complyance.net", "COMPLYANCE");
 			$mail->isHTML(true);
			$mail->Subject ='Account Activation'; 
		 	$mail->Body = "Dear ".$fname.", <br/><br/>Thank you for registering for COMPLYANCE. Please <a href='https://complyance.net/tool/passwordreset.php?act=activate&token=".$token."&pr=".$emailEnc."'>cick here</a> to activate your account.
			<br/><br/>In case of any problems, please email us at info@complyance.net<br/><br/> Thank You."; 		 	 
			$mail->AltBody = "Error";
 			if(!$mail->Send()) 
			{
			 	echo json_encode("Error");
			} 
			else
			{  		
			   	$arr = array("email" => $email,"token" => $input,"linkexpire" =>'false');
 			 	$statement= $db->prepare('UPDATE schoolforms.rcm_users_v2 SET forgotKey=:token,linkexpire=:linkexpire WHERE email=:email');
				if($statement->execute($arr))
				{  
					echo json_encode("success");
				}
				else
				{ 
				    echo json_encode("Error");
				 }
			}
		}else
			{  echo json_encode("No Records"); 	}
}

function my_simple_crypt( $string, $action = 'e' ) {
    $secret_key = 'my_simple_secret_key';
    $secret_iv = 'my_simple_secret_iv';

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }

    return $output;
}